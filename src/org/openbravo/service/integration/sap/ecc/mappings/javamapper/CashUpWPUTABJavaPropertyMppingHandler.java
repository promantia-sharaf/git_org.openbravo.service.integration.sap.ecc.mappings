/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.CashUp;
import org.openbravo.service.integration.sap.ecc.mappings.javamapper.processor.AggregatedSalesByPaymentMethod;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the CashUp entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = CashUp.ENTITY_NAME)
public class CashUpWPUTABJavaPropertyMppingHandler extends JavaPropertyMappingHandler<CashUp> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_CASHUP_UPDATE);
    properties.put(31, MappingConstants.SAP_CASHUP_TERMINAL);
    properties.put(41, MappingConstants.SAP_CASHUP_CASHIER);
    properties.put(51, MappingConstants.SAP_LINE_CASHUP_SEGMENT);
    return properties;
  }

  @Override
  public Object getValueOfProperty(CashUp cashUp, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return getFiliale(cashUp);
    case MappingConstants.SAP_CASHUP_UPDATE:
      return getCashUpUpdate(cashUp);
    case MappingConstants.SAP_CASHUP_TERMINAL:
      return getTerminal(cashUp);
    case MappingConstants.SAP_CASHUP_CASHIER:
      return getCashier(cashUp);
    case MappingConstants.SAP_LINE_CASHUP_SEGMENT:
      return new AggregatedSalesByPaymentMethod(cashUp).getSalesByPaymentMethod();
    default:
      return null;
    }
  }

  protected String getCashier(CashUp cashUp) {
    return cashUp.getCreatedBy().getUsername();
  }

  private Object getCashUpUpdate(CashUp cashUp) {
    return IdocDateUtils.getDatePart(cashUp.getCashUpdate());
  }

  private Object getFiliale(CashUp cashUp) {
    return MappingUtils.getSAPFiliale(cashUp);
  }

  private String getTerminal(CashUp cashUp) {
    OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
    orderCriteria.add(Restrictions.eq(Order.PROPERTY_OBPOSAPPCASHUP, cashUp.getId()));
    orderCriteria.setMaxResults(1);
    Order order = (Order) orderCriteria.uniqueResult();
    if (order != null) {
      if (StringUtils.isBlank(order.getObposApplications().getSapobmpSapTerminal())) {
        throw new OBException("POS Terminal " + order.getObposApplications()
            + " is not mapped to SAP Terminal");
      }
      return order.getObposApplications().getSapobmpSapTerminal();
    }
    return null;
  }

}
