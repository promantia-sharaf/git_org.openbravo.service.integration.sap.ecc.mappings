/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.hook;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectAfterFlushHook;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * 
 * Creates parent/child relationship on ProductCategory
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = ProductCategory.ENTITY_NAME)
public class ProductCategoryHierarchyHook implements ImportedBaseOBObjectAfterFlushHook {

  private static final String SEGMENT_E1WPW02 = "E1WPW02";
  private static final String PARENT = "VERKNUEPFG";

  @SuppressWarnings("unchecked")
  @Override
  public void afterFlush(BaseOBObject bob, SynchronizableBusinessObject item) {
    List<SynchronizableBusinessObject> segments = (List<SynchronizableBusinessObject>) item
        .getProperties().get(SEGMENT_E1WPW02);
    String parentSearchKey = (String) segments.get(0).getProperties().get(PARENT);
    if (parentSearchKey != null) {
      ProductCategory entity = (ProductCategory) bob;
      ProductCategory parent = getParentByClientAndSearchKey(entity.getClient().getId(),
          parentSearchKey);
      TreeNode parentNode = MappingUtils.getTreeNodeByEntityId(parent);
      TreeNode entityNode = MappingUtils.getTreeNodeByEntityId(entity);
      setRelationship(entityNode, parentNode);
    }
  }

  private void setRelationship(TreeNode entityNode, TreeNode parentNode) {
    if (parentNode == null) {
      throw new OBException("Node of parent Product Category does not exists.");
    }
    entityNode.setReportSet(parentNode.getNode());
    OBDal.getInstance().save(entityNode);
  }

  private ProductCategory getParentByClientAndSearchKey(String clientId, String parentSearchKey) {
    OBCriteria<ProductCategory> criteria = OBDal.getInstance()
        .createCriteria(ProductCategory.class);
    criteria.add(Restrictions.eq(ProductCategory.PROPERTY_CLIENT + "." + Client.PROPERTY_ID,
        clientId));
    criteria.add(Restrictions.eq(ProductCategory.PROPERTY_SEARCHKEY, parentSearchKey));
    ProductCategory parent = (ProductCategory) criteria.uniqueResult();
    if (parent == null) {
      throw new OBException("Parent ProductCategory (" + parentSearchKey
          + ") does not exists. Consider reprocess the EDL request.");
    }
    return parent;
  }
}
