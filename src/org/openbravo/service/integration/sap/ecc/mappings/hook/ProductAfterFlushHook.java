/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.hook;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.config.OBRETCOProductList;
import org.openbravo.retail.config.OBRETCOProlProduct;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectAfterFlushHook;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * 
 * Adds the Product to the default Assortment after being imported
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Product.ENTITY_NAME)
public class ProductAfterFlushHook implements ImportedBaseOBObjectAfterFlushHook {

  @Override
  public void afterFlush(BaseOBObject bob, SynchronizableBusinessObject item) {
    Product product = (Product) bob;
    OBRETCOProductList defaultAssortment = OBDal.getInstance().get(OBRETCOProductList.class,
        MappingConstants.DEFAULT_ASSORTMENT_ID);
    if (defaultAssortment == null) {
      defaultAssortment = createDefaultAssortment();
    }
    assignProductToAssortmentIfNotExists(product, defaultAssortment);
  }

  private OBRETCOProductList createDefaultAssortment() {
    OBRETCOProductList defaultAssortment = OBProvider.getInstance().get(OBRETCOProductList.class);
    defaultAssortment.setId(MappingConstants.DEFAULT_ASSORTMENT_ID);
    defaultAssortment.setNewOBObject(true);
    defaultAssortment.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    defaultAssortment.setName(MappingConstants.DEFAULT_ASSORTMENT_NAME);
    OBDal.getInstance().save(defaultAssortment);
    return defaultAssortment;
  }

  private void assignProductToAssortmentIfNotExists(Product product,
      OBRETCOProductList defaultAssortment) {
    if (productNotExistsInAssortment(product, defaultAssortment)) {
      OBRETCOProlProduct assortmentProduct = getAssortmentProduct(product, defaultAssortment);
      defaultAssortment.getOBRETCOProlProductList().add(assortmentProduct);
      OBDal.getInstance().flush();
    }
  }

  private boolean productNotExistsInAssortment(Product product, OBRETCOProductList defaultAssortment) {
    OBCriteria<OBRETCOProlProduct> criteria = OBDal.getInstance().createCriteria(
        OBRETCOProlProduct.class);
    criteria
        .add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_OBRETCOPRODUCTLIST, defaultAssortment));
    criteria.add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_PRODUCT, product));
    return criteria.count() == 0;
  }

  private OBRETCOProlProduct getAssortmentProduct(Product product,
      OBRETCOProductList defaultAssortment) {
    OBRETCOProlProduct assortmentProduct = OBProvider.getInstance().get(OBRETCOProlProduct.class);
    assortmentProduct.setNewOBObject(true);
    assortmentProduct.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    assortmentProduct.setBestseller(false);
    assortmentProduct.setProduct(product);
    assortmentProduct.setObretcoProductlist(defaultAssortment);
    OBDal.getInstance().save(assortmentProduct);
    return assortmentProduct;
  }

}
