/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPB06 in PUBON
 */
public class PubonInvoicePaymentMethod implements PropertiesHolder {

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public PubonInvoicePaymentMethod(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getTotalPaid() {
    BigDecimal amount = (BigDecimal) properties.get(MappingConstants.SAP_INVOICE_TOTAL_PAID);
    return amount.toString();
  }

  public String getPaymentMethodSapCode() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SAP_CODE_FIELD),
        StringUtils.EMPTY);
  }

  public String getCurrency() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CURRENCY),
        StringUtils.EMPTY);
  }

  public String getPaymentMethodSign() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_TOTAL_PAID_SIGN), StringUtils.EMPTY);
  }

  public String getCreditCardDetail() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CCDETAIL),
        StringUtils.EMPTY);
  }

  public String getCreditCardAuth() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CCAUTHCODE),
        StringUtils.EMPTY);
  }

  public Boolean getShowCreditCardInfo() {
    return properties.containsKey(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SHOWCC);
  }

}
