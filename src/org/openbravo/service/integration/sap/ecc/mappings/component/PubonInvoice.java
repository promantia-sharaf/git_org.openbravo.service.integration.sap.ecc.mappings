/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the PUBON template
 */
@ApplicationScoped
@IdocType("PUBON")
public class PubonInvoice extends IdocTemplate {

  private static final String SAP_INVOICELINE_SEGMENT = "E1WPB02";
  private static final String SAP_CURRENCY_ISOCODE = "BELEGWAERS";
  private static final String SAP_USER_NAME = "CSHNAME";
  private static final String SAP_USER_USERNAME = "KASSIERER";
  private static final String SAP_BUSINESS_PARTNER = "KUNDNR";
  private static final String SAP_INVOICEDATE_TIME = "VORGZEIT";
  private static final String SAP_POS_TERMINAL = "KASSID";
  private static final String TEMPLATE_ID = "F1EDDE9F64ED4B5C8387154DCD6A92FE";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getPosTerminal() {
    return (String) getProperties().get(SAP_POS_TERMINAL);
  }

  public String getOrderDate() {
    Date date = (Date) getProperties().get(MappingConstants.SAP_INVOICEDATE_DATE);
    if (date == null) {
      return StringUtils.EMPTY;
    }
    return IdocDateUtils.getDatePart(date);
  }

  public String getOrderTime() {
    Date date = (Date) getProperties().get(SAP_INVOICEDATE_TIME);
    return IdocDateUtils.getTimePart(date);
  }

  public String getDocumentNumber() {
    return (String) getProperties().get(MappingConstants.SAP_INVOICE_DOCNO);
  }

  public String getBusinessPartner() {
    return (String) getProperties().get(SAP_BUSINESS_PARTNER);
  }

  public String getCashierUserName() {
    return StringUtils.defaultIfBlank((String) getProperties().get(SAP_USER_USERNAME),
        StringUtils.EMPTY);
  }

  public String getCashierName() {
    return (String) getProperties().get(SAP_USER_NAME);
  }

  public String getCurrency() {
    return (String) getProperties().get(SAP_CURRENCY_ISOCODE);
  }

  public String getTotalPaid() {
    BigDecimal amount = (BigDecimal) getProperties().get(MappingConstants.SAP_INVOICE_TOTAL_PAID);
    return amount.toString();
  }

  public String getPaymentMethodSapCode() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SAP_CODE_FIELD),
        StringUtils.EMPTY);
  }

  @SuppressWarnings("unchecked")
  public List<PubonInvoiceLine> getInvoiceLines() {
    List<PubonInvoiceLine> orderLines = new ArrayList<>();
    List<SynchronizableBusinessObject> orderLineSBOs = (List<SynchronizableBusinessObject>) getProperties()
        .get(SAP_INVOICELINE_SEGMENT);
    if (orderLineSBOs == null) {
      return orderLines;
    }
    for (SynchronizableBusinessObject orderLineSBO : orderLineSBOs) {
      PubonInvoiceLine orderLine = new PubonInvoiceLine(orderLineSBO.getProperties());
      orderLines.add(orderLine);
    }
    return orderLines;
  }

  @SuppressWarnings("unchecked")
  public List<PubonInvoicePaymentMethod> getPaymentMethodLines() {
    List<PubonInvoicePaymentMethod> paymentMethodLines = new ArrayList<>();

    List<SynchronizableBusinessObject> oaymentLinesSbo = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SEGMENT);
    if (oaymentLinesSbo == null) {
      return paymentMethodLines;
    }
    for (SynchronizableBusinessObject paymentLineSbo : oaymentLinesSbo) {
      PubonInvoicePaymentMethod paymentMethodLine = new PubonInvoicePaymentMethod(
          paymentLineSbo.getProperties());
      paymentMethodLines.add(paymentMethodLine);
    }
    return paymentMethodLines;
  }
}
