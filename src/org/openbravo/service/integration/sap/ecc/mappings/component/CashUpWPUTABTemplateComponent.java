/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the WPUUTAB template
 */
@ApplicationScoped
@IdocType("WPUTAB")
public class CashUpWPUTABTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "EF4D109339C74FEE87AD12A801790487";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getCashUpUpdate() {
    return (String) getProperties().get(MappingConstants.SAP_CASHUP_UPDATE);
  }

  public String getTerminal() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_CASHUP_TERMINAL), StringUtils.EMPTY);
  }

  public String getCashier() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_CASHUP_CASHIER), StringUtils.EMPTY);
  }

  @SuppressWarnings("unchecked")
  public List<CashUpWPUTABLineTemplateComponent> getLines() {
    List<CashUpWPUTABLineTemplateComponent> lines = new ArrayList<>();
    List<SynchronizableBusinessObject> linesSbo = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_LINE_CASHUP_SEGMENT);
    if (linesSbo == null) {
      return lines;
    }
    for (SynchronizableBusinessObject sbo : linesSbo) {
      lines.add(new CashUpWPUTABLineTemplateComponent(sbo.getProperties()));
    }
    return lines;
  }
}
