/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import org.openbravo.model.common.plm.Product;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.process.SapEccExporterSynchronizableBusinessObjectExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * SynchronizableBusinessObjectExporter that exports Products to WP_PLU documents
 * 
 * Returns the id of Products wich have been modified since the last export.
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Product.ENTITY_NAME)
public class ProductExporter extends SapEccExporterSynchronizableBusinessObjectExporter {

  @Override
  public String exportedEntityName() {
    return Product.ENTITY_NAME;
  }

  @Override
  public String getSynchronizationQuery() {
    final StringBuilder query = new StringBuilder();
    query.append("select p.id from Product p ");
    query.append("where p.$incrementalUpdateCriteria ");
    query.append("and p.$clientCriteria ");
    query.append("and p.sapobmpIsreadytoexport = true ");
    query
        .append("or exists (select 1 from PricingProductPrice pprice where pprice.product.id = p.id and pprice.$incrementalUpdateCriteria)");
    return query.toString();
  }

  @Override
  protected String getIdocType() {
    return "WP_PLU";
  }

}
