/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPP03 of the WP_PER template
 */
public class BusinessPartnerLocationTemplateComponent implements PropertiesHolder {

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public BusinessPartnerLocationTemplateComponent(SynchronizableBusinessObject locationSBO) {
    properties = locationSBO.getProperties();
  }

  public String getPhone() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_WP_PER_PHONE),
        StringUtils.EMPTY);
  }

  public String getFax() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_WP_PER_FAX),
        StringUtils.EMPTY);
  }

  public String getAddress() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_ADDRESS),
        StringUtils.EMPTY);
  }

  public String getPostalCode() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_POSTAL_CODE),
        StringUtils.EMPTY);
  }

  public String getCity() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_CITY),
        StringUtils.EMPTY);
  }
}
