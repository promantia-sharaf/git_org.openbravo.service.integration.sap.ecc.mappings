/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.exception.OBException;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaPropertyMappingHandler for the Product Category entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = ProductCategory.ENTITY_NAME)
public class ProductCategoryJavaPropertyMappingHandler extends
    JavaPropertyMappingHandler<ProductCategory> {

  private static final String LEAF = "00";
  private static final Logger log = LoggerFactory
      .getLogger(ProductCategoryJavaPropertyMappingHandler.class);

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_ISACTIVE);
    properties.put(41, MappingConstants.SAP_WPDWGR_SEGMENT_E1WPW02);
    return properties;
  }

  @Override
  public void setPropertyInBaseOBObject(ProductCategory productCategory, String mappingName,
      SynchronizableBusinessObject sbo) {
    Map<String, Object> properties = sbo.getProperties();
    if (!properties.containsKey(mappingName)) {
      log.warn("Property: {} is not included in SynchronizableBusinessObject {}", mappingName,
          properties.keySet());
      return;
    }
    try {
      Object itemValue = properties.get(mappingName);
      switch (mappingName) {
      case MappingConstants.SAP_ORGANIZATION:
        setOrganization(productCategory, itemValue);
        break;
      case MappingConstants.SAP_ISACTIVE:
        setIsActive(productCategory, (String) itemValue);
        break;
      case MappingConstants.SAP_WPDWGR_SEGMENT_E1WPW02:
        setGeneralData(productCategory, itemValue);
        break;
      default:
        log.warn("Java property not defined in the mapping: {}", mappingName);
      }
    } catch (Exception e) {
      log.error("Error while importing the property " + mappingName, e);
      throw new OBException(e);
    }
  }

  @SuppressWarnings("unchecked")
  private void setGeneralData(ProductCategory productCategory, Object itemValue) {
    List<SynchronizableBusinessObject> list = (List<SynchronizableBusinessObject>) itemValue;
    SynchronizableBusinessObject sbo = list.get(0);
    Map<String, Object> properties = sbo.getProperties();
    for (Map.Entry<String, Object> property : properties.entrySet()) {
      String key = property.getKey();
      String value = (String) property.getValue();

      if (StringUtils.equals(MappingConstants.SAP_WPDWGR_DESCRIPTION, key)) {
        setNameAndDescription(productCategory, value);
      } else if (StringUtils.equals(MappingConstants.SAP_WPDWGR_HIERARCHY, key)) {
        setIsSummary(productCategory, value);
      }
    }
    productCategory.setSapobmpSapCode(productCategory.getSearchKey());
  }

  private void setIsActive(ProductCategory productCategory, String modi) {
    productCategory.setActive(StringUtils.equals(MappingConstants.SAP_ACTIVE, modi));
  }

  private void setIsSummary(ProductCategory productCategory, String hierarchy) {
    productCategory.setSummaryLevel(!StringUtils.equals(LEAF, hierarchy));
  }

  private void setNameAndDescription(ProductCategory productCategory, String description) {
    productCategory.setDescription(description);
    productCategory.setName(description);
  }

  private void setOrganization(ProductCategory productCategory, Object itemValue) {
    productCategory.setOrganization(MappingUtils.getOrganization((String) itemValue));
  }
}
