/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaPropertyMappingHandler for the Tax Rate entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = TaxRate.ENTITY_NAME)
public class TaxRateJavaPropertyMappingHandler extends JavaPropertyMappingHandler<TaxRate> {

  private static final String TAX_RATE_SAP_CODE = "MWSI";
  private static final Logger log = LoggerFactory
      .getLogger(TaxRateJavaPropertyMappingHandler.class);

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_ISACTIVE);
    properties.put(31, MappingConstants.SAP_WPDTAX_VALIDFROM);
    properties.put(41, MappingConstants.SAP_WPDTAX_TAX_RATE_SEGMENT);
    return properties;
  }

  @Override
  public void setPropertyInBaseOBObject(TaxRate taxRate, String mappingName,
      SynchronizableBusinessObject sbo) {
    Map<String, Object> properties = sbo.getProperties();
    if (!properties.containsKey(mappingName)) {
      log.warn("Property: {} is not included in SynchronizableBusinessObject {}", mappingName,
          properties.keySet());
      return;
    }
    try {
      Object itemValue = properties.get(mappingName);
      switch (mappingName) {
      case MappingConstants.SAP_ORGANIZATION:
        setOrganization(taxRate, itemValue);
        break;
      case MappingConstants.SAP_ISACTIVE:
        setIsActive(taxRate, (String) itemValue);
        break;
      case MappingConstants.SAP_WPDTAX_VALIDFROM:
        setValidFrom(taxRate, itemValue);
        break;
      case MappingConstants.SAP_WPDTAX_TAX_RATE_SEGMENT:
        setTaxRate(taxRate, itemValue);
        break;
      default:
        log.warn("Java property not defined in the mapping: {}", mappingName);
      }
    } catch (Exception e) {
      log.error("Error while importing the property " + mappingName, e);
      throw new OBException(e);
    }
  }

  private void setValidFrom(TaxRate taxRate, Object itemValue) {
    taxRate.setValidFromDate(MappingUtils.convertDateFromSAPFormat((String) itemValue));
  }

  @SuppressWarnings("unchecked")
  private void setTaxRate(TaxRate taxRate, Object itemValue) {
    SynchronizableBusinessObject segment = ((List<SynchronizableBusinessObject>) itemValue).get(0);
    String description = (String) segment.getProperties().get(
        MappingConstants.SAP_WPDTAX_DESCRIPTION);
    BigDecimal rate = new BigDecimal((String) segment.getProperties().get(
        MappingConstants.SAP_WPDTAX_TAX_RATE));
    taxRate.setDescription(description);
    taxRate.setRate(rate);
    taxRate.setTaxExempt(isZeroRate(rate));
    taxRate.setSapobmpSapCode(TAX_RATE_SAP_CODE);
    setTaxCategory(taxRate);
  }

  private boolean isZeroRate(BigDecimal rate) {
    return rate.compareTo(BigDecimal.ZERO) == 0;
  }

  private void setTaxCategory(TaxRate taxRate) {
    TaxCategory taxCategory = getTaxCategoryByName(taxRate);
    if (taxCategory == null) {
      taxCategory = createTaxCategory(taxRate);
    }
    taxRate.setTaxCategory(taxCategory);
  }

  private TaxCategory getTaxCategoryByName(TaxRate taxRate) {
    OBCriteria<TaxCategory> criteria = OBDal.getInstance().createCriteria(TaxCategory.class);
    criteria.add(Restrictions.eq(TaxCategory.PROPERTY_NAME, taxRate.getName()));
    return (TaxCategory) criteria.uniqueResult();
  }

  private TaxCategory createTaxCategory(TaxRate taxRate) {
    TaxCategory taxCategory = OBProvider.getInstance().get(TaxCategory.class);
    taxCategory.setNewOBObject(true);
    taxCategory.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    taxCategory.setName(taxRate.getName());
    taxCategory.setDescription(taxRate.getDescription());
    taxCategory.setSapobmpSapCode(taxRate.getName());
    OBDal.getInstance().save(taxCategory);
    return taxCategory;
  }

  private void setIsActive(TaxRate taxRate, String modi) {
    taxRate.setActive(StringUtils.equals(MappingConstants.SAP_ACTIVE, modi));
  }

  private void setOrganization(TaxRate taxRate, Object itemValue) {
    taxRate.setOrganization(MappingUtils.getOrganization((String) itemValue));
  }

}
