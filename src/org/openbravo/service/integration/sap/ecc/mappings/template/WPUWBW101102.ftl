<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

-->
<?xml version="1.0" encoding="utf-8"?>
<WPUWBW01>
  <IDOC BEGIN="1">
    <EDI_DC40 SEGMENT="1">
      <TABNAM>EDI_DC40</TABNAM>
      <MANDT>100</MANDT>
      <IDOCTYP>WPUWBW01</IDOCTYP>
      <MESTYP>WPUWBW</MESTYP>
      <SNDPOR>POS-OUT</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <SNDPRN>${data.filiale}</SNDPRN>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>KU</RCVPRT>
      <RCVPRN>SAP</RCVPRN>
    </EDI_DC40>
    <E1WPG01 SEGMENT="1">
      <BELEGDATUM>${data.movementDate}</BELEGDATUM>
    <#list data.lines as line>
      <E1WPG02 SEGMENT="1">
      <#if line.poReference != "">
        <REFERENZNR>${line.poReference}</REFERENZNR>
      </#if>
        <LITEM>${line.lineNo}</LITEM>
        <QUALARTNR>006</QUALARTNR>
        <ARTNR>${line.product}</ARTNR>
        <BEWART>${line.movementType}</BEWART>
        <MEINH>${line.uomEdiCode}</MEINH>
        <MENGE>${line.movementQuantity}</MENGE>
        <LIFNR>${line.businessPartner}</LIFNR>
        <LGORT>${line.warehouse}</LGORT>
      </E1WPG02>
    </#list>
    </E1WPG01>
  </IDOC>
</WPUWBW01>