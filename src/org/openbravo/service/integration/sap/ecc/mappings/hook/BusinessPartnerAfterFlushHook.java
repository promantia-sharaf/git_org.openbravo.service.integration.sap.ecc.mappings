/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.hook;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectAfterFlushHook;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Adds Contact and Locations to Business Partner after bein imported
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = BusinessPartner.ENTITY_NAME)
public class BusinessPartnerAfterFlushHook implements ImportedBaseOBObjectAfterFlushHook {

  private static final Logger log = LoggerFactory.getLogger(BusinessPartnerAfterFlushHook.class);

  @Override
  public void afterFlush(BaseOBObject bob, SynchronizableBusinessObject item) {
    BusinessPartner businessPartner = (BusinessPartner) bob;
    setBusinessPartnerLocation(businessPartner, item);
    createOrUpdateContact(businessPartner, item);
  }

  private void createOrUpdateContact(BusinessPartner businessPartner,
      SynchronizableBusinessObject item) {
    final boolean userHasNoContacts = businessPartner.getADUserList().isEmpty();
    if (userHasNoContacts) {
      createNewContact(businessPartner, item);
    } else {
      User contact = MappingUtils.getContactShownInWebPos(businessPartner);
      setContactInformation(contact, item);
    }
  }

  private User createNewContact(BusinessPartner businessPartner, SynchronizableBusinessObject item) {
    User contact = OBProvider.getInstance().get(User.class);
    contact.setNewOBObject(true);
    contact.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    contact.setBusinessPartner(businessPartner);
    setContactInformation(contact, item);
    OBDal.getInstance().save(contact);
    businessPartner.getADUserList().add(contact);
    return contact;
  }

  private void setContactInformation(User contact, SynchronizableBusinessObject item) {
    contact.setName(contact.getBusinessPartner().getName());
    contact.setFirstName(contact.getBusinessPartner().getName());
    contact.setPhone(getPhoneFromSegment(item));
  }

  @SuppressWarnings("unchecked")
  private String getPhoneFromSegment(SynchronizableBusinessObject item) {
    List<SynchronizableBusinessObject> phoneSegmentList = (List<SynchronizableBusinessObject>) item
        .getProperties().get(MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT);
    final boolean isAddressSegmentPresent = phoneSegmentList != null && !phoneSegmentList.isEmpty();
    if (isAddressSegmentPresent) {
      SynchronizableBusinessObject segment = phoneSegmentList.get(0);
      return (String) segment.getProperties().get(MappingConstants.SAP_WP_PER_PHONE);
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  private void setBusinessPartnerLocation(BusinessPartner businessPartner,
      SynchronizableBusinessObject itemValue) {
    List<SynchronizableBusinessObject> items = (List<SynchronizableBusinessObject>) itemValue
        .getProperties().get(MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT);
    if (items != null && !items.isEmpty()) {
      try {
        SynchronizableBusinessObject sbo = items.get(0);
        Location bpLocation = MappingUtils.getBusinessPartnerFiscalLocation(businessPartner);
        if (bpLocation == null) {
          bpLocation = createBusinessPartnerLocation(businessPartner, sbo);
        } else {
          setLocationInformation(bpLocation, sbo);
          setLocationAddressInformation(sbo, bpLocation.getLocationAddress());
        }
      } catch (UnsupportedOperationException e) {
        log.warn("The Business Partner has more than one fiscal address. Modifications will not be imported.");
      }
    }
  }

  private void setLocationInformation(Location businessPartnerLocation,
      SynchronizableBusinessObject sbo) {
    if (businessPartnerLocation != null) {
      businessPartnerLocation.setPhone((String) sbo.getProperties().get(
          MappingConstants.SAP_WP_PER_PHONE));
      businessPartnerLocation.setFax((String) sbo.getProperties().get(
          MappingConstants.SAP_WP_PER_FAX));
      businessPartnerLocation.setName(getBusinessPartnerLocationName(sbo));
    }
  }

  private Location createBusinessPartnerLocation(BusinessPartner businessPartner,
      SynchronizableBusinessObject sbo) {
    Location businessPartnerLocation = OBProvider.getInstance().get(Location.class);
    businessPartnerLocation.setNewOBObject(true);
    businessPartnerLocation.setLocationAddress(createLocationAddress(sbo));
    businessPartnerLocation.setName(getBusinessPartnerLocationName(sbo));
    businessPartnerLocation.setPayFromAddress(true);
    businessPartnerLocation.setShipToAddress(true);
    businessPartnerLocation.setInvoiceToAddress(true);
    businessPartnerLocation.setRemitToAddress(true);
    businessPartnerLocation.setTaxLocation(false);
    businessPartnerLocation.setBusinessPartner(businessPartner);
    businessPartnerLocation.setSapobmpIsreadytoexport(false);
    setLocationInformation(businessPartnerLocation, sbo);
    OBDal.getInstance().save(businessPartnerLocation);
    businessPartner.getBusinessPartnerLocationList().add(businessPartnerLocation);

    return businessPartnerLocation;
  }

  private org.openbravo.model.common.geography.Location createLocationAddress(
      SynchronizableBusinessObject sbo) {
    org.openbravo.model.common.geography.Location locationAddress = OBProvider.getInstance().get(
        org.openbravo.model.common.geography.Location.class);
    locationAddress.setNewOBObject(true);
    locationAddress.setCountry(OBDal.getInstance().getProxy(Country.class,
        MappingConstants.DEFAULT_COUNTRY_ID));
    setLocationAddressInformation(sbo, locationAddress);
    OBDal.getInstance().save(locationAddress);

    return locationAddress;
  }

  private void setLocationAddressInformation(SynchronizableBusinessObject sbo,
      org.openbravo.model.common.geography.Location locationAddress) {
    locationAddress.setPostalCode((String) sbo.getProperties()
        .get(MappingConstants.SAP_POSTAL_CODE));
    locationAddress.setCityName((String) sbo.getProperties().get(MappingConstants.SAP_CITY));
    locationAddress.setAddressLine1((String) sbo.getProperties().get(MappingConstants.SAP_ADDRESS));
  }

  private String getBusinessPartnerLocationName(SynchronizableBusinessObject sbo) {
    return StringUtils.defaultIfBlank((String) sbo.getProperties()
        .get(MappingConstants.SAP_ADDRESS), StringUtils.EMPTY)
        + " - "
        + StringUtils.defaultIfBlank((String) sbo.getProperties().get(MappingConstants.SAP_CITY),
            StringUtils.EMPTY);
  }
}
