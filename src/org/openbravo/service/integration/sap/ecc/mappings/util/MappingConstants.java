/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.util;

/**
 * Constants used in mappings
 *
 */
public class MappingConstants {

  public static final String SAP_CITY = "ORT";
  public static final String SAP_POSTAL_CODE = "PLZ";
  public static final String SAP_ADDRESS = "STRASSE";
  public static final String SAP_WP_PER_PHONE = "TELEFON";
  public static final String SAP_ORGANIZATION = "FILIALE";
  public static final String SAP_NAME = "NAME";
  public static final String SAP_ISACTIVE = "AENDKENNZ";
  // Spain
  public static final String DEFAULT_COUNTRY_ID = "106";
  // Default Payment Term
  public static final String DEFAULT_PAYMENT_TERM_ID = "05AC30EB446D4F3CBC4A2F02D10DFEE5";
  // Default Assortment
  public static final Object DEFAULT_ASSORTMENT_ID = "964BF8777D9842739FE57ACE9CA2F670";
  public static final String SAP_ACTIVE = "MODI";
  public static final String SAP_INVOICE_TOTAL_PAID = "SUMME";
  public static final String SAP_INVOICELINE_TAX_AMOUNT = "KONDVALUETAXAMT";
  public static final String SAP_INVOICELINE_GROSS_AMOUNT = "KONDVALUEGROSS";
  public static final String SAP_INVOICELINE_RETURN_SIGN = "VORZEICHEN";
  public static final String SAP_INVOICELINE_RETURN_CODE = "VORGANGART";
  public static final String SAP_INVOICELINE_PRODUCT_CODE_VALUE = "ARTNR";
  public static final String SAP_INVOICELINE_PRODUCT_CODE = "QUALARTNR";
  public static final String DEFAULT_BP_CATEGORY_ID = "FF8B22902DE7487881C5EB63FC0675B3";
  public static final String SAP_TAX_RATE = "MWSKZ";
  public static final String SAP_CASHUP_UPDATE = "BELEGDATUM";
  public static final String SAP_INVOICELINE_SEGMENT = "E1WPU02";
  public static final String SAP_INVOICELINE_QTYINVOICED = "UMSMENGE";
  public static final String SAP_INVOICELINE_WPUUMS_LINE_GROSS_AMOUNT = "UMSWERT";
  public static final String SAP_INVOICELINE_WPUUMS_RETURNORSALE = "VORZMENGE";
  public static final String SAP_INVOICELINE_WPUUMS_TAXVALUE = "TAXVALUE";
  public static final String SAP_WPUTAB_TOTALPAID = "SUMME";
  public static final String SAP_DATE_WPUWBW = "BELEGDATUM";
  public static final String SAP_LINE_WPUWBW_SEGMENT = "E1WPG02";
  public static final String SAP_WPUWBW_POREFERENCE = "REFERENZNR";
  public static final String SAP_WPUWBW_LINENO = "LITEM";
  public static final String SAP_WPUWBW_PRODUCT = "ARTNR";
  public static final String SAP_WPUWBW_MOVEMENT_TYPE = "BEWART";
  public static final String SAP_WPUWBW_UOM_EDI_CODE = "MEINH";
  public static final String SAP_WPUWBW_MOVEMENT_QTY = "MENGE";
  public static final String SAP_WPUWBW_BP_SEARCH_KEY = "LIFNR";
  public static final String SAP_WPUWBW_ORGANIZATION_TO = "ZFILIALE";
  public static final String SAP_WPUWBW_WAREHOUSE = "LGORT";
  public static final String SAP_WVINVE_WAREHOUSE = "LGORT";
  public static final String SAP_WVINVE_FISCAL_YEAR = "GJAHR";
  public static final String SAP_WVINVE_PLANNED_MOVEMENT_DATE = "GIDAT";
  public static final String SAP_WVINVE_NAME = "XBLNI";
  public static final String SAP_WVINVE_CREATED = "DNAME";
  public static final String SAP_WVINVE_MOVEMENT_DATE = "DDATE";
  public static final String SAP_WVINVE_TIME_OF_MOVEMENT_DATE = "DTIME";
  public static final String SAP_WVINVE_LINE = "E1WVINI";
  public static final String SAP_WVINVE_LINE_NO = "ZEILI";
  public static final String SAP_WVINVE_PRODUCT = "ARTNR";
  public static final String SAP_WVINVE_QUANTITY = "ERFMG";
  public static final String SAP_WVINVE_UOM = "ERFME";
  public static final String SAP_WPDTAX_VALIDFROM = "AKTIVDATUM";
  public static final String SAP_WPDTAX_TAX_RATE_SEGMENT = "E1WPT02";
  public static final String SAP_WPDTAX_DESCRIPTION = "BEZEICH";
  public static final String SAP_WPDTAX_TAX_RATE = "SATZ";
  public static final String SAP_WP_PER_NAME_SEGMENT = "E1WPP02";
  public static final String SAP_WP_PER_ADDRESS_SEGMENT = "E1WPP03";
  public static final String SAP_WP_PER_PAYMENT_METHOD_SEGMENT = "E1WPP05";
  public static final String SAP_WP_PER_FAX = "FAX";
  public static final String SAP_WP_PER_SEARCH_KEY = "PERSNR";
  public static final String SAP_WP_PLU_UOM = "POSME";
  public static final String SAP_WP_PLU_PRODUCT_CATEGORY_SEGMENT = "E1WPA02";
  public static final String SAP_WP_PLU_NAME_SEGMENT = "E1WPA03";
  public static final String SAP_WP_PLU_TAX_SEGMENT = "E1WPA07";
  public static final String SAP_WP_PLU_PRICE_SEGMENT = "E1WPA04";
  public static final String SAP_WP_PLU_PRICE_SUBSEGMENT = "E1WPA05";
  public static final String SAP_WP_PLU_PRICE = "KONDWERT";
  public static final String SAP_WP_PLU_PRICE_LIST_NAME = "KONDART";
  public static final String SAP_WP_PLU_VALID_FROM = "BEGINDATUM";
  public static final String SAP_WP_PLU_CURRENCY = "CURRENCY";
  public static final String SAP_WP_PLU_NAME_LENGTH = "QUALARTTXT";
  public static final String SAP_WP_PLU_LONG_NAME = "LTXT";
  public static final String SAP_WP_PLU_NAME = "TEXT";
  public static final String SAP_WP_PLU_SHORT_NAME = "02";
  public static final String SAP_WP_PLU_TAX = "MWSKZ";
  public static final String SAP_WPDCUR_CONVERSION_RATE_SEGMENT = "E1WPC02";
  public static final String SAP_WPDCUR_MULTIPLYBY_RATE = "KURS";
  public static final String SAP_WPDCUR_TO_CURRENCY_ISOCODE = "ZWAEHRUNG";
  public static final String SAP_WPDCUR_DESCRIPTION = "BEZEICH";
  public static final String SAP_WPDWGR_SEGMENT_E1WPW02 = "E1WPW02";
  public static final String SAP_WPDWGR_DESCRIPTION = "BEZEICH";
  public static final String SAP_WPDWGR_HIERARCHY = "HIERARCHIE";
  public static final String SAP_WPUBON_SAPTERMINAL = "KASSID";
  public static final String DEFAULT_ASSORTMENT_NAME = "SAPECC Default Assortment";
  public static final String DEFAULT_BP_CATEGORY_NAME = "POS Customer";
  public static final String SAP_INVOICE_PRICELIST_SAP_CODE_FIELD = "SAP_INVOICE_PRICELIST_SAP_CODE";
  public static final String SAP_CODE_FIELD = "sapobmpSapCode";
  public static final String SAP_INVOICE_PAYMENT_METHOD_SAP_CODE_FIELD = "ZAHLART";
  public static final String SAP_INVOICE_PAYMENT_METHOD_SEGMENT = "E1WPB06";
  public static final String SAP_WPUTAB_PAYMENT_METHOD_FIELD = "ZAHLART";
  public static final String SAP_LINE_CASHUP_SEGMENT = "E1WPZ02";
  public static final String IGNORE_ORGANIZATION_MAPPING_PREFERENCE = "SAPOBMP_IgnoreMappingForOrganization";
  public static final String SAP_INVOICE_PAYMENT_METHOD_CURRENCY = "WAEHRUNG";
  public static final String SAP_WVINVE_FILIALE = "SNDPRN";
  public static final String SAP_INVOICE_SERIAL_NUMBER = "POSSERNR";
  public static final String SAP_INVOICELINE_DISCOUNT_SIGN = "SEGMENT03VORZEICHEN";
  public static final String SAP_INVOICELINE_PRICE_SIGN = "VORZEICHENPRICE";
  public static final String SAP_INVOICELINE_DISCOUNT_SEGMENT = "SEGMENT03DISCOUNTS";
  public static final String SAP_INVOICELINE_DISCOUNT_SAP_CODE_FIELD = "DISCOUNTSAPCODE";
  public static final String SAP_INVOICELINE_DISCOUNT_AMOUNT = "DISCOUNTAMOUNT";
  public static final String SAP_INVOICELINE_TAX_RATE = "SEGMENT03TAXRATE";
  public static final String SAP_INVOICELINE_QUANTITY = "MENGE";
  public static final String SAP_WPUTAB_CURRENCY = "WAEHRUNG";
  public static final String SAP_WPUTAB_SIGN = "VORZEICHEN";
  public static final String SAP_INVOICELINE_WPUUMS_TAXCODE = "TAXCODE";
  public static final String SAP_CASHUP_TERMINAL = "KASSID";
  public static final String SAP_CASHUP_CASHIER = "KASSIERER";
  public static final String SAP_INVOICE_TOTAL_PAID_SIGN = "E1WPB06VORZEICHEN";
  public static final String SAP_INVOICE_PAYMENT_METHOD_CCDETAIL = "KARTENNR";
  public static final String SAP_INVOICE_PAYMENT_METHOD_CCAUTHCODE = "ZUONR";
  public static final String SAP_INVOICE_PAYMENT_METHOD_SHOWCC = "SHOWCC";
  public static final String SAP_INACTIVE = "DELE";
  public static final String SAP_WP_PLU_CREATION_DATE = "AKTIVDATUM";
  public static final String SAP_WP_PLU_EAN = "HAUPTEAN";
  public static final String SAP_WP_PLU_SEARCH_KEY = "ARTIKELNR";
  public static final String SAP_WP_PLU_PRODUCT_CATEGORY = "WARENGR";
  public static final String SAP_WP_PLU_NAME_SPLIT_INDEX = "LFDNR";
  public static final String SAP_WP_PLU_NAME_LANGUAGE = "SPRASCODE";
  public static final String SAP_INVOICE_USER_NAME = "KASSIERER";
  public static final String SAP_INVOICE_DOCNO = "BONNUMMER";
  public static final String DEFAULT_PRICE_LIST_SAP_CODE_VALUE = "PN10";
  public static final String DEFAULT_PAYMENT_METHOD_SAP_CODE_VALUE = "PTCS";
  public static final String ISREADYTOEXPORT_PROPERTY_NAME = "sapobmpIsreadytoexport";
  public static final String SAP_INVOICEDATE_DATE = "VORGDATUM";

}
