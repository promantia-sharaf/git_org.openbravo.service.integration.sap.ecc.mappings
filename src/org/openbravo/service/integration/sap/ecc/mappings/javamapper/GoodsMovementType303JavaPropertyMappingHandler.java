/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for 303 Goods Movements type entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = InternalMovement.ENTITY_NAME)
public class GoodsMovementType303JavaPropertyMappingHandler extends
    JavaPropertyMappingHandler<InternalMovement> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_DATE_WPUWBW);
    properties.put(21, MappingConstants.SAP_LINE_WPUWBW_SEGMENT);
    properties.put(31, MappingConstants.SAP_ORGANIZATION);
    return properties;
  }

  @Override
  public Object getValueOfProperty(InternalMovement bob, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return getFiliale(bob);
    case MappingConstants.SAP_DATE_WPUWBW:
      return getDate(bob);
    case MappingConstants.SAP_LINE_WPUWBW_SEGMENT:
      return getLinesForReceipOrtRtvShipment(bob);
    default:
      return null;
    }
  }

  private Object getLinesForReceipOrtRtvShipment(InternalMovement internalMovement) {
    List<SynchronizableBusinessObject> sboList = new ArrayList<>();
    for (InternalMovementLine internalMovementLine : internalMovement
        .getMaterialMgmtInternalMovementLineList()) {
      sboList.add(getSboForReceipOrtRtvShipment(internalMovementLine));
    }
    return sboList;
  }

  protected SynchronizableBusinessObject getSboForReceipOrtRtvShipment(
      InternalMovementLine internalMovementLine) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WPUWBW_LINENO, internalMovementLine.getLineNo());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_PRODUCT, internalMovementLine.getProduct()
        .getSearchKey());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_MOVEMENT_TYPE, "303");
    sbo.addProperty(MappingConstants.SAP_WPUWBW_UOM_EDI_CODE, internalMovementLine.getUOM()
        .getEDICode());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_MOVEMENT_QTY,
        internalMovementLine.getMovementQuantity());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_ORGANIZATION_TO, internalMovementLine
        .getNewStorageBin().getWarehouse().getOrganization().getSapobmpFiliale());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_WAREHOUSE,
        MappingUtils.getSAPCode(internalMovementLine.getStorageBin().getWarehouse()));
    return sbo;
  }

  private Object getDate(InternalMovement bob) {
    return IdocDateUtils.getDatePart(bob.getMovementDate());
  }

  private Object getFiliale(InternalMovement internalMovement) {
    List<InternalMovementLine> internalMovements = internalMovement
        .getMaterialMgmtInternalMovementLineList();
    if (internalMovements.isEmpty()) {
      return "";
    } else {
      return MappingUtils.getSAPFiliale(internalMovements.get(0).getStorageBin().getWarehouse());
    }
  }

}
