/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the WP_PER template
 */
@ApplicationScoped
@IdocType("WP_PER")
public class BusinessPartnerTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "7176FD8BCB70405895A95A7FEB8771C8";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getSearchKey() {
    return (String) getProperties().get(MappingConstants.SAP_WP_PER_SEARCH_KEY);
  }

  public String getName() {
    return (String) getProperties().get(MappingConstants.SAP_NAME);
  }

  @SuppressWarnings("unchecked")
  public List<BusinessPartnerLocationTemplateComponent> getlocations() {
    List<BusinessPartnerLocationTemplateComponent> locationList = new ArrayList<>();
    List<SynchronizableBusinessObject> locationsSBOs = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT);

    if (locationsSBOs == null) {
      return locationList;
    }
    for (SynchronizableBusinessObject locationSBO : locationsSBOs) {
      BusinessPartnerLocationTemplateComponent location = new BusinessPartnerLocationTemplateComponent(
          locationSBO);
      locationList.add(location);
    }

    return locationList;
  }

}
