/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPB03 corresponding to Discount lines of the PUBON template
 */
public class PubonInvoiceDiscount implements PropertiesHolder {

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public PubonInvoiceDiscount(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getDiscountSapCode() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICELINE_DISCOUNT_SAP_CODE_FIELD),
        StringUtils.EMPTY);
  }

  public String getDiscountAmount() {
    BigDecimal discountAmount = (BigDecimal) properties
        .get(MappingConstants.SAP_INVOICELINE_DISCOUNT_AMOUNT);
    if (discountAmount == null) {
      return "NULL";
    }
    return discountAmount.toString();
  }

  public String getDiscountSign() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_DISCOUNT_SIGN);
  }
}
