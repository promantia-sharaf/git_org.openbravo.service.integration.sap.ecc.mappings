/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.initializer;

import java.math.BigDecimal;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectInitializer;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Initialize Product Category entities with default values
 *
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = ProductCategory.ENTITY_NAME)
public class ProductCategoryOBObjectInitializer implements ImportedBaseOBObjectInitializer {

  @Override
  public void initialize(BaseOBObject bob) {
    ProductCategory productCategory = (ProductCategory) bob;
    productCategory.setPlannedMargin(BigDecimal.ZERO);
  }

}
