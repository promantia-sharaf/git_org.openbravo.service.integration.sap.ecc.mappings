/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;

/**
 * Base template to be used to generate WPUWBW iDocs 
 *
 */
public abstract class StockTransferComponentTemplate extends IdocTemplate {

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getMovementDate() {
    return (String) getProperties().get(MappingConstants.SAP_DATE_WPUWBW);
  }

  @SuppressWarnings("unchecked")
  public List<StockTransferLineTemplateComponent> getLines() {
    List<StockTransferLineTemplateComponent> lines = new ArrayList<>();
    List<SynchronizableBusinessObject> linesSbo = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_LINE_WPUWBW_SEGMENT);
    if (linesSbo == null) {
      return lines;
    }
    for (SynchronizableBusinessObject sbo : linesSbo) {
      lines.add(new StockTransferLineTemplateComponent(sbo.getProperties()));
    }
    return lines;
  }

}
