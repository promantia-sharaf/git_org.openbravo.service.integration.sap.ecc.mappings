/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBInterceptor;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaPropertyMappingHandler for the Business Partner entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = BusinessPartner.ENTITY_NAME)
public class BusinessPartnerJavaPropertyMappingHandler extends
    JavaPropertyMappingHandler<BusinessPartner> {

  private static final Logger log = LoggerFactory
      .getLogger(BusinessPartnerJavaPropertyMappingHandler.class);

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_NAME);
    properties.put(31, MappingConstants.SAP_ISACTIVE);
    properties.put(41, MappingConstants.SAP_WP_PER_NAME_SEGMENT);
    properties.put(51, MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT);

    return properties;
  }

  @Override
  public void setPropertyInBaseOBObject(BusinessPartner businessPartner, String mappingName,
      SynchronizableBusinessObject sbo) {
    Map<String, Object> properties = sbo.getProperties();
    if (!properties.containsKey(mappingName)) {
      return;
    }
    try {
      Object itemValue = properties.get(mappingName);
      switch (mappingName) {
      case MappingConstants.SAP_ORGANIZATION:
        setOrganization(businessPartner, itemValue);
        setPriceList(businessPartner);
        break;
      case MappingConstants.SAP_ISACTIVE:
        businessPartner.setActive(StringUtils.equals(MappingConstants.SAP_ACTIVE,
            (String) itemValue));
        break;
      case MappingConstants.SAP_WP_PER_NAME_SEGMENT:
        setName(businessPartner, itemValue);
        break;
      default:
        log.warn("Java property not defined in the mapping: {}", mappingName);
      }
    } catch (Exception e) {
      log.error("Error while importing the property " + mappingName, e);
      throw new OBException(e);
    }
  }

  private void setOrganization(BusinessPartner businessPartner, Object itemValue) {
    businessPartner.setOrganization(MappingUtils.getOrganization((String) itemValue));
  }

  private void setPriceList(BusinessPartner businessPartner) {
    PriceList salesPriceList = getSalesPriceList();
    businessPartner.setPriceList(salesPriceList);
    businessPartner.setCurrency(salesPriceList.getCurrency());
  }

  private PriceList getSalesPriceList() {
    PriceList priceList = MappingUtils.geDefaultSalesPriceList();
    if (priceList == null) {
      priceList = getFirstSalesPriceList();
    }
    return priceList;
  }

  private PriceList getFirstSalesPriceList() {
    PriceList priceList;
    OBCriteria<PriceList> criteria = OBDal.getInstance().createCriteria(PriceList.class);
    criteria.add(Restrictions.eq(PriceList.PROPERTY_SALESPRICELIST, true));
    criteria.add(Restrictions.eq(PriceList.PROPERTY_ORGANIZATION + "." + Organization.PROPERTY_ID,
        "0"));
    criteria.setMaxResults(1);
    List<PriceList> list = criteria.list();
    if (list.isEmpty()) {
      throw new OBException("There is no sales Price List for organization \"*\"");
    }
    priceList = list.get(0);
    return priceList;
  }

  @SuppressWarnings("unchecked")
  private SynchronizableBusinessObject getSegment(Object itemValue) {
    List<SynchronizableBusinessObject> items = (List<SynchronizableBusinessObject>) itemValue;
    return items.get(0);
  }

  private void setName(BusinessPartner businessPartner, Object itemValue) {
    SynchronizableBusinessObject item = getSegment(itemValue);
    businessPartner.setName((String) item.getProperties().get(MappingConstants.SAP_NAME));
  }

  @Override
  public Object getValueOfProperty(BusinessPartner bob, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_WP_PER_ADDRESS_SEGMENT:
      final List<SynchronizableBusinessObject> businessPartnerContactData = getBusinessPartnerContactData(bob);
      setReadyToExport(bob);
      return businessPartnerContactData;
    case MappingConstants.SAP_NAME:
      return bob.getName();
    case MappingConstants.SAP_ORGANIZATION:
      return MappingUtils.getSAPFiliale(bob);
    default:
      return null;
    }
  }

  private void setReadyToExport(BusinessPartner bob) {
    OBInterceptor.setPreventUpdateInfoChange(true);
    bob.setSapobmpIsreadytoexport(false);
    OBDal.getInstance().flush();
    OBInterceptor.setPreventUpdateInfoChange(false);
  }

  private List<SynchronizableBusinessObject> getBusinessPartnerContactData(BusinessPartner bob) {
    List<SynchronizableBusinessObject> address = new ArrayList<>();
    try {
      Location location = getValidLocationFromBusinessPartner(bob);
      if (location != null) {
        SynchronizableBusinessObject sbo = createSboFromBusinessPartnerLocation(bob, location);
        setLocationAsAlreadyExported(location);
        if (!sbo.getProperties().isEmpty()) {
          address.add(sbo);
        }
      }
    } catch (UnsupportedOperationException e) {
      log.warn("The Business Partner has more than one fiscal address. Address segment in iDoc will be empty.");
      if (bob.isSapobmpIsreadytoexport()) {
        address.clear();
        return address;
      }
      throw new OBException(e);
    }
    return address;
  }

  /**
   * Get the Business Partner Location to be exported
   * 
   * @param bob
   *          The Business Partner
   * @return The Business Partner Location
   */
  protected Location getValidLocationFromBusinessPartner(BusinessPartner bob) {
    return MappingUtils.getBusinessPartnerFiscalLocation(bob);
  }

  private void setLocationAsAlreadyExported(Location location) {
    OBInterceptor.setPreventUpdateInfoChange(true);
    location.setSapobmpIsreadytoexport(false);
    OBDal.getInstance().flush();
    OBInterceptor.setPreventUpdateInfoChange(false);
  }

  private SynchronizableBusinessObject createSboFromBusinessPartnerLocation(BusinessPartner bob,
      Location location) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    addToSynchronizedBusinessObjectIfNotNull(sbo, MappingConstants.SAP_WP_PER_PHONE,
        getTelephoneFromContact(bob));
    addToSynchronizedBusinessObjectIfNotNull(sbo, MappingConstants.SAP_WP_PER_FAX,
        location.getFax());

    org.openbravo.model.common.geography.Location locationAddress = location.getLocationAddress();
    addToSynchronizedBusinessObjectIfNotNull(sbo, MappingConstants.SAP_CITY,
        locationAddress.getCityName());
    addToSynchronizedBusinessObjectIfNotNull(sbo, MappingConstants.SAP_POSTAL_CODE,
        locationAddress.getPostalCode());
    addToSynchronizedBusinessObjectIfNotNull(sbo, MappingConstants.SAP_ADDRESS,
        getAddress(locationAddress));
    return sbo;
  }

  private Object getTelephoneFromContact(BusinessPartner bob) {
    if (!bob.getADUserList().isEmpty()) {
      return MappingUtils.getContactShownInWebPos(bob).getPhone();
    }
    return null;
  }

  private Object getAddress(org.openbravo.model.common.geography.Location locationAddress) {
    if (locationAddress.getAddressLine2() != null) {
      return locationAddress.getAddressLine1() + " - " + locationAddress.getAddressLine2();
    }
    return locationAddress.getAddressLine1();
  }

  private void addToSynchronizedBusinessObjectIfNotNull(SynchronizableBusinessObject sbo,
      String property, Object value) {
    if (value != null) {
      sbo.addProperty(property, value);
    }
  }
}
