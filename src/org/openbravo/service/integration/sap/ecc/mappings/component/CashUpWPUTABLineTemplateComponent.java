/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPZ02 in WPUTAB template 
 *
 */
public class CashUpWPUTABLineTemplateComponent implements PropertiesHolder {
  Map<String, Object> properties;

  public CashUpWPUTABLineTemplateComponent(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getPaymentMethod() {
    return (String) properties.get(MappingConstants.SAP_WPUTAB_PAYMENT_METHOD_FIELD);
  }

  public String getTotalPaid() {
    BigDecimal totalPaid = (BigDecimal) properties.get(MappingConstants.SAP_WPUTAB_TOTALPAID);
    return totalPaid.toString();
  }

  public String getCurrency() {
    return (String) properties.get(MappingConstants.SAP_WPUTAB_CURRENCY);
  }

  public String getSign() {
    return (String) properties.get(MappingConstants.SAP_WPUTAB_SIGN);
  }

  @Override
  public Map<String, Object> getProperties() {
    return this.properties;
  }

}
