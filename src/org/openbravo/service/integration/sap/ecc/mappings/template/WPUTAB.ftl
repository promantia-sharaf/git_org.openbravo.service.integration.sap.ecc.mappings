<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

-->
<?xml version="1.0" encoding="utf-8"?>
<WPUTAB01>
  <IDOC BEGIN="1">
    <EDI_DC40 SEGMENT="1">
      <IDOCTYP>WPUTAB01</IDOCTYP>
      <MESTYP>WPUTAB</MESTYP>
      <STDMES>WPUTAB</STDMES>
      <SNDPOR>WPUX</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <SNDPRN>${data.filiale}</SNDPRN>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>KU</RCVPRT>
      <RCVPRN>${data.filiale}</RCVPRN>
    </EDI_DC40>
    <E1WPZ01 SEGMENT="1">
      <BELEGDATUM>${data.cashUpUpdate}</BELEGDATUM>
      <KASSID>${data.terminal}</KASSID>
      <KASSIERER>${data.cashier}</KASSIERER>
    <#list data.lines as line>
      <E1WPZ02 SEGMENT="1">
        <ZAHLART>${line.paymentMethod}</ZAHLART>
        <SUMME>${line.totalPaid}</SUMME>
        <WAEHRUNG>${line.currency}</WAEHRUNG>
        <VORZEICHEN>${line.sign}</VORZEICHEN>
      </E1WPZ02>
    </#list>
    </E1WPZ01>
  </IDOC>
</WPUTAB01>