/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the 303 WPUWBW template
 */
@ApplicationScoped
@IdocType("WPUWBW303")
public class GoodsMovement303TemplateComponent extends StockTransferComponentTemplate {

  private static final String TEMPLATE_ID = "AE31C94B37264A2D9D94AF9725F1402B";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

}
