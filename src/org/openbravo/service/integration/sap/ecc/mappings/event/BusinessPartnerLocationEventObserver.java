/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.event;

import java.util.Date;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * Prevents Business Partner Location be set as ready to export when is modified as part of the
 * export flow.
 *
 */
public class BusinessPartnerLocationEventObserver extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Location.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    /*
     * When exporting this entity, it is set as not ready to be exported without changing the
     * updated field, which causes a call to onUpdate method anyway. This check allows setting the
     * entity as ready to export when onUpdate is called because of a modification that provoques
     * change in updated field.
     */
    if (locationHasBeenModified(event)) {
      setBusinessPartnerLocationAsReadyToExport(event);
    }
  }

  private void setBusinessPartnerLocationAsReadyToExport(EntityPersistenceEvent event) {
    final Property isReadyToExportProperty = MappingUtils
        .getReadyToExportProperty(Location.ENTITY_NAME);
    event.setCurrentState(isReadyToExportProperty, true);
  }

  private boolean locationHasBeenModified(EntityUpdateEvent event) {
    final Entity bpEntity = ModelProvider.getInstance().getEntity(Location.ENTITY_NAME);
    final Property updated = bpEntity.getProperty(Location.PROPERTY_UPDATED);
    return ((Date) event.getPreviousState(updated)).getTime() != ((Date) event
        .getCurrentState(updated)).getTime();
  }

}
