/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import org.openbravo.retail.posterminal.OBPOSAppCashup;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.process.SapEccExporterSynchronizableBusinessObjectExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * SynchronizableBusinessObjectExporter that exports Aggregated Sales to WPUUMS documents
 * 
 * Returns the ids of Cashup generated from a terminal configured to group orders, since the last export.
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = OBPOSAppCashup.ENTITY_NAME)
public class CashUpExporter extends SapEccExporterSynchronizableBusinessObjectExporter {

  @Override
  public String exportedEntityName() {
    return OBPOSAppCashup.ENTITY_NAME;
  }

  @Override
  public String getSynchronizationQuery() {
    StringBuilder query = new StringBuilder();
    query.append(" SELECT cs.id from OBPOS_App_Cashup cs ");
    query.append(" WHERE cs.$incrementalUpdateCriteria ");
    query.append(" AND cs.isprocessedbo = true ");
    query.append(" AND cs.pOSTerminal.obposTerminaltype.groupingOrders = true ");
    query.append(" AND cs.$clientCriteria ");
    query
        .append(" AND EXISTS (select 1 from InvoiceLine il inner join il.salesOrderLine ol inner join ol.salesOrder so ");
    query.append("where so.$clientCriteria ");
    query.append("and so.obposAppCashup is not null ");
    query.append("and so.obposAppCashup = cs.id ");
    query.append("and il.invoice.salesOrder is null) ");

    return query.toString();
  }

  @Override
  protected String getIdocType() {
    return "WPUUMS";
  }

}
