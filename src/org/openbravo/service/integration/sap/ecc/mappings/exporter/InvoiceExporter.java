/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.process.SapEccExporterSynchronizableBusinessObjectExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * SynchronizableBusinessObjectExporter that exports Invoices to PUBON documents
 * 
 * Returns the id of processed Invoices generated directly by WebPOS and not exported yet since the last export.
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Invoice.ENTITY_NAME)
public class InvoiceExporter extends SapEccExporterSynchronizableBusinessObjectExporter {

  @Override
  public String exportedEntityName() {
    return Invoice.ENTITY_NAME;
  }

  @Override
  protected String getIdocType() {
    return "PUBON";
  }

  @Override
  public String getSynchronizationQuery() {
    final StringBuilder hql = new StringBuilder();
    hql.append(" SELECT i.id FROM Invoice i ");
    hql.append(" WHERE i.salesTransaction = true ");
    hql.append(" AND i.sapobmpIsExported = false ");
    hql.append(" AND i.$clientCriteria ");
    hql.append(" AND i.salesOrder is not null ");
    hql.append(" AND i.processed = true ");
    return hql.toString();
  }

}
