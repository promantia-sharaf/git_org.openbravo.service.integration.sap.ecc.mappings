/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper.processor;

import java.math.BigDecimal;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.service.integration.sap.ecc.mappings.CashUp;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * Class to compute sales by payment methods, based on CashUp
 *
 */
public class AggregatedSalesByPaymentMethod extends SalesByPaymentMethodProcessor {

  private CashUp cashUp;

  public AggregatedSalesByPaymentMethod(CashUp cashUp) {
    this.cashUp = cashUp;
  }

  @Override
  protected ScrollableResults getSalesGroupedByPaymentMethod() {
    StringBuilder hqlQuery = new StringBuilder();
    hqlQuery
        .append("select fpsd._computedColumns.aPRMPaymentMethod.id, fpsd._computedColumns.aPRMFinancialAccount.currency.iSOCode, sum(fpsd.amount) ");
    hqlQuery
        .append("from FIN_Payment_ScheduleDetail fpsd inner join fpsd.invoicePaymentSchedule fps ");
    hqlQuery.append("where fps.invoice.client.id = :clientId ");
    hqlQuery.append("and fps.invoice.salesOrder is null ");
    hqlQuery.append("and exists ( ");
    hqlQuery.append("  select 1 from InvoiceLine il ");
    hqlQuery.append("  where il.invoice.id = fps.invoice.id ");
    hqlQuery.append("  and il.salesOrderLine.salesOrder.obposAppCashup = :cashUpId ) ");
    hqlQuery
        .append("group by fpsd._computedColumns.aPRMPaymentMethod.id, fpsd._computedColumns.aPRMFinancialAccount.currency.iSOCode ");
    hqlQuery.append("having sum(fpsd.amount) <> 0 ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());

    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    query.setParameter("cashUpId", cashUp.getId());
    query.setFetchSize(1000);
    return query.scroll(ScrollMode.FORWARD_ONLY);
  }

  @Override
  public boolean hasSalesToProcess() {
    StringBuilder hqlQuery = new StringBuilder();
    hqlQuery
        .append("select count(*) as total from InvoiceLine il inner join il.salesOrderLine ol inner join ol.salesOrder so ");
    hqlQuery.append("where so.client.id = :clientId ");
    hqlQuery.append("and so.obposAppCashup is not null ");
    hqlQuery.append("and so.obposAppCashup = :cashUpId ");
    hqlQuery.append("and il.invoice.salesOrder is null ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    query.setParameter("cashUpId", cashUp.getId());

    Long total = (Long) query.uniqueResult();
    return total.longValue() > 0;
  }

  @Override
  protected String getTotalPaidSign(ScrollableResults totalPaidByPaymentMethod) {
    return getTotalPaid(totalPaidByPaymentMethod).signum() < 0 ? "-" : "+";
  }

  @Override
  protected BigDecimal getTotalPaid(ScrollableResults totalPaidByPaymentMethod) {
    return (BigDecimal) totalPaidByPaymentMethod.get()[2];
  }

  @Override
  protected String getCurrencyISOCode(ScrollableResults totalPaidByPaymentMethod) {
    return (String) totalPaidByPaymentMethod.get()[1];
  }

  @Override
  protected String getPaymentMethodSapCode(ScrollableResults totalPaidByPaymentMethod) {
    String paymentMethodId = (String) totalPaidByPaymentMethod.get()[0];
    FIN_PaymentMethod paymentMethod = OBDal.getInstance().get(FIN_PaymentMethod.class,
        paymentMethodId);
    return MappingUtils.getSAPCode(paymentMethod);
  }
}
