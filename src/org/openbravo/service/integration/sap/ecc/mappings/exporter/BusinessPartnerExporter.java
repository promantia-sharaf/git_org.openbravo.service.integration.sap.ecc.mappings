/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.process.SapEccExporterSynchronizableBusinessObjectExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * SynchronizableBusinessObjectExporter that exports Orders to WP_PER documents
 * 
 * Returns the id of Business Partners which have been modified since last export. The modifications
 * include thoose made in header, locations and contact.
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = BusinessPartner.ENTITY_NAME)
public class BusinessPartnerExporter extends SapEccExporterSynchronizableBusinessObjectExporter {

  @Override
  public String exportedEntityName() {
    return BusinessPartner.ENTITY_NAME;
  }

  @Override
  public String getSynchronizationQuery() {
    final StringBuilder query = new StringBuilder();
    query.append(" SELECT bp.id from BusinessPartner bp ");
    query.append(" WHERE ((bp.$incrementalUpdateCriteria AND bp.sapobmpIsreadytoexport = true) ");
    query
        .append(" OR exists (select 1 from bp.businessPartnerLocationList bpll WHERE "
            + "             (bpll.$incrementalUpdateCriteria AND bpll.sapobmpIsreadytoexport = true AND bpll.invoiceToAddress = true) "
            + "             OR ((bpll.locationAddress.$incrementalUpdateCriteria AND bpll.locationAddress.creationDate != bpll.locationAddress.updated) "
            + "                 AND bpll.invoiceToAddress = true ))");
    query
        .append(" OR exists (select 1 from bp.aDUserList adul WHERE (adul.$incrementalUpdateCriteria AND adul.creationDate != adul.updated) AND "
            + "             (adul.id = (select max(adu.id) from ADUser adu where adu.businessPartner.id = bp.id)) ))");
    query.append(" AND bp.$clientCriteria ");
    return query.toString();
  }

  @Override
  protected String getIdocType() {
    return "WP_PER";
  }

}
