<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

-->
<?xml version="1.0" encoding="utf-8"?>
<WVINVE03>
  <IDOC BEGIN="1">
    <EDI_DC40>
      <TABNAM>EDI_DC40</TABNAM>
      <MANDT>100</MANDT>
      <IDOCTYP>WVINVE03</IDOCTYP>
      <MESTYP>WVINVE</MESTYP>
      <SNDPOR>WPUX</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <SNDPRN>${data.filiale}</SNDPRN>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>LS</RCVPRT>
      <RCVPRN>OPENBRAVO1</RCVPRN>
    </EDI_DC40>
    <E1WVINH SEGMENT="1">
      <LGORT>${data.warehouse}</LGORT>
      <GJAHR>${data.fiscalYear}</GJAHR>
      <GIDAT>${data.movementDate}</GIDAT>
      <XBLNI>${data.name}</XBLNI>
      <DNAME>${data.userName}</DNAME>
      <DDATE>${data.movementDate}</DDATE>
      <DTIME>${data.timeOfMovementDate}</DTIME>
      <FILIALE>${data.filiale}</FILIALE>
    <#list data.lines as line>
      <E1WVINI SEGMENT="1">
        <ZEILI>${line.lineNo}</ZEILI>
        <QUALARTNR>006</QUALARTNR>
        <ARTNR>${line.product}</ARTNR>
        <ERFMG>${line.quantity}</ERFMG>
        <ERFME>${line.uomEdiCode}</ERFME>
      </E1WVINI>
    </#list>
    </E1WVINH>
  </IDOC>
</WVINVE03>
