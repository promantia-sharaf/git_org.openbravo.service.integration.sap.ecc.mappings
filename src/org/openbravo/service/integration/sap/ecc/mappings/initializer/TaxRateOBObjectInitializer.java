/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.initializer;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectInitializer;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Initialize Tax Rate entities with default values
 *
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = TaxRate.ENTITY_NAME)
public class TaxRateOBObjectInitializer implements ImportedBaseOBObjectInitializer {

  private static final String SALES_TAX = "S";

  @Override
  public void initialize(BaseOBObject bob) {
    TaxRate taxRate = (TaxRate) bob;
    taxRate.setSalesPurchaseType(SALES_TAX);
  }

}
