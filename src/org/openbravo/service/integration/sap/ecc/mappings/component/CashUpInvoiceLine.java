/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for the Invoice lines of the WPUUMS template
 */
public class CashUpInvoiceLine implements PropertiesHolder {

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public CashUpInvoiceLine(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getProduct() {
    return (String) this.properties.get(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE);
  }

  public String getQtyInvoiced() {
    BigDecimal qtyInvoiced = (BigDecimal) this.properties
        .get(MappingConstants.SAP_INVOICELINE_QTYINVOICED);
    return qtyInvoiced.toString();
  }

  public String getGrossAmount() {
    BigDecimal grossAmount = (BigDecimal) this.properties
        .get(MappingConstants.SAP_INVOICELINE_WPUUMS_LINE_GROSS_AMOUNT);
    return grossAmount.toString();
  }

  public String getReturnOrSale() {
    return StringUtils.defaultIfBlank(
        (String) this.properties.get(MappingConstants.SAP_INVOICELINE_WPUUMS_RETURNORSALE),
        StringUtils.EMPTY);
  }

  public String getTaxValue() {
    BigDecimal taxValue = (BigDecimal) this.properties
        .get(MappingConstants.SAP_INVOICELINE_WPUUMS_TAXVALUE);
    if (taxValue == null) {
      return StringUtils.EMPTY;
    }
    return taxValue.toString();
  }

  public String getTaxRateSapCode() {
    return StringUtils
        .defaultIfBlank((String) properties.get(MappingConstants.SAP_INVOICELINE_WPUUMS_TAXCODE),
            StringUtils.EMPTY);
  }

}
