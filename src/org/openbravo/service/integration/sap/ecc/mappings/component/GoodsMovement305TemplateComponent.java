/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the 305 WPUWBW template
 */
@ApplicationScoped
@IdocType("WPUWBW305")
public class GoodsMovement305TemplateComponent extends StockTransferComponentTemplate {

  private static final String TEMPLATE_ID = "97161B5FA71044FAA4FC0B83332679B0";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

}
