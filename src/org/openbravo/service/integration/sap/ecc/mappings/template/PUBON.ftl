<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


-->
<?xml version="1.0" encoding="utf-8"?>
<WPUBON01>
  <IDOC BEGIN="1">
    <EDI_DC40 SEGMENT="1">
      <TABNAM>EDI_DC40</TABNAM>
      <IDOCTYP>WPUBON01</IDOCTYP>
      <MESTYP>WPUBON</MESTYP>
      <SNDPOR>POS-OUT</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>KU</RCVPRT>
      <RCVPRN>${data.filiale}</RCVPRN>
      <SNDPRN>${data.filiale}</SNDPRN>
    </EDI_DC40>
    <E1WPB01 SEGMENT="1">
      <POSKREIS>1</POSKREIS>
      <KASSID>${data.posTerminal}</KASSID>
      <VORGDATUM>${data.orderDate}</VORGDATUM>
      <VORGZEIT>${data.orderTime}</VORGZEIT>
      <BONNUMMER>${data.documentNumber}</BONNUMMER>
      <KUNDNR>${data.businessPartner}</KUNDNR>
      <KASSIERER>${data.cashierUserName}</KASSIERER>
      <CSHNAME>${data.cashierName}</CSHNAME>
      <BELEGWAERS>${data.currency}</BELEGWAERS>
  <#list data.invoiceLines as line>   
      <E1WPB02 SEGMENT="1">
        <VORGANGART>${line.isReturn}</VORGANGART>
        <QUALARTNR>${line.productCode}</QUALARTNR>
        <ARTNR>${line.product}</ARTNR>
        <MENGE>${line.quantity}</MENGE>
        <VORZEICHEN>${line.sign}</VORZEICHEN>
      <#if line.serialNumber != "">
        <SERIENNR>${line.serialNumber}</SERIENNR>
      </#if>
        <E1WPB03 SEGMENT="1">
          <KONDITION>${line.priceListSapCode}</KONDITION>
          <KONDVALUE>${line.grossAmount}</KONDVALUE>
          <VORZEICHEN>${line.priceSign}</VORZEICHEN>
        </E1WPB03>
        <E1WPB03 SEGMENT="1">
          <KONDITION>${line.invoiceLineTaxRate}</KONDITION>
          <KONDVALUE>${line.taxAmount}</KONDVALUE>
          <VORZEICHEN>${line.priceSign}</VORZEICHEN>
        </E1WPB03>
        <#list line.discounts as discount>
        <E1WPB03 SEGMENT="1">
          <KONDITION>${discount.discountSapCode}</KONDITION>
          <KONDVALUE>${discount.discountAmount}</KONDVALUE>
          <VORZEICHEN>${discount.discountSign}</VORZEICHEN>
        </E1WPB03>
        </#list>
      <#if line.taxRate != "">
        <E1WPB04 SEGMENT="1">
          <MWSKZ>${line.taxRate}</MWSKZ>
        </E1WPB04>      
      </#if>
      </E1WPB02>
  </#list>
  <#list data.paymentMethodLines as paymentMethodline> 
      <E1WPB06 SEGMENT="1">
        <ZAHLART>${paymentMethodline.paymentMethodSapCode}</ZAHLART>
        <SUMME>${paymentMethodline.totalPaid}</SUMME>
        <WAEHRUNG>${paymentMethodline.currency}</WAEHRUNG>
        <VORZEICHEN>${paymentMethodline.paymentMethodSign}</VORZEICHEN>
      <#if paymentMethodline.showCreditCardInfo == true>
        <KARTENNR>${paymentMethodline.creditCardDetail}</KARTENNR>
        <ZUONR>${paymentMethodline.creditCardAuth}</ZUONR>
      </#if>
      </E1WPB06>
  </#list>
    </E1WPB01>
  </IDOC>
</WPUBON01>