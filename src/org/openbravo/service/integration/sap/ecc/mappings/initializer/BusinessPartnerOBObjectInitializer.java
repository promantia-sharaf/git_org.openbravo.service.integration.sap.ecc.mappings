/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.initializer;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.service.external.integration.importer.ImportedBaseOBObjectInitializer;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Initialize Business Partner entities with default values
 *
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = BusinessPartner.ENTITY_NAME)
public class BusinessPartnerOBObjectInitializer implements ImportedBaseOBObjectInitializer {

  private static final String PAYMENT_TERM_SEARCH_KEY = "SAP_PAYMENT_TERM";
  private static final String PAYMENT_TERM_NAME = "Payment Term";
  private static final String WITHDRAWN_PAYMENT_ACCOUNT = "WIT";
  private static final String DEPOSITED_PAYMENT_ACCOUNT = "DEP";
  private static final String DEFAULT_PAYMENT_METHOD_NAME = "SAP Payment Method";

  @Override
  public void initialize(BaseOBObject bob) {
    BusinessPartner businessPartner = (BusinessPartner) bob;
    businessPartner.setPurchaseOrder(false);
    businessPartner.setPurchaseInvoice(false);
    businessPartner.setPaymentOut(false);
    businessPartner.setSalesOrder(false);
    businessPartner.setGoodsShipment(false);
    businessPartner.setSalesInvoice(false);
    businessPartner.setPotentialCustomer(false);
    businessPartner.setPrintDiscount(false);
    businessPartner.setBusinessPartnerCategory(getBusinessPartnerGroup());
    businessPartner.setPaymentMethod(getPaymentMethod());
    businessPartner.setPaymentTerms(getPaymentTerm());
    businessPartner.setSapobmpIsreadytoexport(false);
  }

  private static synchronized Category getBusinessPartnerGroup() {
    Category bpCategory = OBDal.getInstance().get(Category.class,
        MappingConstants.DEFAULT_BP_CATEGORY_ID);
    if (bpCategory == null) {
      bpCategory = createDefaultBPCategory();
    }
    return bpCategory;
  }

  private static Category createDefaultBPCategory() {
    Category bpDefaultCategory = OBProvider.getInstance().get(Category.class);
    bpDefaultCategory.setId(MappingConstants.DEFAULT_BP_CATEGORY_ID);
    bpDefaultCategory.setNewOBObject(true);
    bpDefaultCategory.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    bpDefaultCategory.setName(MappingConstants.DEFAULT_BP_CATEGORY_NAME);
    bpDefaultCategory.setSearchKey(MappingConstants.DEFAULT_BP_CATEGORY_NAME);
    OBDal.getInstance().save(bpDefaultCategory);
    return bpDefaultCategory;
  }

  private static synchronized FIN_PaymentMethod getPaymentMethod() {
    FIN_PaymentMethod defaultPaymentMethod = getDefaultPaymentMethod();
    if (defaultPaymentMethod == null) {
      defaultPaymentMethod = createDefaultPaymentMethod();
    }
    return defaultPaymentMethod;
  }

  private static FIN_PaymentMethod createDefaultPaymentMethod() {
    FIN_PaymentMethod defaultPaymentMethod = OBProvider.getInstance().get(FIN_PaymentMethod.class);
    defaultPaymentMethod.setNewOBObject(true);
    defaultPaymentMethod.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    defaultPaymentMethod.setName(DEFAULT_PAYMENT_METHOD_NAME);
    defaultPaymentMethod.setUponDepositUse(DEPOSITED_PAYMENT_ACCOUNT);
    defaultPaymentMethod.setUponWithdrawalUse(WITHDRAWN_PAYMENT_ACCOUNT);
    defaultPaymentMethod.setSapobmpSapCode(MappingConstants.DEFAULT_PAYMENT_METHOD_SAP_CODE_VALUE);
    defaultPaymentMethod.setObposBankable(false);
    OBDal.getInstance().save(defaultPaymentMethod);
    return defaultPaymentMethod;
  }

  private static FIN_PaymentMethod getDefaultPaymentMethod() {
    OBCriteria<FIN_PaymentMethod> criteria = OBDal.getInstance().createCriteria(
        FIN_PaymentMethod.class);
    criteria.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_SAPOBMPSAPCODE,
        MappingConstants.DEFAULT_PAYMENT_METHOD_SAP_CODE_VALUE));
    criteria.setMaxResults(1);
    return (FIN_PaymentMethod) criteria.uniqueResult();
  }

  private static synchronized PaymentTerm getPaymentTerm() {
    PaymentTerm defaultPaymentTerm = getDefaultPaymentTerm();
    if (defaultPaymentTerm == null) {
      defaultPaymentTerm = createDefaultPaymentTerm();
    }
    return defaultPaymentTerm;
  }

  private static PaymentTerm createDefaultPaymentTerm() {
    PaymentTerm defaultPaymentTerm = OBProvider.getInstance().get(PaymentTerm.class);
    defaultPaymentTerm.setId(MappingConstants.DEFAULT_PAYMENT_TERM_ID);
    defaultPaymentTerm.setNewOBObject(true);
    defaultPaymentTerm.setName(PAYMENT_TERM_NAME);
    defaultPaymentTerm.setSearchKey(PAYMENT_TERM_SEARCH_KEY);
    defaultPaymentTerm.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    defaultPaymentTerm.setOverduePaymentDaysRule(30L);
    defaultPaymentTerm.setOffsetMonthDue(0L);
    defaultPaymentTerm.setNextBusinessDay(true);
    OBDal.getInstance().save(defaultPaymentTerm);
    return defaultPaymentTerm;
  }

  private static PaymentTerm getDefaultPaymentTerm() {
    return OBDal.getInstance().get(PaymentTerm.class, MappingConstants.DEFAULT_PAYMENT_TERM_ID);
  }
}
