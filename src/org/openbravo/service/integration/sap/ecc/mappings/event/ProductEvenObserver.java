/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.event;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.plm.Product;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * Prevents Product be set as ready to export when is modified by background processes
 *
 */
public class ProductEvenObserver extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Product.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    if (!productWasReadyToExport(event)) {
      setProductReadyToExport(event);
    }
  }

  private void setProductReadyToExport(EntityPersistenceEvent event) {
    final Property isReadyToExportProperty = MappingUtils
        .getReadyToExportProperty(Product.ENTITY_NAME);
    event.setCurrentState(isReadyToExportProperty, true);
  }

  private boolean productWasReadyToExport(EntityUpdateEvent event) {
    final Property isReadyToExportProperty = MappingUtils
        .getReadyToExportProperty(Product.ENTITY_NAME);
    return (Boolean) event.getPreviousState(isReadyToExportProperty);
  }

}
