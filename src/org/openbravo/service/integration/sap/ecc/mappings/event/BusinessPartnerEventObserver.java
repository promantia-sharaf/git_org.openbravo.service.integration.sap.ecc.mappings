/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.event;

import java.math.BigDecimal;

import javax.enterprise.event.Observes;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * Prevent Business Partner be set as ready to export when is modified at ticket processing in
 * WebPOS
 *
 */
public class BusinessPartnerEventObserver extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      BusinessPartner.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    if (businessPartnerShouldBeMarkedAsReadyToExport(event)) {
      setBusinessPartnerReadyToExport(event);
    }
  }

  private void setBusinessPartnerReadyToExport(EntityPersistenceEvent event) {
    Property isReadyToExportProperty = MappingUtils
        .getReadyToExportProperty(BusinessPartner.ENTITY_NAME);
    event.setCurrentState(isReadyToExportProperty, true);
  }

  private boolean businessPartnerWasReadyToExport(EntityUpdateEvent event) {
    Property isReadyToExportProperty = MappingUtils
        .getReadyToExportProperty(BusinessPartner.ENTITY_NAME);
    return (Boolean) event.getPreviousState(isReadyToExportProperty);
  }

  private boolean businessPartnerCreditUsedHasBeenModified(EntityUpdateEvent event) {
    final Entity bpEntity = ModelProvider.getInstance().getEntity(BusinessPartner.ENTITY_NAME);
    final Property property = bpEntity.getProperty(BusinessPartner.PROPERTY_CREDITUSED);

    BigDecimal previousValue = (BigDecimal) ObjectUtils.defaultIfNull(
        event.getPreviousState(property), BigDecimal.ZERO);
    BigDecimal currentValue = (BigDecimal) ObjectUtils.defaultIfNull(
        event.getCurrentState(property), BigDecimal.ZERO);

    return previousValue.compareTo(currentValue) != 0;
  }

  private boolean businessPartnerShouldBeMarkedAsReadyToExport(EntityUpdateEvent event) {
    return !businessPartnerWasReadyToExport(event)
        && !businessPartnerCreditUsedHasBeenModified(event);
  }

}
