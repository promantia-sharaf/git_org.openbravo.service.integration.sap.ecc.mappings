/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.Map;

import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPA03 in WP_PLU template
 */
public class ProductTranslationTemplate implements PropertiesHolder {
  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public ProductTranslationTemplate(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getCode() {
    return (String) properties.get(MappingConstants.SAP_WP_PLU_NAME_LENGTH);
  }

  public String getLanguage() {
    return (String) properties.get(MappingConstants.SAP_WP_PLU_NAME_LANGUAGE);
  }

  public String getText() {
    return (String) properties.get(MappingConstants.SAP_WP_PLU_NAME);
  }

  public String getIndex() {
    return (String) properties.get(MappingConstants.SAP_WP_PLU_NAME_SPLIT_INDEX);
  }
}
