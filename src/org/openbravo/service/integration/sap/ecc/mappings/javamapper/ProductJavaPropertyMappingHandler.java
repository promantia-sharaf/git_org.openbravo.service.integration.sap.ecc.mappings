/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBInterceptor;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListSchema;
import org.openbravo.model.pricing.pricelist.PriceListSchemeLine;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaPropertyMappingHandler for the Product entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Product.ENTITY_NAME)
public class ProductJavaPropertyMappingHandler extends JavaPropertyMappingHandler<Product> {

  private static final int DESCRIPTION_PART_LENGTH = 80;
  private boolean isActive;
  private PriceListVersion priceListVersion;

  private static final Logger log = LoggerFactory
      .getLogger(ProductJavaPropertyMappingHandler.class);

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_ISACTIVE);
    properties.put(31, MappingConstants.SAP_WP_PLU_UOM);
    properties.put(41, MappingConstants.SAP_WP_PLU_PRODUCT_CATEGORY_SEGMENT);
    properties.put(51, MappingConstants.SAP_WP_PLU_NAME_SEGMENT);
    properties.put(61, MappingConstants.SAP_WP_PLU_TAX_SEGMENT);
    properties.put(71, MappingConstants.SAP_WP_PLU_PRICE_SEGMENT);
    properties.put(81, MappingConstants.SAP_WP_PLU_CREATION_DATE);
    properties.put(91, MappingConstants.SAP_WP_PLU_EAN);
    properties.put(101, MappingConstants.SAP_WP_PLU_SEARCH_KEY);
    properties.put(111, MappingConstants.SAP_WP_PLU_PRODUCT_CATEGORY);
    properties.put(121, MappingConstants.SAP_WP_PLU_PRICE_LIST_NAME);
    properties.put(131, MappingConstants.SAP_WP_PLU_VALID_FROM);
    properties.put(141, MappingConstants.SAP_WP_PLU_PRICE);
    properties.put(151, MappingConstants.SAP_WP_PLU_CURRENCY);
    properties.put(161, MappingConstants.SAP_WP_PLU_TAX);
    return properties;
  }

  @Override
  public boolean acceptNullValue(String mappingName) {
    return false;
  }

  @Override
  public void setPropertyInBaseOBObject(Product product, String mappingName,
      SynchronizableBusinessObject sbo) {
    Map<String, Object> properties = sbo.getProperties();
    if (!properties.containsKey(mappingName)) {
      log.warn("Property: {} is not included in SynchronizableBusinessObject {}", mappingName,
          properties.keySet());
      return;
    }
    try {
      Object itemValue = properties.get(mappingName);
      switch (mappingName) {
      case MappingConstants.SAP_ORGANIZATION:
        setOrganization(product, itemValue);
        break;
      case MappingConstants.SAP_ISACTIVE:
        setIsActive(product, (String) itemValue);
        break;
      case MappingConstants.SAP_WP_PLU_UOM:
        setUnitOfMeasure(product, itemValue);
        break;
      case MappingConstants.SAP_WP_PLU_PRODUCT_CATEGORY_SEGMENT:
        setProductCategory(product, itemValue);
        break;
      case MappingConstants.SAP_WP_PLU_NAME_SEGMENT:
        setName(product, itemValue);
        break;
      case MappingConstants.SAP_WP_PLU_PRICE_SEGMENT:
        setPrice(product, itemValue);
        break;
      case MappingConstants.SAP_WP_PLU_TAX_SEGMENT:
        setTaxCategory(product, itemValue);
        break;
      default:
        log.warn("Java property not defined in the mapping: {}", mappingName);
      }
    } catch (Exception e) {
      log.error("Error while importing the property " + mappingName, e);
      throw new OBException(e);
    }
  }

  private void setIsActive(Product product, String modi) {
    this.isActive = StringUtils.equals(MappingConstants.SAP_ACTIVE, modi);
    product.setActive(this.isActive);
  }

  private void setPrice(Product product, Object itemValue) {
    ProductPrice productPrice = getProductPrice(product, itemValue);
    if (productPrice == null) {
      OBDal.getInstance().save(product);
      productPrice = OBProvider.getInstance().get(ProductPrice.class);
      productPrice.setNewOBObject(true);
      productPrice.setProduct(product);
      productPrice.setPriceListVersion(this.priceListVersion);
      productPrice.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
      OBDal.getInstance().save(productPrice);
      this.priceListVersion.getPricingProductPriceList().add(productPrice);
    }
    productPrice.setStandardPrice(getPriceFromSegment(itemValue));
    productPrice.setListPrice(getPriceFromSegment(itemValue));
  }

  @SuppressWarnings("unchecked")
  private BigDecimal getPriceFromSegment(Object itemValue) {
    List<SynchronizableBusinessObject> priceLists = (List<SynchronizableBusinessObject>) itemValue;
    List<SynchronizableBusinessObject> prices = (List<SynchronizableBusinessObject>) priceLists
        .get(0).getProperties().get(MappingConstants.SAP_WP_PLU_PRICE_SUBSEGMENT);
    String price = (String) prices.get(0).getProperties().get(MappingConstants.SAP_WP_PLU_PRICE);
    return new BigDecimal(price);
  }

  private ProductPrice getProductPrice(Product product, Object itemValue) {
    this.priceListVersion = getDefaultPriceListVersion(itemValue);
    return getProductPriceByProductAndPriceListVersion(product, priceListVersion);
  }

  private ProductPrice getProductPriceByProductAndPriceListVersion(Product product,
      PriceListVersion defaultPriceListVersion) {
    OBCriteria<ProductPrice> criteria = OBDal.getInstance().createCriteria(ProductPrice.class);
    criteria.add(Restrictions.eq(ProductPrice.PROPERTY_PRODUCT + "." + Product.PROPERTY_ID,
        product.getId()));
    criteria.add(Restrictions.eq(ProductPrice.PROPERTY_PRICELISTVERSION + "."
        + PriceListVersion.PROPERTY_ID, defaultPriceListVersion.getId()));
    return (ProductPrice) criteria.uniqueResult();
  }

  @SuppressWarnings("unchecked")
  private PriceListVersion getDefaultPriceListVersion(Object itemValue) {
    List<SynchronizableBusinessObject> priceLists = (List<SynchronizableBusinessObject>) itemValue;
    String pricelistName = (String) priceLists.get(0).getProperties()
        .get(MappingConstants.SAP_WP_PLU_PRICE_LIST_NAME);
    String validFrom = (String) priceLists.get(0).getProperties()
        .get(MappingConstants.SAP_WP_PLU_VALID_FROM);
    Currency currency = getCurrencyFromSegment(itemValue);

    PriceList priceList = getOrCreatePriceList(pricelistName, currency);
    return getOrCreatePriceListVersion(priceList, pricelistName, validFrom);
  }

  @SuppressWarnings("unchecked")
  private Currency getCurrencyFromSegment(Object itemValue) {
    List<SynchronizableBusinessObject> priceLists = (List<SynchronizableBusinessObject>) itemValue;
    List<SynchronizableBusinessObject> prices = (List<SynchronizableBusinessObject>) priceLists
        .get(0).getProperties().get(MappingConstants.SAP_WP_PLU_PRICE_SUBSEGMENT);
    String currency = (String) prices.get(0).getProperties()
        .get(MappingConstants.SAP_WP_PLU_CURRENCY);
    return MappingUtils.getCurrencyByISOCode(currency);
  }

  private PriceListVersion getOrCreatePriceListVersion(PriceList priceList, String pricelistName,
      String validFrom) {
    if (!priceList.getPricingPriceListVersionList().isEmpty()) {
      return priceList.getPricingPriceListVersionList().get(0);
    }
    PriceListVersion newPriceListVersion = OBProvider.getInstance().get(PriceListVersion.class);
    newPriceListVersion.setNewOBObject(true);
    newPriceListVersion.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    newPriceListVersion.setActive(this.isActive);
    newPriceListVersion.setPriceList(priceList);
    newPriceListVersion.setName(pricelistName);
    newPriceListVersion.setGeneratePriceListVersion(false);
    newPriceListVersion.setPriceListSchema(getOrCreatePriceListSchema(pricelistName, validFrom));
    newPriceListVersion.setValidFromDate(getValidFromDate(validFrom));
    OBDal.getInstance().save(newPriceListVersion);

    priceList.getPricingPriceListVersionList().add(newPriceListVersion);
    return newPriceListVersion;
  }

  private Date getValidFromDate(String validFrom) {
    return MappingUtils.convertDateFromSAPFormat(validFrom);
  }

  private PriceListSchema getOrCreatePriceListSchema(String pricelistName, String validFrom) {
    PriceListSchema priceListSchema = getPriceListSchemaByName(pricelistName);
    if (priceListSchema == null) {
      priceListSchema = createPriceListSchema(pricelistName, validFrom);
    }
    return priceListSchema;
  }

  private PriceListSchema createPriceListSchema(String pricelistName, String validFrom) {
    PriceListSchema priceListSchema = OBProvider.getInstance().get(PriceListSchema.class);
    priceListSchema.setNewOBObject(true);
    priceListSchema.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    priceListSchema.setActive(this.isActive);
    priceListSchema.setName(pricelistName);
    Date validFromDate = getValidFromDate(validFrom);
    priceListSchema.setValidFromDate(validFromDate);
    priceListSchema.setQuantityBased(true);
    priceListSchema.setCumulativeLevel("L");
    priceListSchema.setProcessNow(false);
    priceListSchema.setDiscountType("P");
    OBDal.getInstance().save(priceListSchema);

    priceListSchema.getPricingPriceListSchemeLineList().add(
        createPriceListSchemaLine(priceListSchema, validFromDate));
    return priceListSchema;
  }

  private PriceListSchemeLine createPriceListSchemaLine(PriceListSchema priceListSchema,
      Date validFromDate) {
    PriceListSchemeLine priceListSchemeline = OBProvider.getInstance().get(
        PriceListSchemeLine.class);
    priceListSchemeline.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    priceListSchemeline.setSequenceNumber(10L);
    priceListSchemeline.setActive(this.isActive);
    priceListSchemeline.setBaseListPrice("L");
    priceListSchemeline.setConversionRateType("S");
    priceListSchemeline.setListPriceRounding("C");
    priceListSchemeline.setStandardBasePrice("S");
    priceListSchemeline.setStandardPriceRounding("C");
    priceListSchemeline.setStandardPriceDiscount(BigDecimal.ZERO);
    priceListSchemeline.setBaseLimitPrice("X");
    priceListSchemeline.setPriceLimitRounding("C");
    priceListSchemeline.setPriceLimitDiscount(BigDecimal.ZERO);
    priceListSchemeline.setPriceListSchema(priceListSchema);
    priceListSchemeline.setConversionDate(validFromDate);
    priceListSchemeline.setListPriceDiscount(BigDecimal.ZERO);
    priceListSchemeline.setListPriceDiscount(BigDecimal.ZERO);
    OBDal.getInstance().save(priceListSchemeline);
    return priceListSchemeline;
  }

  private PriceListSchema getPriceListSchemaByName(String pricelistName) {
    OBCriteria<PriceListSchema> criteria = OBDal.getInstance()
        .createCriteria(PriceListSchema.class);
    criteria.add(Restrictions.eq(PriceListSchema.PROPERTY_NAME, pricelistName));
    criteria.setFilterOnReadableOrganization(false);
    return (PriceListSchema) criteria.uniqueResult();
  }

  private PriceList getOrCreatePriceList(String pricelistName, Currency currency) {
    PriceList priceList = MappingUtils.geDefaultSalesPriceList();
    if (priceList == null) {
      priceList = createPriceList(pricelistName, currency);
    }
    return priceList;
  }

  private PriceList createPriceList(String pricelistName, Currency currency) {
    PriceList priceList = OBProvider.getInstance().get(PriceList.class);
    priceList.setNewOBObject(true);
    priceList.setSalesPriceList(true);
    priceList.setName(pricelistName);
    priceList.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    priceList.setActive(this.isActive);
    priceList.setCurrency(currency);
    priceList.setSapobmpSapCode(MappingConstants.DEFAULT_PRICE_LIST_SAP_CODE_VALUE);
    priceList.setPriceIncludesTax(true);
    OBDal.getInstance().save(priceList);
    return priceList;
  }

  @SuppressWarnings("unchecked")
  private void setName(Product product, Object itemValue) {
    List<SynchronizableBusinessObject> names = (List<SynchronizableBusinessObject>) itemValue;
    String shortName = null;
    String longName = null;
    for (SynchronizableBusinessObject sbo : names) {
      Map<String, Object> properties = sbo.getProperties();
      if (MappingConstants.SAP_WP_PLU_LONG_NAME.equals(properties
          .get(MappingConstants.SAP_WP_PLU_NAME_LENGTH))) {
        longName = (String) properties.get(MappingConstants.SAP_WP_PLU_NAME);
      }
      if (MappingConstants.SAP_WP_PLU_SHORT_NAME.equals(properties
          .get(MappingConstants.SAP_WP_PLU_NAME_LENGTH))) {
        shortName = (String) properties.get(MappingConstants.SAP_WP_PLU_NAME);
      }
    }
    product.setName(longName);
    product.setDescription(longName);
    if (shortName != null) {
      product.setDescription(shortName);
    }
  }

  private void setOrganization(Product product, Object itemValue) {
    product.setOrganization(MappingUtils.getOrganization((String) itemValue));
  }

  private void setUnitOfMeasure(Product product, Object itemValue) {
    OBCriteria<UOM> criteria = OBDal.getInstance().createCriteria(UOM.class);
    criteria.add(Restrictions.eq(UOM.PROPERTY_EDICODE, itemValue));
    UOM uom = (UOM) criteria.uniqueResult();
    if (uom == null) {
      throw new OBException("There is no UOM with EDI Code " + itemValue);
    }
    product.setUOM(uom);
  }

  @SuppressWarnings("unchecked")
  private void setProductCategory(Product product, Object itemValue) {
    List<SynchronizableBusinessObject> productCategories = (List<SynchronizableBusinessObject>) itemValue;
    String productCategoryCode = (String) productCategories.get(0).getProperties().get("WARENGR");
    OBCriteria<ProductCategory> criteria = OBDal.getInstance()
        .createCriteria(ProductCategory.class);
    criteria.setFilterOnReadableOrganization(false);
    criteria.add(Restrictions.eq(ProductCategory.PROPERTY_SAPOBMPSAPCODE, productCategoryCode));
    ProductCategory productCategory = (ProductCategory) criteria.uniqueResult();
    if (productCategory == null) {
      throw new OBException("The Product Category with SAP Code " + productCategoryCode
          + " does not exists or is inactive.");
    }
    product.setProductCategory(productCategory);
  }

  @SuppressWarnings("unchecked")
  private void setTaxCategory(Product product, Object itemValue) {
    List<SynchronizableBusinessObject> taxCategories = (List<SynchronizableBusinessObject>) itemValue;
    String taxCategoryCode = (String) taxCategories.get(0).getProperties()
        .get(MappingConstants.SAP_WP_PLU_TAX);
    OBCriteria<TaxCategory> criteria = OBDal.getInstance().createCriteria(TaxCategory.class);
    criteria.add(Restrictions.eq(TaxCategory.PROPERTY_SAPOBMPSAPCODE, taxCategoryCode));
    TaxCategory taxCategory = (TaxCategory) criteria.uniqueResult();
    if (taxCategory == null) {
      throw new OBException("There is no Tax Category with SAP Code " + taxCategoryCode);
    }
    product.setTaxCategory(taxCategory);
  }

  @Override
  public Object getValueOfProperty(Product bob, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return MappingUtils.getSAPFiliale(bob);
    case MappingConstants.SAP_ISACTIVE:
      return bob.isActive() ? MappingConstants.SAP_ACTIVE : MappingConstants.SAP_INACTIVE;
    case MappingConstants.SAP_WP_PLU_CREATION_DATE:
      return IdocDateUtils.getDatePart(bob.getCreationDate());
    case MappingConstants.SAP_WP_PLU_EAN:
      return bob.getUPCEAN();
    case MappingConstants.SAP_WP_PLU_SEARCH_KEY:
      return bob.getSearchKey();
    case MappingConstants.SAP_WP_PLU_UOM:
      return bob.getUOM().getEDICode();
    case MappingConstants.SAP_WP_PLU_PRODUCT_CATEGORY:
      return getProductCategory(bob);
    case MappingConstants.SAP_WP_PLU_NAME_SEGMENT:
      return getNameData(bob);
    case MappingConstants.SAP_WP_PLU_PRICE_LIST_NAME:
      return getPriceListSapCode(bob);
    case MappingConstants.SAP_WP_PLU_VALID_FROM:
      return getPriceListVersionValidFrom(bob);
    case MappingConstants.SAP_WP_PLU_PRICE:
      return getStandardPrice(bob);
    case MappingConstants.SAP_WP_PLU_CURRENCY:
      return getPriceListCurrency(bob);
    case MappingConstants.SAP_WP_PLU_TAX:
      setReadyToExport(bob);
      return MappingUtils.getSAPCode(bob.getTaxCategory());
    default:
      return null;
    }
  }

  private void setReadyToExport(Product bob) {
    OBInterceptor.setPreventUpdateInfoChange(true);
    bob.setSapobmpIsreadytoexport(false);
    OBDal.getInstance().flush();
    OBInterceptor.setPreventUpdateInfoChange(false);
  }

  private Object getPriceListCurrency(Product bob) {
    if (!bob.getPricingProductPriceList().isEmpty()) {
      return bob.getPricingProductPriceList().get(0).getPriceListVersion().getPriceList()
          .getCurrency().getISOCode();
    }
    return null;
  }

  private BigDecimal getStandardPrice(Product bob) {
    if (!bob.getPricingProductPriceList().isEmpty()) {
      return bob.getPricingProductPriceList().get(0).getStandardPrice();
    }
    return null;
  }

  private String getPriceListVersionValidFrom(Product bob) {
    if (!bob.getPricingProductPriceList().isEmpty()) {
      return IdocDateUtils.getDatePart(bob.getPricingProductPriceList().get(0)
          .getPriceListVersion().getValidFromDate());
    }
    return null;
  }

  private String getPriceListSapCode(Product bob) {
    if (!bob.getPricingProductPriceList().isEmpty()) {
      return MappingUtils.getSAPCode(bob.getPricingProductPriceList().get(0).getPriceListVersion()
          .getPriceList());
    }
    return null;
  }

  private List<SynchronizableBusinessObject> getNameData(Product bob) {
    List<SynchronizableBusinessObject> segmentsE1WPA03 = new ArrayList<>();
    segmentsE1WPA03.add(getNameSegment(bob));
    segmentsE1WPA03.addAll(getNameSegmentTranslations(bob));
    segmentsE1WPA03.addAll(getDEscriptionSegments(bob));
    return segmentsE1WPA03;
  }

  private List<SynchronizableBusinessObject> getDEscriptionSegments(Product bob) {
    List<SynchronizableBusinessObject> sbos = new ArrayList<>();
    if (bob.getDescription() != null) {
      int start = 1;
      for (String descriptionPart : getDescriptionParts(bob)) {
        sbos.add(getDescriptionSegment(descriptionPart, start));
        start += DESCRIPTION_PART_LENGTH;
      }
    }
    return sbos;
  }

  private SynchronizableBusinessObject getDescriptionSegment(String descriptionPart, int index) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LENGTH, MappingConstants.SAP_WP_PLU_SHORT_NAME);
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME, descriptionPart);
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_SPLIT_INDEX,
        StringUtils.leftPad(String.valueOf(index), 4, '0'));
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LANGUAGE, "S");
    return sbo;
  }

  private String[] getDescriptionParts(Product bob) {
    String description = bob.getDescription();
    String[] chunks = new String[(description.length() / DESCRIPTION_PART_LENGTH) + 1];
    int start = 0;
    int chunkSize = Math.min(description.length(), DESCRIPTION_PART_LENGTH);
    int index = 0;
    do {
      chunks[index] = description.substring(start, start + chunkSize);
      start += chunkSize;
      if (start + chunkSize > description.length()) {
        chunkSize = bob.getDescription().length() - start;
      }
      index++;
    } while (chunkSize > 0);
    return chunks;
  }

  private List<SynchronizableBusinessObject> getNameSegmentTranslations(Product bob) {
    List<SynchronizableBusinessObject> sbos = new ArrayList<>();
    ScrollableResults translations = getActiveTranslations(bob);
    while (translations.next()) {
      sbos.add(getNameTranslations(translations));
    }
    return sbos;
  }

  private SynchronizableBusinessObject getNameTranslations(ScrollableResults translations) {
    String language = (String) translations.get()[0];
    String name = (String) translations.get()[1];
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LENGTH, MappingConstants.SAP_WP_PLU_LONG_NAME);
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME, name);
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_SPLIT_INDEX, "0001");
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LANGUAGE, language);
    return sbo;
  }

  private ScrollableResults getActiveTranslations(Product bob) {
    StringBuilder hqlQuery = new StringBuilder(
        "select ptrl.language.language, ptrl.name from Product p join p.productTrlList ptrl ");
    hqlQuery
        .append("where exists (select 1 from ADLanguage l where l.systemLanguage = true and l.language = ptrl.language.language) ");
    hqlQuery.append("and p.id = :productId ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("productId", bob.getId());
    query.setFetchSize(1000);

    return query.scroll(ScrollMode.FORWARD_ONLY);
  }

  private SynchronizableBusinessObject getNameSegment(Product bob) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LENGTH, MappingConstants.SAP_WP_PLU_LONG_NAME);
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME, bob.getName());
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_SPLIT_INDEX, "0001");
    sbo.addProperty(MappingConstants.SAP_WP_PLU_NAME_LANGUAGE, "S");
    return sbo;
  }

  private String getProductCategory(Product bob) {
    if (bob.getProductCategory().getSapobmpSapCode() == null) {
      throw new OBException("Product Category " + bob.getProductCategory()
          + " does not has SAP Code defined");
    }
    return bob.getProductCategory().getSapobmpSapCode();
  }
}
