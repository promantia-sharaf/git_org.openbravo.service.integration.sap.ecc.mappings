/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.hibernate.Query;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.service.external.integration.export.RecordScheduledToBeExportedListener;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * Class that listener to the event that is trigger when an invoice is scheduled to be exported
 * 
 * It resets that Is Exported flag, so that the invoice is not re exported the next time the export
 * process is executed
 */
@ApplicationScoped
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Invoice.ENTITY_NAME)
public class ExportedInvoiceListener implements RecordScheduledToBeExportedListener {

  private static final int MAX_PARAMS_PER_INEXPR = 900;

  @Override
  public void recordsWillBeExported(List<String> recordIds) {

    final int numberOfInvoices = recordIds.size();
    int numberOfInvoiceGroups = numberOfInvoices / MAX_PARAMS_PER_INEXPR;
    if (numberOfInvoiceGroups > 0) {
      updateGroupOfInvoicesAsExported(recordIds, numberOfInvoices, numberOfInvoiceGroups);
    } else {
      updateInvoicesAsExported(recordIds);
    }

  }

  private void updateGroupOfInvoicesAsExported(List<String> recordIds, final int numberOfInvoices,
      final int numberOfInvoiceGroups) {
    int invoiceGroups = numberOfInvoices;
    if ((numberOfInvoices % MAX_PARAMS_PER_INEXPR) != 0) {
      invoiceGroups = numberOfInvoiceGroups + 1;
    }
    for (int i = 0; i < invoiceGroups; i++) {
      final int firstInvoiceIndex = i * MAX_PARAMS_PER_INEXPR;
      final int lastInvoiceIndex = firstInvoiceIndex + MAX_PARAMS_PER_INEXPR;
      List<String> groupOfInvoices = recordIds.subList(firstInvoiceIndex,
          Math.min(lastInvoiceIndex, numberOfInvoices));
      updateInvoicesAsExported(groupOfInvoices);
    }
  }

  private void updateInvoicesAsExported(List<String> recordIds) {
    String hql = "UPDATE Invoice set sapobmpIsExported = true WHERE id in (:idList)";
    Query updateQry = OBDal.getInstance().getSession().createQuery(hql);
    updateQry.setParameterList("idList", recordIds);
    updateQry.executeUpdate();
  }

}
