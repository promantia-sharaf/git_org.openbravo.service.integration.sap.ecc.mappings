/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the InventoryCount entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = InventoryCount.ENTITY_NAME)
public class InventoryCountJavaPropertyMappingHandler extends
    JavaPropertyMappingHandler<InventoryCount> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_WVINVE_WAREHOUSE);
    properties.put(31, MappingConstants.SAP_WVINVE_FISCAL_YEAR);
    properties.put(41, MappingConstants.SAP_WVINVE_PLANNED_MOVEMENT_DATE);
    properties.put(51, MappingConstants.SAP_WVINVE_NAME);
    properties.put(61, MappingConstants.SAP_WVINVE_CREATED);
    properties.put(71, MappingConstants.SAP_WVINVE_MOVEMENT_DATE);
    properties.put(81, MappingConstants.SAP_WVINVE_TIME_OF_MOVEMENT_DATE);
    properties.put(91, MappingConstants.SAP_WVINVE_LINE);
    properties.put(101, MappingConstants.SAP_WVINVE_FILIALE);

    return properties;
  }

  @Override
  public Object getValueOfProperty(InventoryCount inventoryCount, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return getFiliale(inventoryCount);
    case MappingConstants.SAP_WVINVE_WAREHOUSE:
      return MappingUtils.getSAPCode(inventoryCount.getWarehouse());
    case MappingConstants.SAP_WVINVE_FISCAL_YEAR:
      return IdocDateUtils.getDatePart(inventoryCount.getMovementDate()).substring(0, 4);
    case MappingConstants.SAP_WVINVE_PLANNED_MOVEMENT_DATE:
      return IdocDateUtils.getDatePart(inventoryCount.getMovementDate());
    case MappingConstants.SAP_WVINVE_NAME:
      return inventoryCount.getName();
    case MappingConstants.SAP_WVINVE_CREATED:
      return inventoryCount.getCreatedBy().getName();
    case MappingConstants.SAP_WVINVE_MOVEMENT_DATE:
      return IdocDateUtils.getDatePart(inventoryCount.getMovementDate());
    case MappingConstants.SAP_WVINVE_TIME_OF_MOVEMENT_DATE:
      return IdocDateUtils.getTimePart(inventoryCount.getMovementDate());
    case MappingConstants.SAP_WVINVE_LINE:
      return getLines(inventoryCount);
    case MappingConstants.SAP_WVINVE_FILIALE:
      return getFiliale(inventoryCount);
    default:
      return null;
    }
  }

  private Object getLines(InventoryCount inventoryCount) {
    List<SynchronizableBusinessObject> sboList = new ArrayList<>();
    for (InventoryCountLine inventoryCountLine : inventoryCount
        .getMaterialMgmtInventoryCountLineList()) {
      SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
      sbo.addProperty(MappingConstants.SAP_WVINVE_LINE_NO, inventoryCountLine.getLineNo());
      sbo.addProperty(MappingConstants.SAP_WVINVE_PRODUCT, inventoryCountLine.getProduct()
          .getSearchKey());
      sbo.addProperty(MappingConstants.SAP_WVINVE_QUANTITY, inventoryCountLine.getQuantityCount());
      sbo.addProperty(MappingConstants.SAP_WVINVE_UOM, inventoryCountLine.getUOM().getEDICode());
      sboList.add(sbo);
    }
    return sboList;
  }

  private Object getFiliale(InventoryCount inventoryCount) {
    return MappingUtils.getSAPFiliale(inventoryCount);
  }

}
