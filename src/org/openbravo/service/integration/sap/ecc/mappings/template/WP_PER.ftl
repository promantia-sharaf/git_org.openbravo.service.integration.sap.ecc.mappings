<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


-->
<?xml version="1.0" encoding="utf-8"?>
<WP_PER01>
  <IDOC BEGIN="1">
    <EDI_DC40 SEGMENT="1">
      <TABNAM>EDI_DC40</TABNAM>
      <MANDT>100</MANDT>
      <IDOCTYP>WP_PER01</IDOCTYP>
      <MESTYP>WP_PER</MESTYP>
      <SNDPOR>SAPEED</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <SNDPRN>${data.filiale}</SNDPRN>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>KU</RCVPRT>
      <RCVPRN>${data.filiale}</RCVPRN>
    </EDI_DC40>
    <E1WPP01 SEGMENT="1">
      <FILIALE>${data.filiale}</FILIALE>
      <AENDKENNZ>MODI</AENDKENNZ>
      <QUALIFIER>KUND</QUALIFIER>
      <PERSNR>${data.searchKey}</PERSNR>
      <E1WPP02 SEGMENT="1">
        <NAME>${data.name}</NAME>
      </E1WPP02>
<#list data.locations as location>
      <E1WPP03 SEGMENT="1">
      <#if location.phone != "" >
        <TELEFON>${location.phone}</TELEFON>
      </#if>
      <#if location.fax != "" >
        <FAX>${location.fax}</FAX>
      </#if>
      <#if location.postalCode != "">
        <PLZ>${location.postalCode}</PLZ>
      </#if>
      <#if location.city != "">
        <ORT>${location.city}</ORT>
      </#if>
      <#if location.address != "">
        <STRASSE>${location.address}</STRASSE>
      </#if>
      </E1WPP03>
</#list>
    </E1WPP01>  
  </IDOC>
</WP_PER01>