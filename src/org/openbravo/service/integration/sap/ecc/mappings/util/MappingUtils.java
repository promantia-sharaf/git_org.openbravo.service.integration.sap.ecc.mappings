/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.pricing.pricelist.PriceList;

/**
 * Helper class containing commonly used methods
 *
 */
public class MappingUtils {

  /**
   * Get Organization associated with the FILIALE code, considering the value of the preference
   * "SAPOBMP_IgnoreMappingForOrganization", which controls when the mapping for Organization should
   * be ignored
   * 
   * @param filiale
   *          The FILIALE code found in iDoc
   * @return The Organization associated with the FILIALE code
   */
  public static Organization getOrganization(String filiale) {
    if (organizationMappingShouldBeIgnored()) {
      return OBDal.getInstance().getProxy(Organization.class, "0");
    }
    Organization organization = getOrganizationByFiliale(filiale);
    if (organization != null) {
      return organization;
    }
    throw new OBException("There is no Organization mapped with SAP Filiale " + filiale);
  }

  private static Organization getOrganizationByFiliale(String filiale) {
    OBCriteria<Organization> criteria = OBDal.getInstance().createCriteria(Organization.class);
    criteria.add(Restrictions.eq(Organization.PROPERTY_SAPOBMPFILIALE, filiale));
    return (Organization) criteria.uniqueResult();
  }

  private static boolean organizationMappingShouldBeIgnored() {

    try {
      if (Preferences.YES.equals(Preferences.getPreferenceValue(
          MappingConstants.IGNORE_ORGANIZATION_MAPPING_PREFERENCE, true, OBContext.getOBContext()
              .getCurrentClient(), null, null, null, null))) {
        return true;
      }
    } catch (PropertyException e) {
      // Property is not defined or conflict exists
      return false;
    }
    return false;
  }

  /**
   * Get the Currency with the given ISO Code
   * 
   * @param isoCode
   *          The ISO Code for the Currency
   * @return The Currency
   */
  public static Currency getCurrencyByISOCode(String isoCode) {
    OBCriteria<Currency> criteria = OBDal.getInstance().createCriteria(Currency.class);
    criteria.add(Restrictions.eq(Currency.PROPERTY_ISOCODE, isoCode));
    return (Currency) criteria.uniqueResult();
  }

  /**
   * Get a Date from a String representing a date with format "YYY-MM-DD"
   * 
   * @param dateString
   *          The String representing the date
   * @return The Date
   */
  public static Date convertDateFromSAPFormat(String dateString) {
    String year = dateString.substring(0, 4);
    String month = dateString.substring(4, 6);
    String day = dateString.substring(6, 8);
    Calendar date = Calendar.getInstance();
    date.set(Calendar.YEAR, Integer.parseInt(year));
    date.set(Calendar.MONTH, Integer.parseInt(month) - 1);
    date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
    return date.getTime();
  }

  /**
   * Get the FILIALE code from an Organization the entity belongs to.
   * 
   * @param bob
   *          The entity
   * @return The Organization
   * @throws OBException
   *           if the Organization has not defined FILIALE code
   */
  public static String getSAPFiliale(OrganizationEnabled bob) {
    String filiale = bob.getOrganization().getSapobmpFiliale();
    if (StringUtils.isBlank(filiale)) {
      throw new OBException("Organization " + bob.getOrganization()
          + " is not mapped with SAP FILIALE");
    }
    return filiale;
  }

  /**
   * Get SAP Code from an entity
   * 
   * @param entity
   *          The entity
   * @return The SAP Code
   * @throws OBException
   *           if the entity is null, the SAP Code is null, or the entity does not have the property
   *           "sapobmpSapcode"
   */
  public static String getSAPCode(BaseOBObject entity) {
    if (entity == null) {
      String stackTrace = ExceptionUtils.getStackTrace(new Exception());
      throw new OBException("Entity is null: " + stackTrace);
    }
    if (entity.getEntity().hasProperty(MappingConstants.SAP_CODE_FIELD)) {
      if (StringUtils.isNotBlank((String) entity.get(MappingConstants.SAP_CODE_FIELD))) {
        return (String) entity.get(MappingConstants.SAP_CODE_FIELD);
      }
      throw new OBException(entity + " does not have SAP Code assigned");
    }
    throw new OBException(entity + " does not have property SAP Code defined");
  }

  /**
   * Get the Sales Order from an Invoice. If Invoice doesn't have a Sales Order Assigned, the Order
   * is taken from the first Invoice Line
   * 
   * @param invoice
   *          The Invoice
   * @return The Order
   */
  public static Order getSalesOrderFromIvoice(Invoice invoice) {
    Order salesOrder = invoice.getSalesOrder();
    if (salesOrder == null && isSafeGettingOrderFromInvoice(invoice)) {
      salesOrder = invoice.getInvoiceLineList().get(0).getSalesOrderLine().getSalesOrder();
    }
    if (salesOrder == null) {
      throw new OBException("Unable to get Order from Invoice");
    }
    return salesOrder;
  }

  private static boolean isSafeGettingOrderFromInvoice(Invoice invoice) {
    return !invoice.getInvoiceLineList().isEmpty()
        && invoice.getInvoiceLineList().get(0).getSalesOrderLine() != null;
  }

  /**
   * Gets the contact shown in Web POS (the one with max uuid) for a Business Partner
   * 
   * @param businessPartner
   *          The Business Partner
   * @return The User entity
   */
  public static User getContactShownInWebPos(BusinessPartner businessPartner) {
    final boolean userHasNoContacts = businessPartner.getADUserList().isEmpty();
    if (userHasNoContacts) {
      return null;
    }
    OBCriteria<User> userCriteria = OBDal.getInstance().createCriteria(User.class);
    userCriteria.add(Restrictions.eq(User.PROPERTY_BUSINESSPARTNER, businessPartner));
    userCriteria.addOrderBy(User.PROPERTY_ID, false);
    userCriteria.setMaxResults(1);
    return (User) userCriteria.uniqueResult();
  }

  /**
   * Get the Goss Amount for an Invoice Line, considering if the Price List include taxes
   * 
   * @param invoiceLine
   *          The Invoice Line
   * @return The amount
   */
  public static BigDecimal getGrossAmountConsideringTaxesInPriceList(InvoiceLine invoiceLine) {
    if (!invoiceLine.getInvoice().getPriceList().isPriceIncludesTax()) {
      return invoiceLine.getStandardPrice().multiply(invoiceLine.getInvoicedQuantity()).abs();
    }
    return invoiceLine.getGrossAmount().abs();
  }

  /**
   * Get the Business Partner Location set as Invoice Address.
   * 
   * @param businessPartner
   *          The Business Partner
   * @return The BusinessPartnerLocation
   * @throws UnsupportedOperationException
   *           if Business Partner has more than one Invoice Address
   */
  public static Location getBusinessPartnerFiscalLocation(BusinessPartner businessPartner) {
    List<Location> fiscalAddresses = new ArrayList<>();
    for (Location location : businessPartner.getBusinessPartnerLocationList()) {
      if (location.isInvoiceToAddress()) {
        fiscalAddresses.add(location);
      }
    }
    if (fiscalAddresses.isEmpty()) {
      return null;
    } else if (fiscalAddresses.size() == 1) {
      return fiscalAddresses.get(0);
    }
    throw new UnsupportedOperationException("There are more than one fiscal address");
  }

  /**
   * Get the TreeNode for a Product Category entity
   * 
   * @param entity
   *          The Product Category
   * @return The TreeNode
   */
  public static TreeNode getTreeNodeByEntityId(ProductCategory entity) {
    OBCriteria<TreeNode> criteria = OBDal.getInstance().createCriteria(TreeNode.class);
    criteria.add(Restrictions.eq(TreeNode.PROPERTY_NODE, entity.getId()));
    criteria.add(Restrictions.eq(TreeNode.PROPERTY_TREE,
        getTreeByClient(entity.getClient().getId())));
    return (TreeNode) criteria.uniqueResult();
  }

  /**
   * Get the Product Category Tree for the Client
   * 
   * @param clientId
   *          The Client
   * @return The Tree
   */
  private static Tree getTreeByClient(String clientId) {
    ClientInformation clientInfo = OBDal.getInstance().get(ClientInformation.class, clientId);
    return clientInfo.getPrimaryTreeProductCategory();
  }

  /**
   * Get the default Sales PriceList for imported Business Partners and Products
   * 
   * @return Te Price List
   */
  public static PriceList geDefaultSalesPriceList() {
    OBCriteria<PriceList> criteria = OBDal.getInstance().createCriteria(PriceList.class);
    criteria.setFilterOnReadableOrganization(false);
    criteria.add(Restrictions.eq(PriceList.PROPERTY_SAPOBMPSAPCODE,
        MappingConstants.DEFAULT_PRICE_LIST_SAP_CODE_VALUE));
    return (PriceList) criteria.uniqueResult();
  }

  /**
   * Get the Property object associated with the sapobmpIsreadytoexport property
   * 
   * @param entityName
   *          The entity name
   * @return The Property object
   */
  public static Property getReadyToExportProperty(String entityName) {
    final Entity entity = ModelProvider.getInstance().getEntity(entityName);
    if (entity.hasProperty(MappingConstants.ISREADYTOEXPORT_PROPERTY_NAME)) {
      return entity.getProperty(MappingConstants.ISREADYTOEXPORT_PROPERTY_NAME);
    }
    throw new OBException("Entity " + entity + "has not defined property "
        + MappingConstants.ISREADYTOEXPORT_PROPERTY_NAME);
  }

  /**
   * Get the sum of discounts for an Invoice Line
   * 
   * @param invoiceLine
   *          The invoice line
   * @return The total discounts
   */
  public static BigDecimal getTotalDiscountsFromInvoiceLine(InvoiceLine invoiceLine) {
    OBCriteria<InvoiceLineOffer> lineOfferCriteria = OBDal.getInstance().createCriteria(
        InvoiceLineOffer.class);
    lineOfferCriteria.add(Restrictions.eq(InvoiceLineOffer.PROPERTY_INVOICELINE, invoiceLine));

    ProjectionList projections = Projections.projectionList();
    projections.add(Projections.sum(InvoiceLineOffer.PROPERTY_TOTALAMOUNT));

    lineOfferCriteria.setProjection(projections);
    return (BigDecimal) ObjectUtils.defaultIfNull((BigDecimal) lineOfferCriteria.uniqueResult(),
        BigDecimal.ZERO);
  }
}