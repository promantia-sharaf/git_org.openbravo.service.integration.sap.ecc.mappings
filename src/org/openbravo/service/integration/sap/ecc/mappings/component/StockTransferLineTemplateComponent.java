/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Base template to be used to generate segment E1WPG02 in WPUWBW template
 */
public class StockTransferLineTemplateComponent implements PropertiesHolder {

  Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public StockTransferLineTemplateComponent(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getPoReference() {
    return StringUtils.defaultIfBlank(
        (String) this.properties.get(MappingConstants.SAP_WPUWBW_POREFERENCE), StringUtils.EMPTY);
  }

  public String getLineNo() {
    Long lineNo = (Long) this.properties.get(MappingConstants.SAP_WPUWBW_LINENO);
    return lineNo.toString();
  }

  public String getProduct() {
    return (String) this.properties.get(MappingConstants.SAP_WPUWBW_PRODUCT);
  }

  public String getMovementType() {
    return StringUtils.defaultIfBlank(
        (String) this.properties.get(MappingConstants.SAP_WPUWBW_MOVEMENT_TYPE), StringUtils.EMPTY);
  }

  public String getUomEdiCode() {
    return (String) this.properties.get(MappingConstants.SAP_WPUWBW_UOM_EDI_CODE);
  }

  public String getMovementQuantity() {
    BigDecimal movementQuantity = (BigDecimal) this.properties
        .get(MappingConstants.SAP_WPUWBW_MOVEMENT_QTY);
    return movementQuantity.toString();
  }

  public String getBusinessPartner() {
    return (String) this.properties.get(MappingConstants.SAP_WPUWBW_BP_SEARCH_KEY);
  }

  public String getFilialeWarehouseTo() {
    return StringUtils.defaultIfBlank(
        (String) this.properties.get(MappingConstants.SAP_WPUWBW_ORGANIZATION_TO),
        StringUtils.EMPTY);
  }

  public String getWarehouse() {
    return (String) this.properties.get(MappingConstants.SAP_WPUWBW_WAREHOUSE);
  }

}
