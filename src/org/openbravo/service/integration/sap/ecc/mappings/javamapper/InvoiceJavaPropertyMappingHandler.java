/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the Invoice entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Invoice.ENTITY_NAME)
public class InvoiceJavaPropertyMappingHandler extends JavaPropertyMappingHandler<Invoice> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_WPUBON_SAPTERMINAL);
    properties.put(31, MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SEGMENT);
    properties.put(41, MappingConstants.SAP_INVOICE_USER_NAME);
    properties.put(51, MappingConstants.SAP_INVOICE_DOCNO);
    properties.put(100, MappingConstants.SAP_INVOICEDATE_DATE);
    return properties;
  }

  @Override
  public Object getValueOfProperty(Invoice invoice, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return invoice.getOrganization().getSapobmpFiliale();
    case MappingConstants.SAP_WPUBON_SAPTERMINAL:
      return getSapTerminal(invoice);
    case MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SEGMENT:
      return getPaymentMethodLines(invoice);
    case MappingConstants.SAP_INVOICE_USER_NAME:
      return getCashier(invoice);
    case MappingConstants.SAP_INVOICE_DOCNO:
      return getDocumentNumber(invoice);
    case MappingConstants.SAP_INVOICEDATE_DATE:
      return invoice.getInvoiceDate();
    default:
      return null;
    }
  }

  private String getDocumentNumber(Invoice invoice) {
    return MappingUtils.getSalesOrderFromIvoice(invoice).getDocumentNo();
  }

  private String getCashier(Invoice invoice) {
    Order salesOrder = MappingUtils.getSalesOrderFromIvoice(invoice);
    return salesOrder.getCreatedBy().getUsername();
  }

  protected void processPaymentDetail(SynchronizableBusinessObject sbo,
      FIN_PaymentScheduleDetail paymentDetail) {
    // Derived class should override this method if extra processing of PaymentDetails is needed
  }

  protected List<SynchronizableBusinessObject> getPaymentMethodLines(Invoice invoice) {
    List<SynchronizableBusinessObject> sbos = new ArrayList<>();
    OBCriteria<FIN_PaymentScheduleDetail> paidSchedDetails = OBDal.getInstance().createCriteria(
        FIN_PaymentScheduleDetail.class);
    paidSchedDetails.createAlias(FIN_PaymentScheduleDetail.PROPERTY_INVOICEPAYMENTSCHEDULE, "ips");
    paidSchedDetails.add(Restrictions.eq("ips." + FIN_PaymentSchedule.PROPERTY_INVOICE, invoice));
    paidSchedDetails.add(Restrictions.isNotNull(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS));

    for (FIN_PaymentScheduleDetail paymentSchedDetail : paidSchedDetails.list()) {
      SynchronizableBusinessObject sbo = getPaymentMethodLines(paymentSchedDetail);
      processPaymentDetail(sbo, paymentSchedDetail);
      sbos.add(sbo);
    }
    return sbos;
  }

  private SynchronizableBusinessObject getPaymentMethodLines(
      FIN_PaymentScheduleDetail paymentSchedDetail) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    FIN_Payment payment = paymentSchedDetail.getPaymentDetails().getFinPayment();
    sbo.addProperty(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_SAP_CODE_FIELD,
        MappingUtils.getSAPCode(payment.getPaymentMethod()));
    sbo.addProperty(MappingConstants.SAP_INVOICE_TOTAL_PAID, paymentSchedDetail.getAmount().abs());
    sbo.addProperty(MappingConstants.SAP_INVOICE_PAYMENT_METHOD_CURRENCY, payment.getAccount()
        .getCurrency().getISOCode());

    sbo.addProperty(MappingConstants.SAP_INVOICE_TOTAL_PAID_SIGN, paymentSchedDetail.getAmount()
        .signum() < 0 ? "-" : "+");
    return sbo;
  }

  private String getSapTerminal(Invoice invoice) {
    Order salesOrder = MappingUtils.getSalesOrderFromIvoice(invoice);
    if (salesOrder.getObposApplications() == null) {
      throw new OBException("Sales Order " + salesOrder.getIdentifier()
          + " does not have a POS Terminal assigned");
    }
    if (StringUtils.isBlank(salesOrder.getObposApplications().getSapobmpSapTerminal())) {
      throw new OBException("POS Terminal " + salesOrder.getObposApplications()
          + " is not mapped to SAP Terminal");
    }
    return salesOrder.getObposApplications().getSapobmpSapTerminal();
  }

}
