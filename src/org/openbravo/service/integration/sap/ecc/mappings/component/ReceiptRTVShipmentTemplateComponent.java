/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for 101 and 102 WPUWBW template
 */
@ApplicationScoped
@IdocType("WPUWBW101102")
public class ReceiptRTVShipmentTemplateComponent extends StockTransferComponentTemplate {

  private static final String TEMPLATE_ID = "B14DBBD6110A4707B319592670D8648C";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

}
