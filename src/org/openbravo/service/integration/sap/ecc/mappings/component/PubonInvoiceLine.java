/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WPB02 in the PUBON template
 */
public class PubonInvoiceLine implements PropertiesHolder {

  private static final String SAP_QUANTITY_INVOICED = "MENGE";

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public PubonInvoiceLine(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getProduct() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE);
  }

  public String getProductCode() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE);
  }

  public String getQuantity() {
    BigDecimal quantity = (BigDecimal) properties.get(SAP_QUANTITY_INVOICED);
    return quantity.toString();
  }

  public String getIsReturn() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_RETURN_CODE);
  }

  public String getSign() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_RETURN_SIGN);
  }

  public String getGrossAmount() {
    BigDecimal amount = (BigDecimal) properties.get(MappingConstants.SAP_INVOICELINE_GROSS_AMOUNT);
    return amount != null ? amount.toString() : StringUtils.EMPTY;
  }

  public String getTaxAmount() {
    BigDecimal amount = (BigDecimal) properties.get(MappingConstants.SAP_INVOICELINE_TAX_AMOUNT);
    return amount.toString();
  }

  public String getTaxRate() {
    return StringUtils.defaultIfBlank((String) properties.get(MappingConstants.SAP_TAX_RATE),
        StringUtils.EMPTY);
  }

  public String getInvoiceLineTaxRate() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICELINE_TAX_RATE), StringUtils.EMPTY);
  }

  public String getPriceListSapCode() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_PRICELIST_SAP_CODE_FIELD),
        StringUtils.EMPTY);
  }

  public String getSerialNumber() {
    return StringUtils.defaultIfBlank(
        (String) properties.get(MappingConstants.SAP_INVOICE_SERIAL_NUMBER), StringUtils.EMPTY);
  }

  public String getPriceSign() {
    return (String) properties.get(MappingConstants.SAP_INVOICELINE_PRICE_SIGN);
  }

  @SuppressWarnings("unchecked")
  public List<PubonInvoiceDiscount> getDiscounts() {
    List<PubonInvoiceDiscount> discountLines = new ArrayList<>();

    List<SynchronizableBusinessObject> discountsSbo = (List<SynchronizableBusinessObject>) properties
        .get(MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT);
    if (discountsSbo == null) {
      return discountLines;
    }
    for (SynchronizableBusinessObject discountSbo : discountsSbo) {
      PubonInvoiceDiscount discountLine = new PubonInvoiceDiscount(discountSbo.getProperties());
      discountLines.add(discountLine);
    }
    return discountLines;
  }
}
