/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the WPUUMS template
 */
@ApplicationScoped
@IdocType("WPUUMS")
public class CashUpTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "8DAA1A83F76A49CEA8FFC4102F169CC3";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) this.getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getCashUpUpdate() {
    return (String) this.getProperties().get(MappingConstants.SAP_CASHUP_UPDATE);
  }

  @SuppressWarnings("unchecked")
  public List<CashUpInvoiceLine> getInvoiceLines() {
    List<SynchronizableBusinessObject> invoiceSbo = (List<SynchronizableBusinessObject>) this
        .getProperties().get(MappingConstants.SAP_INVOICELINE_SEGMENT);
    List<CashUpInvoiceLine> invoiceLines = new ArrayList<>();
    if (invoiceSbo == null) {
      return invoiceLines;
    }
    for (SynchronizableBusinessObject sbo : invoiceSbo) {
      CashUpInvoiceLine cashUpInvoiceLine = new CashUpInvoiceLine(sbo.getProperties());
      invoiceLines.add(cashUpInvoiceLine);
    }
    return invoiceLines;
  }
}
