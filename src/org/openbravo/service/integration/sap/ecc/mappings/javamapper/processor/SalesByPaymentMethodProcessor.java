/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.ScrollableResults;
import org.openbravo.dal.service.OBDal;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;

/**
 * Base class adding default behaviour for exporting aggregated sales.
 *
 */
public abstract class SalesByPaymentMethodProcessor {

  /**
   * Get ScrollableResults containig the information to be used to create a list of
   * SynchronizableBusinessObject, with which the WPUTAB iDoc will be generated
   * 
   * @return ScrollableResults based on custom query
   */
  protected abstract ScrollableResults getSalesGroupedByPaymentMethod();

  /**
   * Verifies if the CashUp bein processed generated invoices that should be exported
   * 
   * @return boolean
   */
  public abstract boolean hasSalesToProcess();

  /**
   * Adds/modify information to the SynchronizableBusinessObject that will be used to generate
   * WPUTAB iDoc
   * 
   * @param sbo
   *          SynchronizableBusinessObject containing default information
   * @param totalPaidByPaymentMethod
   *          ScrollableResults containing the records to be proccesed
   */
  protected void processPaymentScheduleDetail(SynchronizableBusinessObject sbo,
      ScrollableResults totalPaidByPaymentMethod) {
    // Derived classes should override this method if an extra processing of PaymentScheduleDetail
    // retrieved is needed
  }

  /**
   * Get a list of SynchronizableBusinessObject, with which the WPUTAB iDoc will be generated
   * 
   * @return List of SynchronizableBusinessObject
   */
  public List<SynchronizableBusinessObject> getSalesByPaymentMethod() {
    ScrollableResults totalPaidByPaymentMethod = getSalesGroupedByPaymentMethod();
    List<SynchronizableBusinessObject> sbos = new ArrayList<>();
    int i = 0;
    while (totalPaidByPaymentMethod.next()) {
      sbos.add(getSyncronizableBusinessObject(totalPaidByPaymentMethod));

      if (++i % 100 == 0) {
        OBDal.getInstance().getSession().clear();
      }
    }
    totalPaidByPaymentMethod.close();
    addExtraSegments(sbos);
    return sbos;
  }

  /**
   * Adds new SynchronizableBusinessObject to the list. Derived classes should override this method
   * if during processing additionals SynchronizableBusinessObject have been created.
   * 
   * @param sbos
   *          The SynchronizableBusinessObject list
   */
  protected void addExtraSegments(List<SynchronizableBusinessObject> sbos) {
    // Derived classes should override this method if during processing additionals
    // SynchronizableBusinessObject have been created
  }

  private SynchronizableBusinessObject getSyncronizableBusinessObject(
      ScrollableResults totalPaidByPaymentMethod) {
    String currencyISOCode = getCurrencyISOCode(totalPaidByPaymentMethod);
    BigDecimal totalPaid = getTotalPaid(totalPaidByPaymentMethod);

    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WPUTAB_PAYMENT_METHOD_FIELD,
        getPaymentMethodSapCode(totalPaidByPaymentMethod));
    sbo.addProperty(MappingConstants.SAP_WPUTAB_CURRENCY, currencyISOCode);
    sbo.addProperty(MappingConstants.SAP_WPUTAB_TOTALPAID, totalPaid.abs());
    sbo.addProperty(MappingConstants.SAP_WPUTAB_SIGN, getTotalPaidSign(totalPaidByPaymentMethod));

    processPaymentScheduleDetail(sbo, totalPaidByPaymentMethod);
    return sbo;
  }

  /**
   * Get the sign of the paid amount
   * 
   * @param totalPaidByPaymentMethod
   *          ScrollableResults based on custom query
   * @return The sign of the paid amount
   */
  protected abstract String getTotalPaidSign(ScrollableResults totalPaidByPaymentMethod);

  /**
   * Get the total paid by payment mehods
   * 
   * @param totalPaidByPaymentMethod
   *          ScrollableResults based on custom query
   * @return The total paid
   */
  protected abstract BigDecimal getTotalPaid(ScrollableResults totalPaidByPaymentMethod);

  /**
   * Get the currency by payment method
   * 
   * @param totalPaidByPaymentMethod
   *          ScrollableResults based on custom query
   * @return The currency by payment method
   */
  protected abstract String getCurrencyISOCode(ScrollableResults totalPaidByPaymentMethod);

  /**
   * Get the SAP Code by payment method
   * 
   * @param totalPaidByPaymentMethod
   *          ScrollableResults based on custom query
   * @return The SAP Code by payment method
   */
  protected abstract String getPaymentMethodSapCode(ScrollableResults totalPaidByPaymentMethod);
}
