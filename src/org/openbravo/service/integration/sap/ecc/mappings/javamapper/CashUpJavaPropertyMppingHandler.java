/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.retail.posterminal.OBPOSAppCashup;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the CashUp entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = OBPOSAppCashup.ENTITY_NAME)
public class CashUpJavaPropertyMppingHandler extends JavaPropertyMappingHandler<OBPOSAppCashup> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_CASHUP_UPDATE);
    properties.put(31, MappingConstants.SAP_INVOICELINE_SEGMENT);
    return properties;
  }

  @Override
  public Object getValueOfProperty(OBPOSAppCashup cashUp, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return getFiliale(cashUp);
    case MappingConstants.SAP_CASHUP_UPDATE:
      return getCashUpUpdate(cashUp);
    case MappingConstants.SAP_INVOICELINE_SEGMENT:
      return getInvoiceLines(cashUp);
    default:
      return null;
    }
  }

  private Object getInvoiceLines(OBPOSAppCashup cashUp) {
    ScrollableResults results = getLinesInvoiced(cashUp);
    List<SynchronizableBusinessObject> soList = new ArrayList<>();
    int i = 0;
    while (results.next()) {
      InvoiceLine invoiceLine = (InvoiceLine) results.get()[0];
      SynchronizableBusinessObject sbo = getSyncronizableBuisinessObjectFromInvoiceLine(invoiceLine);
      soList.add(sbo);
      if (++i % 100 == 0) {
        OBDal.getInstance().getSession().clear();
      }
    }
    results.close();
    return soList;
  }

  private SynchronizableBusinessObject getSyncronizableBuisinessObjectFromInvoiceLine(
      InvoiceLine invoiceLine) {
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE, invoiceLine.getProduct()
        .getSearchKey());
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_QTYINVOICED, invoiceLine.getInvoicedQuantity()
        .abs());
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_WPUUMS_LINE_GROSS_AMOUNT,
        getAmountConsideringTaxesInPriceListAndDiscounts(invoiceLine));
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_WPUUMS_RETURNORSALE,
        getSignReturnOrSales(invoiceLine));
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_WPUUMS_TAXVALUE, getTaxAmount(invoiceLine));
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_WPUUMS_TAXCODE, getTaxCode(invoiceLine));
    return sbo;
  }

  protected BigDecimal getAmountConsideringTaxesInPriceListAndDiscounts(InvoiceLine invoiceLine) {
    return MappingUtils.getGrossAmountConsideringTaxesInPriceList(invoiceLine).subtract(
        MappingUtils.getTotalDiscountsFromInvoiceLine(invoiceLine).abs());
  }

  private Object getTaxCode(InvoiceLine invoiceLine) {
    if (!invoiceLine.getInvoiceLineTaxList().isEmpty()) {
      return MappingUtils.getSAPCode(invoiceLine.getInvoiceLineTaxList().get(0).getTax());
    }
    return null;
  }

  private BigDecimal getTaxAmount(InvoiceLine invoiceLine) {
    if (!invoiceLine.getInvoiceLineTaxList().isEmpty()) {
      return invoiceLine.getInvoiceLineTaxList().get(0).getTax().getRate().multiply(BigDecimal.TEN);
    }
    return null;
  }

  private String getSignReturnOrSales(InvoiceLine invoiceLine) {
    if (invoiceLine.getSalesOrderLine() == null) {
      return null;
    }
    return invoiceLine.getSalesOrderLine().getSalesOrder().getDocumentType().isReturn()
        || invoiceLine.getSalesOrderLine().getSalesOrder().getCancelledorder() != null ? "+" : "-";
  }

  private ScrollableResults getLinesInvoiced(OBPOSAppCashup cashUp) {
    StringBuilder hqlQuery = new StringBuilder();
    hqlQuery
        .append("from InvoiceLine il inner join il.salesOrderLine ol inner join ol.salesOrder so ");
    hqlQuery.append("where so.client.id = :clientId ");
    hqlQuery.append("and so.obposAppCashup is not null ");
    hqlQuery.append("and so.obposAppCashup = :cashUpId ");
    hqlQuery.append("and il.invoice.salesOrder is null ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());

    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    query.setParameter("cashUpId", cashUp.getId());
    query.setFetchSize(1000);
    return query.scroll(ScrollMode.FORWARD_ONLY);
  }

  private Object getCashUpUpdate(OBPOSAppCashup cashUp) {
    return IdocDateUtils.getDatePart(cashUp.getCashUpDate());
  }

  private Object getFiliale(OBPOSAppCashup cashUp) {
    return MappingUtils.getSAPFiliale(cashUp);
  }

}
