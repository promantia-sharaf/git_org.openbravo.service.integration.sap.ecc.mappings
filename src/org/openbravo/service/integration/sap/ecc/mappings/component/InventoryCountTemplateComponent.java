/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the WVINVE template
 */
@ApplicationScoped
@IdocType("WVINVE")
public class InventoryCountTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "3451C3D2518F45D38A5E9C7DBF8DF3D3";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getWarehouse() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_WAREHOUSE);
  }

  public String getFiscalYear() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_FISCAL_YEAR);
  }

  public String getMovementDate() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_MOVEMENT_DATE);
  }

  public String getName() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_NAME);
  }

  public String getUserName() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_CREATED);
  }

  public String getTimeOfMovementDate() {
    return (String) getProperties().get(MappingConstants.SAP_WVINVE_TIME_OF_MOVEMENT_DATE);
  }

  @SuppressWarnings("unchecked")
  public List<InventoryCountLineTemplateComponent> getlines() {
    List<InventoryCountLineTemplateComponent> lineList = new ArrayList<>();
    List<SynchronizableBusinessObject> linesSbo = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_WVINVE_LINE);

    if (linesSbo == null) {
      return lineList;
    }
    for (SynchronizableBusinessObject lineSbo : linesSbo) {
      InventoryCountLineTemplateComponent inventoryCountLineSbo = new InventoryCountLineTemplateComponent(
          lineSbo.getProperties());
      lineList.add(inventoryCountLineSbo);
    }

    return lineList;
  }

}
