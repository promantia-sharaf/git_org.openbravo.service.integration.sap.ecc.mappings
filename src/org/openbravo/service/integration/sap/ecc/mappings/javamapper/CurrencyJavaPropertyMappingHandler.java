/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.ConversionRate;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JavaPropertyMappingHandler for the Currency entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = Currency.ENTITY_NAME)
public class CurrencyJavaPropertyMappingHandler extends JavaPropertyMappingHandler<Currency> {

  private static final long CURRENCY_PRECISION = 2L;

  private static final Logger log = LoggerFactory
      .getLogger(CurrencyJavaPropertyMappingHandler.class);

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(31, MappingConstants.SAP_ISACTIVE);
    properties.put(41, MappingConstants.SAP_WPDCUR_CONVERSION_RATE_SEGMENT);
    return properties;
  }

  @Override
  public void setPropertyInBaseOBObject(Currency currency, String mappingName,
      SynchronizableBusinessObject sbo) {
    Map<String, Object> properties = sbo.getProperties();
    if (!properties.containsKey(mappingName)) {
      log.warn("Property: {} is not included in SynchronizableBusinessObject {}", mappingName,
          properties.keySet());
      return;
    }
    try {
      Object itemValue = properties.get(mappingName);
      switch (mappingName) {
      case MappingConstants.SAP_ORGANIZATION:
        setOrganization(currency, itemValue);
        break;
      case MappingConstants.SAP_ISACTIVE:
        setIsActive(currency, (String) itemValue);
        break;
      case MappingConstants.SAP_WPDCUR_CONVERSION_RATE_SEGMENT:
        createConversionRates(currency, itemValue);
        break;
      default:
        log.warn("Java property not defined in the mapping: {}", mappingName);
      }
    } catch (Exception e) {
      log.error("Error while importing the property " + mappingName, e);
      throw new OBException(e);
    }
  }

  @SuppressWarnings("unchecked")
  private void createConversionRates(Currency currency, Object itemValue) {
    List<SynchronizableBusinessObject> conversionRates = (List<SynchronizableBusinessObject>) itemValue;
    for (SynchronizableBusinessObject sbo : conversionRates) {
      Currency toCurrency = getOrCreateCurrencyByIsoCode(sbo);
      addConversionRate(currency, sbo, toCurrency);
      addReversalConversionRate(toCurrency, sbo, currency);
    }
  }

  private ConversionRate createNewConversionRate(Currency currency, Currency toCurrency,
      BigDecimal multiplyBy, BigDecimal divideBy) {
    ConversionRate rate = OBProvider.getInstance().get(ConversionRate.class);
    rate.setOrganization(OBDal.getInstance().getProxy(Organization.class, "0"));
    rate.setNewOBObject(true);
    rate.setCurrency(currency);
    rate.setToCurrency(toCurrency);
    rate.setMultipleRateBy(multiplyBy);
    rate.setDivideRateBy(divideBy);
    rate.setConversionRateType("S");
    rate.setValidFromDate(Calendar.getInstance().getTime());
    OBDal.getInstance().save(rate);
    currency.getCurrencyConversionRateList().add(rate);
    return rate;
  }

  private void addReversalConversionRate(Currency toCurrency, SynchronizableBusinessObject sbo,
      Currency currency) {
    BigDecimal multiplyByRate = getMultiplyByRateFromSbo(sbo);
    createNewConversionRate(toCurrency, currency, getDivideRate(toCurrency, multiplyByRate),
        multiplyByRate);
  }

  private void addConversionRate(Currency currency, SynchronizableBusinessObject sbo,
      Currency toCurrency) {
    BigDecimal multiplyByRate = getMultiplyByRateFromSbo(sbo);
    createNewConversionRate(currency, toCurrency, multiplyByRate,
        getDivideRate(toCurrency, multiplyByRate));
  }

  private BigDecimal getDivideRate(Currency toCurrency, BigDecimal multiplyByRate) {
    return BigDecimal.ONE.divide(multiplyByRate, toCurrency.getStandardPrecision().intValue(),
        RoundingMode.HALF_UP);
  }

  private BigDecimal getMultiplyByRateFromSbo(SynchronizableBusinessObject sbo) {
    String multiplyBy = (String) sbo.getProperties().get(
        MappingConstants.SAP_WPDCUR_MULTIPLYBY_RATE);
    return new BigDecimal(multiplyBy);
  }

  private Currency getOrCreateCurrencyByIsoCode(SynchronizableBusinessObject sbo) {
    String isoCode = (String) sbo.getProperties().get(
        MappingConstants.SAP_WPDCUR_TO_CURRENCY_ISOCODE);
    Currency toCurrency = MappingUtils.getCurrencyByISOCode(isoCode);
    if (toCurrency == null) {
      toCurrency = createCurrency(isoCode, sbo);
    }
    return toCurrency;
  }

  private Currency createCurrency(String isoCode, SynchronizableBusinessObject sbo) {
    String currencyDescription = (String) sbo.getProperties().get(
        MappingConstants.SAP_WPDCUR_DESCRIPTION);
    Currency toCurrency = OBProvider.getInstance().get(Currency.class);
    toCurrency.setNewOBObject(true);
    toCurrency.setISOCode(isoCode);
    toCurrency.setDescription(currencyDescription);
    toCurrency.setStandardPrecision(CURRENCY_PRECISION);
    toCurrency.setCostingPrecision(CURRENCY_PRECISION);
    toCurrency.setPricePrecision(CURRENCY_PRECISION);
    OBDal.getInstance().save(toCurrency);
    return toCurrency;
  }

  private void setOrganization(Currency currency, Object itemValue) {
    currency.setOrganization(MappingUtils.getOrganization((String) itemValue));
  }

  private void setIsActive(Currency currency, String modi) {
    currency.setActive(StringUtils.equals(MappingConstants.SAP_ACTIVE, modi));
  }

}
