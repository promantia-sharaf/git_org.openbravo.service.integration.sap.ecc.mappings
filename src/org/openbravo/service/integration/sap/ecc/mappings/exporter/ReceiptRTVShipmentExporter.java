/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.exporter;

import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sapecc.process.SapEccExporterSynchronizableBusinessObjectExporter;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * SynchronizableBusinessObjectExporter that exports Receipt and Return to Vendor Shipment to WPUWBW documents
 * 
 * Returns the id of processed Goods Receipts and Return to Vendor Shipments created since the last export.
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = ShipmentInOut.ENTITY_NAME)
public class ReceiptRTVShipmentExporter extends SapEccExporterSynchronizableBusinessObjectExporter {

  @Override
  public String exportedEntityName() {
    return ShipmentInOut.ENTITY_NAME;
  }

  @Override
  public String getSynchronizationQuery() {
    StringBuilder query = new StringBuilder();
    query.append("select e.id from MaterialMgmtShipmentInOut e ");
    query.append(" WHERE e.movementType in ('V+', 'V-') ");
    query.append(" AND e.$incrementalCreationOnlyCriteria ");
    query.append(" AND e.$clientCriteria ");
    query.append(" AND e.processed = true ");
    return query.toString();
  }

  @Override
  protected String getIdocType() {
    return "WPUWBW101102";
  }

}
