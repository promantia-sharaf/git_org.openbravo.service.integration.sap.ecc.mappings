/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang.StringUtils;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocTemplate;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocType;

/**
 * Template Component for the WP_PLU template
 */
@ApplicationScoped
@IdocType("WP_PLU")
public class ProductTemplateComponent extends IdocTemplate {

  private static final String TEMPLATE_ID = "7039599F4F9644A5A36D299F163F02D5";

  @Override
  protected String getTemplateId() {
    return TEMPLATE_ID;
  }

  public String getFiliale() {
    return (String) getProperties().get(MappingConstants.SAP_ORGANIZATION);
  }

  public String getActive() {
    return (String) getProperties().get(MappingConstants.SAP_ISACTIVE);
  }

  public String getActivationDate() {
    return (String) getProperties().get(MappingConstants.SAP_WP_PLU_CREATION_DATE);
  }

  public String getProductUpcEan() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_WP_PLU_EAN), StringUtils.EMPTY);
  }

  public String getValue() {
    return (String) getProperties().get(MappingConstants.SAP_WP_PLU_SEARCH_KEY);
  }

  public String getUom() {
    return (String) getProperties().get(MappingConstants.SAP_WP_PLU_UOM);
  }

  public String getProductCategory() {
    return (String) getProperties().get(MappingConstants.SAP_WP_PLU_PRODUCT_CATEGORY);
  }

  public String getPriceListSapCode() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_WP_PLU_PRICE_LIST_NAME),
        StringUtils.EMPTY);
  }

  public String getPriceListVersionValidFrom() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_WP_PLU_VALID_FROM), StringUtils.EMPTY);
  }

  public String getStandardPrice() {
    BigDecimal standardPrice = (BigDecimal) getProperties().get(MappingConstants.SAP_WP_PLU_PRICE);
    if (standardPrice == null) {
      return StringUtils.EMPTY;
    }
    return standardPrice.toString();
  }

  public String getPriceListCurrency() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_WP_PLU_CURRENCY), StringUtils.EMPTY);
  }

  public String getTaxCategorySapCode() {
    return StringUtils.defaultIfBlank(
        (String) getProperties().get(MappingConstants.SAP_WP_PLU_TAX), StringUtils.EMPTY);
  }

  @SuppressWarnings("unchecked")
  public List<ProductTranslationTemplate> getTranslations() {
    List<ProductTranslationTemplate> translations = new ArrayList<>();
    List<SynchronizableBusinessObject> translationsSbo = (List<SynchronizableBusinessObject>) getProperties()
        .get(MappingConstants.SAP_WP_PLU_NAME_SEGMENT);
    if (translationsSbo == null) {
      return translations;
    }
    for (SynchronizableBusinessObject sbo : translationsSbo) {
      translations.add(new ProductTranslationTemplate(sbo.getProperties()));
    }
    return translations;
  }

}
