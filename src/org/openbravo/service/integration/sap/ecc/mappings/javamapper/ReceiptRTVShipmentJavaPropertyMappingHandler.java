/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openbravo.base.exception.OBException;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.mapping.templates.IdocDateUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for Goods Receipt and Return to Vendor Shipment entities of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = ShipmentInOut.ENTITY_NAME)
public class ReceiptRTVShipmentJavaPropertyMappingHandler extends
    JavaPropertyMappingHandler<ShipmentInOut> {

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_ORGANIZATION);
    properties.put(21, MappingConstants.SAP_DATE_WPUWBW);
    properties.put(31, MappingConstants.SAP_LINE_WPUWBW_SEGMENT);
    return properties;
  }

  @Override
  public Object getValueOfProperty(ShipmentInOut bob, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_ORGANIZATION:
      return getFiliale(bob);
    case MappingConstants.SAP_DATE_WPUWBW:
      return getDate(bob);
    case MappingConstants.SAP_LINE_WPUWBW_SEGMENT:
      return getLinesForReceipOrtRtvShipment(bob);
    default:
      return null;
    }
  }

  private Object getLinesForReceipOrtRtvShipment(ShipmentInOut mInOut) {
    List<SynchronizableBusinessObject> sboList = new ArrayList<>();
    for (ShipmentInOutLine mInOutLine : mInOut.getMaterialMgmtShipmentInOutLineList()) {
      sboList.add(getSboForReceipOrtRtvShipment(mInOutLine));
    }
    return sboList;
  }

  private SynchronizableBusinessObject getSboForReceipOrtRtvShipment(ShipmentInOutLine mInOutLine) {
    if (mInOutLine.getShipmentReceipt().getSalesOrder() == null) {
      throw new OBException("InOutLine was not created from SalesOrderLine");
    }
    SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
    sbo.addProperty(MappingConstants.SAP_WPUWBW_POREFERENCE, mInOutLine.getShipmentReceipt()
        .getSalesOrder().getDocumentNo());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_LINENO, mInOutLine.getSalesOrderLine().getLineNo());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_PRODUCT, mInOutLine.getProduct().getSearchKey());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_MOVEMENT_TYPE, "101");
    sbo.addProperty(MappingConstants.SAP_WPUWBW_UOM_EDI_CODE, mInOutLine.getUOM().getEDICode());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_MOVEMENT_QTY, mInOutLine.getMovementQuantity());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_BP_SEARCH_KEY, mInOutLine.getShipmentReceipt()
        .getBusinessPartner().getSearchKey());
    sbo.addProperty(MappingConstants.SAP_WPUWBW_WAREHOUSE,
        MappingUtils.getSAPCode(mInOutLine.getStorageBin().getWarehouse()));

    return sbo;
  }

  private Object getDate(ShipmentInOut bob) {
    return IdocDateUtils.getDatePart(bob.getMovementDate());
  }

  private Object getFiliale(ShipmentInOut bob) {
    return MappingUtils.getSAPFiliale(bob);
  }
}
