/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.component;

import java.math.BigDecimal;
import java.util.Map;

import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sapecc.mapping.templates.PropertiesHolder;

/**
 * Template Component for segment E1WVINI of the WVINVE template
 */
public class InventoryCountLineTemplateComponent implements PropertiesHolder {

  private Map<String, Object> properties;

  @Override
  public Map<String, Object> getProperties() {
    return properties;
  }

  public InventoryCountLineTemplateComponent(Map<String, Object> properties) {
    this.properties = properties;
  }

  public String getLineNo() {
    Long lineNo = (Long) properties.get(MappingConstants.SAP_WVINVE_LINE_NO);
    return lineNo.toString();
  }

  public String getProduct() {
    return (String) properties.get(MappingConstants.SAP_WVINVE_PRODUCT);
  }

  public String getQuantity() {
    BigDecimal quantity = (BigDecimal) properties.get(MappingConstants.SAP_WVINVE_QUANTITY);
    return quantity.toString();
  }

  public String getUomEdiCode() {
    return (String) properties.get(MappingConstants.SAP_WVINVE_UOM);
  }
}
