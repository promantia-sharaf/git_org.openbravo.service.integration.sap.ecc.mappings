/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.javamapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.exception.OBException;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.service.external.integration.SynchronizableBusinessObject;
import org.openbravo.service.external.integration.mapping.JavaPropertyMappingHandler;
import org.openbravo.service.external.integration.mapping.MappedEntity;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

/**
 * JavaPropertyMappingHandler for the InvoiceLine entity of SAP ECC
 */
@MappedEntity(systemType = SapEccConstants.SYSTEM_TYPE, entityName = InvoiceLine.ENTITY_NAME)
public class InvoiceLineJavaPropertyMappingHandler extends JavaPropertyMappingHandler<InvoiceLine> {

  private static final String SAP_IS_NEGATIVE_SIGN = "-";
  private static final String SAP_IS_POSITIVE_SIGN = "+";
  private static final String SAP_PRODUCT_CODE_SEARCH_KEY = "ARTN";
  private static final String SAP_PRODUCT_CODE_UPCEANN = "EANN";
  private static final String SAP_IS_SALE_CODE = "251";
  private static final String SAP_IS_RETURN_CODE = "252";

  @Override
  public Map<Integer, String> getPropertySorting() {
    Map<Integer, String> properties = new HashMap<>();
    properties.put(11, MappingConstants.SAP_INVOICELINE_PRODUCT_CODE);
    properties.put(21, MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE);
    properties.put(31, MappingConstants.SAP_INVOICELINE_RETURN_CODE);
    properties.put(41, MappingConstants.SAP_INVOICELINE_RETURN_SIGN);
    properties.put(51, MappingConstants.SAP_INVOICELINE_GROSS_AMOUNT);
    properties.put(61, MappingConstants.SAP_INVOICELINE_TAX_AMOUNT);
    properties.put(71, MappingConstants.SAP_TAX_RATE);
    properties.put(81, MappingConstants.SAP_INVOICE_PRICELIST_SAP_CODE_FIELD);
    properties.put(91, MappingConstants.SAP_INVOICE_SERIAL_NUMBER);
    properties.put(101, MappingConstants.SAP_INVOICELINE_DISCOUNT_SIGN);
    properties.put(111, MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT);
    properties.put(121, MappingConstants.SAP_INVOICELINE_PRICE_SIGN);
    properties.put(131, MappingConstants.SAP_INVOICELINE_TAX_RATE);
    properties.put(141, MappingConstants.SAP_INVOICELINE_QUANTITY);
    return properties;
  }

  @Override
  public Object getValueOfProperty(InvoiceLine invoiceLine, String mappingName) {
    switch (mappingName) {
    case MappingConstants.SAP_INVOICELINE_PRODUCT_CODE:
      return  SAP_PRODUCT_CODE_SEARCH_KEY;
    case MappingConstants.SAP_INVOICELINE_PRODUCT_CODE_VALUE:
      return invoiceLine.getProduct().getSearchKey();
    case MappingConstants.SAP_INVOICELINE_RETURN_CODE:
      return getLineCode(invoiceLine);
    case MappingConstants.SAP_INVOICELINE_RETURN_SIGN:
      return getSignForSalesOrReturns(invoiceLine);
    case MappingConstants.SAP_INVOICELINE_GROSS_AMOUNT:
      return getGrossAmount(invoiceLine);
    case MappingConstants.SAP_INVOICELINE_TAX_AMOUNT:
      return getTaxRateScaled(invoiceLine);
    case MappingConstants.SAP_TAX_RATE:
      return getSalesSAPCode(invoiceLine.getTax().getTaxCategory());
    case MappingConstants.SAP_INVOICELINE_TAX_RATE:
      return MappingUtils.getSAPCode(invoiceLine.getTax());
    case MappingConstants.SAP_INVOICE_PRICELIST_SAP_CODE_FIELD:
      return MappingUtils.getSAPCode(invoiceLine.getInvoice().getPriceList());
    case MappingConstants.SAP_INVOICE_SERIAL_NUMBER:
      return invoiceLine.getSalesOrderLine().getObposSerialNumber();
    case MappingConstants.SAP_INVOICELINE_PRICE_SIGN:
      return getSignForLinePrice(invoiceLine);
    case MappingConstants.SAP_INVOICELINE_DISCOUNT_SEGMENT:
      return getDiscount(invoiceLine);
    case MappingConstants.SAP_INVOICELINE_QUANTITY:
      return invoiceLine.getInvoicedQuantity().abs();
    default:
      return null;
    }
  }

  private BigDecimal getGrossAmount(InvoiceLine invoiceLine) {
    final BigDecimal grossAmountConsideringTaxesInPriceList = MappingUtils
        .getGrossAmountConsideringTaxesInPriceList(invoiceLine);
    if (invoiceLine.getInvoice().getPriceList().isPriceIncludesTax()) {
      return grossAmountConsideringTaxesInPriceList.add(MappingUtils
          .getTotalDiscountsFromInvoiceLine(invoiceLine).abs());
    }
    return grossAmountConsideringTaxesInPriceList;
  }

  private String getSalesSAPCode(TaxCategory taxCategory) {
    if (StringUtils.isBlank(taxCategory.getSapobmpSapCodeSales())) {
      throw new OBException(taxCategory + " does not have Sales SAP Code assigned");
    }
    return taxCategory.getSapobmpSapCodeSales();
  }

  /**
   * Get the sign to be printed in line segment in iDoc
   * 
   * @param invoiceLine
   *          The invoice line
   * @return The sign
   */
  protected String getSignForSalesOrReturns(InvoiceLine invoiceLine) {
    if (isPositiveOrderLine(invoiceLine)) {
      return SAP_IS_NEGATIVE_SIGN;
    } else {
      return SAP_IS_POSITIVE_SIGN;
    }
  }

  /**
   * Get the sign to be printed in price segment in iDoc
   * 
   * @param invoiceLine
   *          The invoice line
   * @return The sign
   */
  protected String getSignForLinePrice(InvoiceLine invoiceLine) {
    if (isPositiveOrderLine(invoiceLine)) {
      return SAP_IS_POSITIVE_SIGN;
    } else {
      return SAP_IS_NEGATIVE_SIGN;
    }
  }

  /**
   * Get the sign to be printed in discount segment in iDoc
   * 
   * @param lineOffer
   *          The discount line
   * @return The sign
   */
  protected String getSignForLineDiscount(InvoiceLineOffer lineOffer) {
    final boolean isPositiveOrderLine = isPositiveOrderLine(lineOffer.getInvoiceLine());
    final boolean isPositiveDiscountLine = isPositiveDiscountLine(lineOffer);

    if ((isPositiveOrderLine && isPositiveDiscountLine)
        || (!isPositiveOrderLine && isPositiveDiscountLine)) {
      return SAP_IS_NEGATIVE_SIGN;
    } else {
      return SAP_IS_POSITIVE_SIGN;
    }
  }

  private boolean isPositiveOrderLine(InvoiceLine invoiceLine) {
    return (invoiceLine.getSalesOrderLine().getLineNetAmount().signum() != 0 ? invoiceLine
        .getSalesOrderLine().getLineNetAmount().signum() >= 0 : (invoiceLine.getSalesOrderLine()
        .getOrderedQuantity()).signum() > 0);
  }

  private boolean isPositiveDiscountLine(InvoiceLineOffer lineOffer) {
    return lineOffer.getTotalAmount().signum() >= 0;
  }

  private String getLineCode(InvoiceLine invoiceLine) {
    if (invoiceLine.getSalesOrderLine().getSalesOrder().getDocumentType().isReturn()
        || invoiceLine.getSalesOrderLine().getSalesOrder().getCancelledorder() != null) {
      return SAP_IS_RETURN_CODE;
    }

    return SAP_IS_SALE_CODE;
  }

  private List<SynchronizableBusinessObject> getDiscount(InvoiceLine invoiceLine) {
    List<SynchronizableBusinessObject> sbos = new ArrayList<>();
    for (InvoiceLineOffer lineOffer : invoiceLine.getInvoiceLineOfferList()) {
      SynchronizableBusinessObject sbo = new SynchronizableBusinessObject();
      processInvoiceLineOffer(sbo, lineOffer);
      if (sbo.getProperties().containsKey(MappingConstants.SAP_INVOICELINE_DISCOUNT_AMOUNT)) {
        sbos.add(sbo);
      }
    }
    return sbos;
  }

  protected void processInvoiceLineOffer(SynchronizableBusinessObject sbo,
      InvoiceLineOffer lineOffer) {
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_SAP_CODE_FIELD,
        MappingUtils.getSAPCode(lineOffer.getPriceAdjustment()));
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_AMOUNT, lineOffer.getTotalAmount()
        .abs());
    sbo.addProperty(MappingConstants.SAP_INVOICELINE_DISCOUNT_SIGN,
        getSignForLineDiscount(lineOffer));
  }

  private BigDecimal getTaxRateScaled(InvoiceLine invoiceLine) {
    if (!invoiceLine.getInvoiceLineTaxList().isEmpty()) {
      return invoiceLine.getInvoiceLineTaxList().get(0).getTax().getRate().multiply(BigDecimal.TEN);
    }
    return BigDecimal.ZERO;
  }

  @Override
  protected boolean itemShouldBeExported(InvoiceLine invoiceLine) {
    return invoiceLine.getSalesOrderLine() != null;
  }
}
