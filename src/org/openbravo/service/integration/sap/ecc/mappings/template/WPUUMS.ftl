<#--
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

-->
<?xml version="1.0" encoding="utf-8"?>
<WPUUMS01>
  <IDOC BEGIN="1">
    <EDI_DC40 SEGMENT="1">
      <IDOCTYP>WPUUMS01</IDOCTYP>
      <MESTYP>WPUUMS</MESTYP>
      <SNDPOR>WPUX</SNDPOR>
      <SNDPRT>KU</SNDPRT>
      <SNDPRN>${data.filiale}</SNDPRN>
      <RCVPOR>SAPEED</RCVPOR>
      <RCVPRT>KU</RCVPRT>
      <RCVPRN>${data.filiale}</RCVPRN>
    </EDI_DC40>
    <E1WPU01 SEGMENT="1">
      <BELEGDATUM>${data.cashUpUpdate}</BELEGDATUM>
      <BELEGWAERS>EUR</BELEGWAERS>
    <#list data.invoiceLines as line>
      <E1WPU02 SEGMENT="1">
        <QUALARTNR>ARTN</QUALARTNR>
        <ARTNR>${line.product}</ARTNR>
        <VORZMENGE>${line.returnOrSale}</VORZMENGE>
        <UMSMENGE>${line.qtyInvoiced}</UMSMENGE>
        <UMSWERT>${line.grossAmount}</UMSWERT>
        <E1WPU05 SEGMENT="1">
          <TAXCODE>${line.taxRateSapCode}</TAXCODE>
          <TAXVALUE>${line.taxValue}</TAXVALUE>
        </E1WPU05>
      </E1WPU02>
    </#list>
    </E1WPU01>
  </IDOC>
</WPUUMS01>