/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test case explains how to export to SAP a physical inventory count made for an Openbravo
 * store.
 *
 */
public class SAPStd380 extends InboundTest {

  private static final String EXPORTED_FILE_NAME = "WVINVE.xml";
  private static final String WVINVE_ENTITY_MAPPING_ID = "494CAC3A0F16435E93247FDBC0ACB1AA";
  private static final String PHYSICAL_INVENTORY_ID = "81D15108951848F7BB8EB5CEF093B0E5";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return PHYSICAL_INVENTORY_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WVINVE_ENTITY_MAPPING_ID;
  }

}
