/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test case explains that while creating a new customer in Openbravo Web POS, only the address
 * configured as invoicing will be exported in WP_PER IDoc.
 * 
 */

public class SAPStd245 extends InboundTest {

  private static final String EXPORTED_FILE_NAME = "WP_PER_two_address.xml";
  private static final String WP_PER_ENTITY_MAPPING_ID = "523A046E62A04BE4815A329DF2E6A962";
  private static final String BUSINESS_PARTNER_ID = "BBBF3461CBB5470F68CCAC80ABF1C757";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return BUSINESS_PARTNER_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WP_PER_ENTITY_MAPPING_ID;
  }

}
