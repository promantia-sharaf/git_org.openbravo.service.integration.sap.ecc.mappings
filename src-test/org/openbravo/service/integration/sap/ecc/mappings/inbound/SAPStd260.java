/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test verifies exporting non aggregated sales in WPUBON document 
 *
 */
public class SAPStd260 extends SalesTest {

  private static final String EXPORTED_FILE_NAME = "WPUBON.xml";
  private static final String INVOICE_ID = "50882DD0B34C8C6F4A573A5EFB315CF7";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return INVOICE_ID;
  }
}
