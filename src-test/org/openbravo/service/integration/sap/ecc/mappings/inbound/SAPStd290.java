/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * 
 * This test case explains how to export a direct return sales done in Openbravo Web POS, through
 * WPUBON Idoc while POS terminal Type setup is generate invoices from orders = Yes. No grouping.
 * Price list used is including taxes.
 *
 */
public class SAPStd290 extends SalesTest {

  private static final String EXPORTED_FILE_NAME = "WPUBON-return.xml";
  private static final String INVOICE_ID = "EFD9D8330A9EAD1E4048CB22F75E3EFB";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return INVOICE_ID;
  }

}
