/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import java.io.IOException;

/**
 * SAP Inbound IDOC WPUUMS and WPUTAB (aggregated sales), including discounts and price including
 * taxes.
 *
 */

public class SAPStd320 extends SalesTest {

  private static final String[] entityMapping = { "AAC228E57EB040D7AE716E9E8DBD3F4A",
      "97C00AA067FD440EAB6C8B36B1F6AA38" };
  private static final String CASHUP_ID = "C757F79D03DF25DB4E1F90EC4D066130";
  private static final String[] fileNames = { "WPUUMS-discount.xml", "WPUTAB-discount.xml" };

  private int iteration = 0;

  @Override
  protected String getInboundFileName() {
    return fileNames[iteration];
  }

  @Override
  protected String getIdToExport() {
    return CASHUP_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return entityMapping[iteration];
  }

  @Override
  public void exportRecordToIdoc() throws IOException {
    for (; iteration < entityMapping.length; iteration++) {
      super.exportRecordToIdoc();
    }
  }

}
