/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants.DEFAULT_ASSORTMENT_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.retail.config.OBRETCOProductList;
import org.openbravo.retail.config.OBRETCOProlProduct;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;

/**
 * This test case explains how to Import customers in Openbravo (*) Organization, through WP_PER
 * idoc
 * 
 */
public class SAPStd160 extends OutBoundTest {

  private static final String EUR = "EUR";
  private static final String PRICE_LIST_NAME = "VKP0";
  private static final String PRODUCT_PRICE = "21";
  private static final String PRODUCT_CATEGORY_NAME = "PRODUCT CATEGORY 001";
  private static final String PRODUCT_SEARCH_KEY = "TEST-PRODUCT";
  private static final String PRODUCT_NAME = "Artículo XMF.....";
  private static final String PRODUCT_ENTITY_MAPPING = "86229831877E4AFE881A5AD2D6D00965";

  @Override
  protected File getIdocFileToImport() {
    File file = new File(OUTBOUND_BASE_PATH + "WP_PLU.xml");
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return PRODUCT_ENTITY_MAPPING;
  }

  @Override
  protected void prepareTest() {
    Product product = getProduct();
    if (product != null) {
      OBDal.getInstance().refresh(product);
      removeProductFromAssortment(product);
      OBDal.getInstance().remove(product);
      OBDal.getInstance().flush();
    }
  }

  private void removeProductFromAssortment(Product product) {
    OBCriteria<OBRETCOProlProduct> criteria = OBDal.getInstance().createCriteria(
        OBRETCOProlProduct.class);
    criteria.add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_OBRETCOPRODUCTLIST + "."
        + OBRETCOProductList.PROPERTY_ID, DEFAULT_ASSORTMENT_ID));
    criteria.add(Restrictions.eq(OBRETCOProlProduct.PROPERTY_PRODUCT, product));

    OBRETCOProlProduct productInAssortment = (OBRETCOProlProduct) criteria.uniqueResult();
    if (productInAssortment != null) {
      OBDal.getInstance().remove(productInAssortment);
    }
  }

  private Product getProduct() {
    OBCriteria<Product> bpCriteria = OBDal.getInstance().createCriteria(Product.class);
    bpCriteria.add(Restrictions.eq(Product.PROPERTY_SEARCHKEY, PRODUCT_SEARCH_KEY));
    bpCriteria.setMaxResults(1);
    return (Product) bpCriteria.uniqueResult();
  }

  @Override
  protected void doAsserts() {
    final Product product = OBDal.getInstance().get(Product.class, getProduct().getId());
    OBDal.getInstance().refresh(product);
    if (product != null) {
      assertProductCategory(product);
      assertName(product);
      assertTaxCategory(product);
      assertPriceListSettings(product);
    } else {
      fail("No Product found");
    }
  }

  private void assertPriceListSettings(Product product) {
    ProductPrice productPrice = product.getPricingProductPriceList().get(0);
    assertTrue("The Product List Price should be \"" + PRODUCT_PRICE + "\"", productPrice
        .getListPrice().toString().equals(PRODUCT_PRICE));
    assertTrue("The Product Standard Price should be \"" + PRODUCT_PRICE + "\"", productPrice
        .getStandardPrice().toString().equals(PRODUCT_PRICE));
    assertTrue("The Price List Version name should be \"" + PRICE_LIST_NAME + "\"", productPrice
        .getPriceListVersion().getName().equals(PRICE_LIST_NAME));
    assertTrue("The Price List Schema name should be \"" + PRICE_LIST_NAME + "\"", productPrice
        .getPriceListVersion().getPriceListSchema().getName().equals(PRICE_LIST_NAME));
    PriceList priceList = productPrice.getPriceListVersion().getPriceList();
    assertTrue("The Price List should include taxes ", priceList.isPriceIncludesTax() == true);
    assertTrue("The Price List SAP CODE should be \"" + PRICE_LIST_NAME + "\"", priceList
        .getSapobmpSapCode().equals(MappingConstants.DEFAULT_PRICE_LIST_SAP_CODE_VALUE));
    assertTrue("The Price List Currency should be \"" + EUR + "\"", priceList.getCurrency()
        .getISOCode().equals(EUR));
  }

  private void assertTaxCategory(Product product) {
    assertTrue("The Tax Category name should be \"TEST-E1\"", product.getTaxCategory().getName()
        .equals("TEST-E1"));
  }

  private void assertName(Product product) {
    assertTrue("The Product name should be \"Artículo XMF.....\"",
        product.getName().equals(PRODUCT_NAME));
  }

  private void assertProductCategory(Product product) {
    assertTrue("The Product Category should be \"" + PRODUCT_CATEGORY_NAME + "\"", product
        .getProductCategory().getName().equals(PRODUCT_CATEGORY_NAME));
  }

}
