/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.financialmgmt.tax.TaxRate;

/**
 * This test case explains how to import the second SAP Outbound IDoc to import, as this IDOC
 * contains mandatory information to properly import product in Openbravo.
 * 
 */
public class SAPStd110 extends OutBoundTest {

  private static final String TAX_NAME = "TEST-E1";
  private static final String TAX_ENTITY_MAPPING = "C2DBE15FC94848D2B41D894B0F7844EF";

  @Override
  protected File getIdocFileToImport() {
    File file = new File(OUTBOUND_BASE_PATH + "WPDTAX.xml");
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return TAX_ENTITY_MAPPING;
  }

  @Override
  protected void prepareTest() {
    TaxRate tax = getTaxRateByName();
    if (tax != null) {
      OBDal.getInstance().remove(tax);
    }

    TaxCategory taxCategory = getTaxCategoryByName();
    if (taxCategory != null) {
      OBDal.getInstance().remove(taxCategory);
    }
    OBDal.getInstance().flush();
  }

  private TaxRate getTaxRateByName() {
    OBCriteria<TaxRate> taxCriteria = OBDal.getInstance().createCriteria(TaxRate.class);
    taxCriteria.add(Restrictions.eq(TaxRate.PROPERTY_NAME, TAX_NAME));
    TaxRate tax = (TaxRate) taxCriteria.uniqueResult();
    return tax;
  }

  private TaxCategory getTaxCategoryByName() {
    OBCriteria<TaxCategory> taxCategoryCriteria = OBDal.getInstance().createCriteria(
        TaxCategory.class);
    taxCategoryCriteria.add(Restrictions.eq(TaxRate.PROPERTY_NAME, TAX_NAME));
    TaxCategory taxCategory = (TaxCategory) taxCategoryCriteria.uniqueResult();
    return taxCategory;
  }

  @Override
  protected void doAsserts() {
    assertTaxCategory();
    assertTaxRate();
  }

  private void assertTaxRate() {
    TaxRate taxRate = getTaxRateByName();
    assertTrue("Tax Rate Sales Purchase type should be [S]",
        taxRate.getSalesPurchaseType().equals("S"));
    assertTrue("Tax Rate Tax Category name should be [" + TAX_NAME + "]", taxRate.getTaxCategory()
        .getName().equals(TAX_NAME));
    assertTrue("Tax Rate should not have Tax Zone", taxRate.getFinancialMgmtTaxZoneList().isEmpty());
    assertTrue("Tax Rate Document Tax Amount should be [D]", taxRate.getDocTaxAmount().equals("D"));
    assertTrue("Tax Rate Base Amount should be [LNA]", taxRate.getBaseAmount().equals("LNA"));
  }

  private void assertTaxCategory() {
    TaxCategory taxCategory = getTaxCategoryByName();
    assertTrue("Tax Category must have name SAP Code [" + TAX_NAME + "]", taxCategory
        .getSapobmpSapCode().equals(TAX_NAME));

  }

}
