/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import org.junit.Before;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.retail.posterminal.OBPOSApplications;

public abstract class SalesTest extends InboundTest {

  private static final String CARD_FIN_PAYMENTMETHOD_SAP_CODE = "WTR";
  private static final String CARD_FIN_PAYMENTMETHOD_ID = "5A741F883A31408CA3AC097D76AA6D32";
  private static final String DISCOUNT_5_PERCENT_OFFER_SAP_CODE = "PROMO1";
  private static final String DISCOUNT_5_PERCENT_OFFER_ID = "08C2C89DB15443478B659A8645828F63";
  private static final String SALES_PRICELIST_SAP_CODE = "S01";
  protected static final String SALES_PRICELIST_ID = "496CF965DF9744D2A41248392D1DE407";
  private static final String ENTREGAS_IVA_21_TAX_RATE_SAP_CODE = "EI21";
  private static final String ENTREGAS_IVA_21_TAX_RATE_ID = "5235D8E99A2749EFA17A5C92A52AEFC6";
  private static final String CASH_FIN_PAYMENTMETHOD_SAP_CODE = "CS01";
  private static final String CASH_FIN_PAYMENTMETHOD_ID = "45A202BF44884F05B8A1BF741E2063B6";
  private static final String IVA_NORMAL_TAX_CATEGORY_SAP_CODE = "TX01";
  private static final String IVA_NORMAL_TAX_CATEGORY_ID = "FF80818123B7FC160123B804AB88000B";
  private static final String VBS1_TERMINAL_SAP_CODE = "ST01";
  private static final String VBS1_TERMINAL_ID = "9104513C2D0741D4850AE8493998A7C8";
  protected static final String WPUBON_ENTITY_MAPPING_ID = "2744E244A5DB4E49A1C253DBDB81D003";

  @Override
  protected String getEntityMappingId() {
    return WPUBON_ENTITY_MAPPING_ID;
  }

  @Before
  public void init() {
    super.init();
    OBContext.setAdminMode(false);
    try {
      mapTerminal();
      mapTaxCategory();
      mapFinPaymentMethod();
      mapTaxRate();
      mapPriceList();
      mapOffer();
      configureSalesPriceListIncludingTaxes();
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void configureSalesPriceListIncludingTaxes() {
    PriceList salesPriceList = OBDal.getInstance().get(PriceList.class, SALES_PRICELIST_ID);
    salesPriceList.setPriceIncludesTax(true);
  }

  private void mapOffer() {
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class,
        DISCOUNT_5_PERCENT_OFFER_ID);
    if (offer.getSapobmpSapCode() == null) {
      offer.setSapobmpSapCode(DISCOUNT_5_PERCENT_OFFER_SAP_CODE);
    }
  }

  private void mapPriceList() {
    PriceList priceList = OBDal.getInstance().get(PriceList.class, SALES_PRICELIST_ID);
    if (priceList.getSapobmpSapCode() == null) {
      priceList.setSapobmpSapCode(SALES_PRICELIST_SAP_CODE);
    }
  }

  private void mapTaxRate() {
    TaxRate taxRate = OBDal.getInstance().get(TaxRate.class, ENTREGAS_IVA_21_TAX_RATE_ID);
    if (taxRate.getSapobmpSapCode() == null) {
      taxRate.setSapobmpSapCode(ENTREGAS_IVA_21_TAX_RATE_SAP_CODE);
    }
  }

  private void mapFinPaymentMethod() {
    FIN_PaymentMethod cash = OBDal.getInstance().get(FIN_PaymentMethod.class,
        CASH_FIN_PAYMENTMETHOD_ID);
    if (cash.getSapobmpSapCode() == null) {
      cash.setSapobmpSapCode(CASH_FIN_PAYMENTMETHOD_SAP_CODE);
    }

    FIN_PaymentMethod card = OBDal.getInstance().get(FIN_PaymentMethod.class,
        CARD_FIN_PAYMENTMETHOD_ID);
    if (card.getSapobmpSapCode() == null) {
      card.setSapobmpSapCode(CARD_FIN_PAYMENTMETHOD_SAP_CODE);
    }

  }

  private void mapTaxCategory() {
    TaxCategory taxCategory = OBDal.getInstance()
        .get(TaxCategory.class, IVA_NORMAL_TAX_CATEGORY_ID);
    if (taxCategory.getSapobmpSapCodeSales() == null) {
      taxCategory.setSapobmpSapCodeSales(IVA_NORMAL_TAX_CATEGORY_SAP_CODE);
    }
  }

  private void mapTerminal() {
    OBPOSApplications terminal = OBDal.getInstance().get(OBPOSApplications.class, VBS1_TERMINAL_ID);
    if (terminal.getSapobmpSapTerminal() == null) {
      terminal.setSapobmpSapTerminal(VBS1_TERMINAL_SAP_CODE);
    }
  }

}
