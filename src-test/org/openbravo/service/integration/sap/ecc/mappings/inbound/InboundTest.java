/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.CLIENT_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.INBOUND_BASE_PATH;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.LANGUAGE;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.ORG_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.ROLE_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.USER_ID;

import java.io.File;

import org.hibernate.Query;
import org.junit.Before;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.service.external.integration.EntityMapping;
import org.openbravo.service.external.integration.export.SynchronizableBusinessObjectExporter;
import org.openbravo.service.external.integration.export.SynchronizableBusinessObjectExporterStore;
import org.openbravo.service.integration.sapecc.edl.IdocExportTestHelper;
import org.openbravo.service.integration.sapecc.util.SapEccConstants;

public abstract class InboundTest extends IdocExportTestHelper {

  private static final String ALL_ORGANIZATION_SAP_CODE = "S001";
  private static final String WHITE_VALLEY_GROUP_ORGANIZATION_SAP_CODE = "S005";
  private static final String WHITE_VALLEY_GROUP_ORGANIZATION_ID = "67839EEFA49E44AC969BD60093FCC899";
  private static final String VALLBLANCASTORE_ORG_SAPFILIALE = "S00A";
  private static final String VALLBLANCASTORE_WHSE_SAPCODE = "VS01";
  private static final String VALLBLANCASTORE_WHSE_ID = "A154EC30A296479BB078B0AFFD74CA22";
  private static final String VALLBLANCASTORE_ORG_ID = "D270A5AC50874F8BA67A88EE977F8E3B";

  @Before
  public void init() {
    OBContext.setOBContext(USER_ID, ROLE_ID, CLIENT_ID, ORG_ID, LANGUAGE);
    OBContext.setAdminMode(false);
    try {
      mapOrganizationWithSAPFiliale();
      mapWarehouseWithSAPCode();
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
    mapAllOrganizationToSAPFiliale();
  }

  public void mapAllOrganizationToSAPFiliale() {
    StringBuilder hql = new StringBuilder("update ad_org set em_sapobmp_filiale = '"
        + ALL_ORGANIZATION_SAP_CODE + "' where ad_org_id = '" + ORG_ID + "'");
    Query query = OBDal.getInstance().getSession().createSQLQuery(hql.toString());
    query.executeUpdate();
    OBDal.getInstance().commitAndClose();
  }

  private void mapWarehouseWithSAPCode() {
    Warehouse warehouse = OBDal.getInstance().get(Warehouse.class, VALLBLANCASTORE_WHSE_ID);
    if (warehouse.getSapobmpSapCode() == null) {
      warehouse.setSapobmpSapCode(VALLBLANCASTORE_WHSE_SAPCODE);
    }

  }

  private void mapOrganizationWithSAPFiliale() {
    Organization organization = OBDal.getInstance().get(Organization.class, VALLBLANCASTORE_ORG_ID);
    if (organization.getSapobmpFiliale() == null) {
      organization.setSapobmpFiliale(VALLBLANCASTORE_ORG_SAPFILIALE);
    }

    organization = OBDal.getInstance().get(Organization.class, WHITE_VALLEY_GROUP_ORGANIZATION_ID);
    if (organization.getSapobmpFiliale() == null) {
      organization.setSapobmpFiliale(WHITE_VALLEY_GROUP_ORGANIZATION_SAP_CODE);
    }

  }

  @Override
  protected SynchronizableBusinessObjectExporter getSynchronizableBusinessObjectExporter() {
    SynchronizableBusinessObjectExporterStore exporterStore = WeldUtils
        .getInstanceFromStaticBeanManager(SynchronizableBusinessObjectExporterStore.class);
    return exporterStore.getSynchronizableBusinessObjectExporter(SapEccConstants.SYSTEM_TYPE, OBDal
        .getInstance().get(EntityMapping.class, getEntityMappingId()).getMappingEntity());
  }

  protected abstract String getInboundFileName();

  @Override
  protected File getExpectedIdocFile() {
    File file = new File(INBOUND_BASE_PATH + getInboundFileName());
    return file;
  }
}
