/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import java.io.IOException;

import org.junit.Before;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.pricing.pricelist.PriceList;

/**
 * SAP Inbound IDOC WPUUMS and WPUTAB (aggregated returns), including discounts and price excluding
 * taxes. excluding taxes.
 *
 */

public class SAPStd335 extends SalesTest {

  private static final String[] entityMapping = { "AAC228E57EB040D7AE716E9E8DBD3F4A",
      "97C00AA067FD440EAB6C8B36B1F6AA38" };
  private static final String CASHUP_ID = "C124EF33930A22FCE0DEB46AE8126A09";
  private static final String[] fileNames = { "WPUUMS-return-discount.xml",
      "WPUTAB-return-discount.xml" };

  private int iteration = 0;

  @Override
  protected String getInboundFileName() {
    return fileNames[iteration];
  }

  @Override
  protected String getIdToExport() {
    return CASHUP_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return entityMapping[iteration];
  }

  @Override
  public void exportRecordToIdoc() throws IOException {
    for (; iteration < entityMapping.length; iteration++) {
      super.exportRecordToIdoc();
    }
  }

  @Before
  public void init() {
    super.init();
    OBContext.setAdminMode(false);
    try {
      configureSalesPriceListExcludingTaxes();
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void configureSalesPriceListExcludingTaxes() {
    PriceList salesPriceList = OBDal.getInstance().get(PriceList.class, SALES_PRICELIST_ID);
    salesPriceList.setPriceIncludesTax(false);
  }

}
