/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SAPStd215.class,//
    SAPStd223.class,//
    SAPStd245.class,//
    SAPStd260.class,//
    SAPStd290.class,//
    SAPStd303.class,//
    SAPStd309.class,//
    SAPStd310.class,//
    SAPStd320.class,//
    SAPStd335.class,//
    SAPStd350.class,//
    SAPStd360.class,//
    SAPStd370.class,//
    SAPStd380.class })
public class SapEccInboundTestSuite {
  // No content is required, this is just the definition of a test suite.
}
