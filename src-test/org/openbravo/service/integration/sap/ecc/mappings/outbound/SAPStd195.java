/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.service.external.integration.EntityMapping;
import org.openbravo.service.external.integration.EntitySynchronization;
import org.openbravo.service.external.integration.process.HqlQueryBuilder;
import org.openbravo.service.external.integration.process.HqlQueryContextInfo;
import org.openbravo.service.external.integration.process.MalformedQueryException;
import org.openbravo.service.integration.sap.ecc.mappings.exporter.BusinessPartnerExporter;

/**
 * This test case explains that a customer imported from SAP is not exported in case there is no
 * modification executed on it.
 * 
 */
public class SAPStd195 extends OutBoundTest {

  private static final String BUSINESS_PARTNER_SEARCH_KEY = "TEST-BP";
  private static final String BUSINESS_PARTNER_ENTITY_MAPPING = "523A046E62A04BE4815A329DF2E6A962";

  @Override
  protected File getIdocFileToImport() {
    File file = new File(OUTBOUND_BASE_PATH + "WP_PER.xml");
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return BUSINESS_PARTNER_ENTITY_MAPPING;
  }

  @Override
  protected void prepareTest() {
    BusinessPartner businessPartner = getBusinessPartner();
    if (businessPartner != null) {
      OBDal.getInstance().remove(businessPartner);
      OBDal.getInstance().flush();
    }
  }

  private BusinessPartner getBusinessPartner() {
    OBCriteria<BusinessPartner> bpCriteria = OBDal.getInstance().createCriteria(
        BusinessPartner.class);
    bpCriteria
        .add(Restrictions.eq(BusinessPartner.PROPERTY_SEARCHKEY, BUSINESS_PARTNER_SEARCH_KEY));
    bpCriteria.setMaxResults(1);
    return (BusinessPartner) bpCriteria.uniqueResult();
  }

  @Override
  protected void doAsserts() {
    assertImportedBusinessPartnerWillNotBeBeExported();
  }

  private void assertImportedBusinessPartnerWillNotBeBeExported() {
    HqlQueryBuilder queryBuilder = WeldUtils
        .getInstanceFromStaticBeanManager(HqlQueryBuilder.class);
    BusinessPartnerExporter exporter = new BusinessPartnerExporter();
    try {
      Query query = queryBuilder.getDalQuery(exporter.getSynchronizationQuery(),
          new HqlQueryContextInfo(OBContext.getOBContext().getCurrentClient().getId(), null,
              getLastUpdate()));
      final String importedBusinessPartner = getBusinessPartnerIdBySearchKey();
      assertTrue("The imported Business Partner" + importedBusinessPartner
          + " will not be exported", !query.list().contains(importedBusinessPartner));
    } catch (MalformedQueryException e) {
      e.printStackTrace();
    }
  }

  private String getBusinessPartnerIdBySearchKey() {
    OBCriteria<BusinessPartner> businessPartnerCriteria = OBDal.getInstance().createCriteria(
        BusinessPartner.class);
    businessPartnerCriteria.add(Restrictions.eq(BusinessPartner.PROPERTY_SEARCHKEY,
        BUSINESS_PARTNER_SEARCH_KEY));
    BusinessPartner businessPartner = (BusinessPartner) businessPartnerCriteria.uniqueResult();
    return businessPartner.getId();
  }

  private Date getLastUpdate() {
    OBCriteria<EntitySynchronization> entitySychronizationCriteria = OBDal.getInstance()
        .createCriteria(EntitySynchronization.class);
    entitySychronizationCriteria.add(Restrictions.eq(
        EntitySynchronization.PROPERTY_OBEIENTITYMAPPING + "." + EntityMapping.PROPERTY_ID,
        getEntityMappingId()));
    entitySychronizationCriteria.add(Restrictions.eq(
        EntitySynchronization.PROPERTY_INTEGRATIONTYPE, "EXPORT"));
    Date lastUpdate = null;
    EntitySynchronization entitySynchronization = (EntitySynchronization) entitySychronizationCriteria
        .uniqueResult();
    if (entitySynchronization != null) {
      lastUpdate = entitySynchronization.getLastSynchronization();
    }
    return lastUpdate;
  }

}
