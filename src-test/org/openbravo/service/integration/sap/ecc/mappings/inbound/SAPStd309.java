/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import org.junit.Before;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.pricing.pricelist.PriceList;

/**
 * This test case explains how to export a direct return sale including discounts and price
 * excluding taxes.
 *
 */
public class SAPStd309 extends SalesTest {

  private static final String EXPORTED_FILE_NAME = "WPUBON-return-discount.xml";
  private static final String INVOICE_ID = "74B0C2EBF1BF4CD2D45755863457BD7D";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return INVOICE_ID;
  }

  @Before
  public void init() {
    super.init();
    OBContext.setAdminMode(false);
    try {
      configureSalesPriceListExcludingTaxes();
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void configureSalesPriceListExcludingTaxes() {
    PriceList salesPriceList = OBDal.getInstance().get(PriceList.class, SALES_PRICELIST_ID);
    salesPriceList.setPriceIncludesTax(false);
  }

}
