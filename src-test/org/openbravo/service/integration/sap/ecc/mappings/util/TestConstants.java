/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.util;

public class TestConstants {

  public static final String OUTBOUND_BASE_PATH = "./modules/org.openbravo.service.integration.sap.ecc.mappings/src-test/data/outbound/";
  public static final String INBOUND_BASE_PATH = "./modules/org.openbravo.service.integration.sap.ecc.mappings/src-test/data/inbound/";
  public static final String USER_ID = "100";
  public static final String ROLE_ID = "E717F902C44C455793463450495FF36B";
  public static final String CLIENT_ID = "39363B0921BB4293B48383844325E84C";
  public static final String ORG_ID = "0";
  public static final String LANGUAGE = "en_US";
}
