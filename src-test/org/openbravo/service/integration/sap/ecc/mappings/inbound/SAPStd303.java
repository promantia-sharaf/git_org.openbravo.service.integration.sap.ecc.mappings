/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test case explains how to export a direct POS Sale to SAP, including discounts and price
 * including taxes.
 *
 */
public class SAPStd303 extends SalesTest {

  private static final String EXPORTED_FILE_NAME = "WPUBON-discount.xml";
  private static final String INVOICE_ID = "45BFB8116E11A4FABCA10FB48654BC27";
  
  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return INVOICE_ID;
  }

}
