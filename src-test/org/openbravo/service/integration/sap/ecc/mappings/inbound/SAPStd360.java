/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test verifies exporting Return To Vendor Shipments
 *
 */
public class SAPStd360 extends InboundTest {

  private static final String EXPORTED_FILE_NAME = "WPUWBW102.xml";
  private static final String RETURN_TO_VENDOR_SHIPMENT_ID = "70215852251A42D081A10702677FBD81";
  private static final String WPUWBW_ENTITY_MAPPING_ID = "EB7A4E4EFA954733A747B539D1EC8F6B";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return RETURN_TO_VENDOR_SHIPMENT_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WPUWBW_ENTITY_MAPPING_ID;
  }

}
