/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.CLIENT_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.LANGUAGE;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.ORG_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.ROLE_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.USER_ID;

import org.hibernate.Query;
import org.junit.Before;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.service.integration.sapecc.edl.IdocImportTestHelper;

public abstract class OutBoundTest extends IdocImportTestHelper {

  private static final String ALL_ORGANIZATION_SAP_CODE = "S001";

  @Before
  public void init() {
    OBContext.setOBContext(USER_ID, ROLE_ID, CLIENT_ID, ORG_ID, LANGUAGE);
    mapAllOrganizationToSAPFiliale();
  }

  protected void mapAllOrganizationToSAPFiliale() {
    StringBuilder hql = new StringBuilder("update ad_org set em_sapobmp_filiale = '"
        + ALL_ORGANIZATION_SAP_CODE + "' where ad_org_id = '" + ORG_ID + "'");
    Query query = OBDal.getInstance().getSession().createSQLQuery(hql.toString());
    query.executeUpdate();
    OBDal.getInstance().commitAndClose();
  }

  protected void unMapAllOrganization() {
    StringBuilder hql = new StringBuilder(
        "update ad_org set em_sapobmp_filiale = null  where ad_org_id = '" + ORG_ID + "'");
    Query query = OBDal.getInstance().getSession().createSQLQuery(hql.toString());
    query.executeUpdate();
    OBDal.getInstance().commitAndClose();
  }

  @Override
  protected void doAsserts() {
  }
}
