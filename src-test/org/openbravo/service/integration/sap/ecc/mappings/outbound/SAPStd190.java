/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.ORG_ID;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingConstants;

/**
 * This test case explains how to Import customers in Openbravo (*) Organization, through WP_PER
 * idoc
 * 
 */
public class SAPStd190 extends OutBoundTest {

  private static final String BUSINESS_PARTNER_LOCATION_ADDRESS = "Mayor 2 -  - 31600 - Mutilva -  - Spain";
  private static final String BUSINESS_PARTNER_SEARCH_KEY = "TEST-BP";
  private static final String BUSINESS_PARTNER_NAME = "Customer test updated with address";
  private static final String BUSINESS_PARTNER_ENTITY_MAPPING = "523A046E62A04BE4815A329DF2E6A962";

  @Override
  protected File getIdocFileToImport() {
    File file = new File(OUTBOUND_BASE_PATH + "WP_PER.xml");
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return BUSINESS_PARTNER_ENTITY_MAPPING;
  }

  @Override
  protected void prepareTest() {
    BusinessPartner businessPartner = getBusinessPartner();
    if (businessPartner != null) {
      OBDal.getInstance().remove(businessPartner);
      OBDal.getInstance().flush();
    }
  }

  private BusinessPartner getBusinessPartner() {
    OBCriteria<BusinessPartner> bpCriteria = OBDal.getInstance().createCriteria(
        BusinessPartner.class);
    bpCriteria
        .add(Restrictions.eq(BusinessPartner.PROPERTY_SEARCHKEY, BUSINESS_PARTNER_SEARCH_KEY));
    bpCriteria.setMaxResults(1);
    return (BusinessPartner) bpCriteria.uniqueResult();
  }

  @Override
  protected void doAsserts() {
    final BusinessPartner businessPartner = getBusinessPartner();
    if (businessPartner != null) {
      assertBusinessPartnerCategory(businessPartner);
      assertBusinessPartnerName(businessPartner);
      assertBusinessPartnerAddress(businessPartner);
      assertPriceList(businessPartner);
      assertPaymentSettings(businessPartner);
    } else {
      fail("No Business Partner found");
    }

  }

  private void assertPaymentSettings(BusinessPartner businessPartner) {
    assertTrue("The Payment Method should be \"Cash\"", businessPartner.getPaymentMethod()
        .getSapobmpSapCode().equals(MappingConstants.DEFAULT_PAYMENT_METHOD_SAP_CODE_VALUE));
    assertTrue("The Payment Term shoud be \"Treinta días\"", businessPartner.getPaymentTerms()
        .getId().equals(MappingConstants.DEFAULT_PAYMENT_TERM_ID));
  }

  private void assertPriceList(BusinessPartner businessPartner) {
    assertTrue("The price list should be a Sales Price List at Organization \"0\"", businessPartner
        .getPriceList().isSalesPriceList() == true
        && businessPartner.getPriceList().getOrganization().getId().equals(ORG_ID));
  }

  private void assertBusinessPartnerAddress(BusinessPartner businessPartner) {
    final Location businessPartnerLocation = businessPartner.getBusinessPartnerLocationList()
        .get(0);
    assertTrue("The Business Partner Location Address should be \""
        + BUSINESS_PARTNER_LOCATION_ADDRESS + "\"", businessPartnerLocation.getLocationAddress()
        .getIdentifier().equals(BUSINESS_PARTNER_LOCATION_ADDRESS));
    assertTrue("The location should be set for shipping",
        businessPartnerLocation.isShipToAddress() == true);
    assertTrue("The location should be set for invoicing",
        businessPartnerLocation.isInvoiceToAddress() == true);
  }

  private void assertBusinessPartnerName(BusinessPartner businessPartner) {
    assertTrue("The Business Partner Name shoud be \"" + BUSINESS_PARTNER_NAME + "\"",
        businessPartner.getName().equals(BUSINESS_PARTNER_NAME));
  }

  private void assertBusinessPartnerCategory(BusinessPartner businessPartner) {
    assertTrue("The Business Partner Category should be \"POS Customer\"", businessPartner
        .getBusinessPartnerCategory().getName().equals(MappingConstants.DEFAULT_BP_CATEGORY_NAME));
  }

}
