/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import org.junit.Before;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.pricing.pricelist.PriceList;

/**
 * This test verifies exporting existing product modification executed in Product window
 *
 */
public class SAPStd215 extends InboundTest {

  private static final String PRICELIST_SAPCODE = "S01";
  private static final String PRICELIST_ID = "496CF965DF9744D2A41248392D1DE407";
  private static final String EXPORTED_FILE_NAME = "WP_PLU.xml";
  private static final String PRODUCT_CATEGORY_TAX_CODE = "TX001";
  private static final String PRODUCT_CATEGORY_SAP_CODE = "S001";
  private static final String TAX_CATEGORY_ID = "FF80818123B7FC160123B804AB860003";
  private static final String PRODUCT_CATEGORY_ID = "9BE78BE519494875B84A004EFD4B5ADC";
  private static final String WP_PLU_ENTITY_MAPPING_ID = "86229831877E4AFE881A5AD2D6D00965";
  private static final String GPS_MINI_PRODUCT_ID = "C00AA6D451794C4AA2DD432EC856E0B6";

  @Override
  protected String getIdToExport() {
    return GPS_MINI_PRODUCT_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WP_PLU_ENTITY_MAPPING_ID;
  }

  @Before
  public void init() {
    super.init();

    productCategorySetUp();
    taxCategorySetUp();
    priceListSetup();

    OBDal.getInstance().flush();
  }

  private void priceListSetup() {
    PriceList priceList = OBDal.getInstance().get(PriceList.class, PRICELIST_ID);
    priceList.setSapobmpSapCode(PRICELIST_SAPCODE);

  }

  private void taxCategorySetUp() {
    TaxCategory taxCategory = OBDal.getInstance().get(TaxCategory.class, TAX_CATEGORY_ID);
    taxCategory.setSapobmpSapCode(PRODUCT_CATEGORY_TAX_CODE);
  }

  private void productCategorySetUp() {
    ProductCategory productCategory = OBDal.getInstance().get(ProductCategory.class,
        PRODUCT_CATEGORY_ID);
    productCategory.setSapobmpSapCode(PRODUCT_CATEGORY_SAP_CODE);
  }

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }
}
