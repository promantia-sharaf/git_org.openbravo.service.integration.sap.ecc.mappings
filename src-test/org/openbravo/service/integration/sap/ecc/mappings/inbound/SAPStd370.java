/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import java.io.IOException;

/**
 * This test verifies exporting Goods Movements
 *
 */
public class SAPStd370 extends InboundTest {

  private static final String GOODS_MOVEMENT_ID = "64DDF540159A44EFB5419A58DE6F050D";
  private int iteration = 0;
  private String[] fileNames = { "WPUWBW303.xml", "WPUWBW305.xml" };
  private String[] entityMappingsId = { "23751E2DDE1A401E92D19B75EAF38373",
      "CD70C4275B594C94A672159DE6F03F60" };

  @Override
  protected String getInboundFileName() {
    return fileNames[iteration];
  }

  @Override
  protected String getIdToExport() {
    return GOODS_MOVEMENT_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return entityMappingsId[iteration];
  }
  
  @Override
  public void exportRecordToIdoc() throws IOException {
    for(; iteration < entityMappingsId.length; iteration++) {
      super.exportRecordToIdoc();
    }
  }

}
