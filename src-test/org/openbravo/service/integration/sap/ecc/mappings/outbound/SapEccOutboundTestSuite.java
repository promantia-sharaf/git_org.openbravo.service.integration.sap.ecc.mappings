/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SAPStd110.class,//
    SAPStd140.class,//
    SAPStd050.class,//
    SAPStd070.class,//
    SAPStd080.class,//
    SAPStd160.class,//
    SAPStd190.class,//
    SAPStd195.class })
public class SapEccOutboundTestSuite {
  // No content is required, this is just the definition of a test suite.
}
