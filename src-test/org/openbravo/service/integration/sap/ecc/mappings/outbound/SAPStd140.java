/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.fail;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.ConversionRate;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * This test case verifies importing currency conversion rates at (*) Organization, valid from the
 * date of the import.
 * 
 */
public class SAPStd140 extends OutBoundTest {

  private static final String CURRENCY_ENTITY_MAPPING = "55131031210D4F998E12F842352B3E44";

  private final ConvertionRateData[] convertionRateAsserts = {
      new ConvertionRateData("EUR", "USD", new BigDecimal("0.94"), new BigDecimal("1.06")),
      new ConvertionRateData("EUR", "MXN", new BigDecimal("22.8"), new BigDecimal("0.04")),
      new ConvertionRateData("USD", "EUR", new BigDecimal("1.06"), new BigDecimal("0.94")),
      new ConvertionRateData("MXN", "EUR", new BigDecimal("0.04"), new BigDecimal("22.8")) };

  @Override
  protected File getIdocFileToImport() {
    File file = new File(OUTBOUND_BASE_PATH + "WPDCUR.xml");
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return CURRENCY_ENTITY_MAPPING;
  }

  @Override
  protected void prepareTest() {
    cleanConversionrate("EUR", "USD", "MXN");
    cleanConversionrate("USD", "EUR");
    cleanConversionrate("MXN", "EUR");

    OBDal.getInstance().flush();
  }

  private void cleanConversionrate(String currency, String... currenciesTo) {
    for (String currencyTo : currenciesTo) {
      ConversionRate currentConversionRate = getCurrentConversionRate(currency, currencyTo);
      setValidToDateToYesterday(currentConversionRate);
    }
  }

  private void setValidToDateToYesterday(ConversionRate currentConversionRate) {
    if (currentConversionRate != null) {
      Calendar yesterday = Calendar.getInstance();
      yesterday.add(Calendar.DAY_OF_MONTH, -1);
      currentConversionRate.setValidToDate(yesterday.getTime());
    }
  }

  private ConversionRate getCurrentConversionRate(String currency, String currencyTo) {
    StringBuilder hqlQuery = new StringBuilder("from CurrencyConversionRate cr where ");
    hqlQuery
        .append("cr.client.id = :clientId and cr.currency.id = :currencyId and cr.toCurrency.id = :toCurrency ");
    hqlQuery.append("order by cr.validToDate desc ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("currencyId", MappingUtils.getCurrencyByISOCode(currency).getId());
    query.setParameter("toCurrency", MappingUtils.getCurrencyByISOCode(currencyTo).getId());
    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());
    query.setMaxResults(1);

    return (ConversionRate) query.uniqueResult();
  }

  @Override
  protected void doAsserts() {
    assertConvertionRate();
  }

  private void assertConvertionRate() {
    for (ConvertionRateData assertData : convertionRateAsserts) {
      assertConversionRate(assertData);
    }
  }

  private void assertConversionRate(ConvertionRateData assertData) {
    StringBuilder hqlQuery = new StringBuilder("from CurrencyConversionRate cr where ");
    hqlQuery.append("cr.client.id = :clientId ");
    hqlQuery.append("and cr.currency.id = :currencyId ");
    hqlQuery.append("and cr.toCurrency.id = :toCurrency ");
    hqlQuery.append("and validFromDate >= :validFromDate ");
    hqlQuery.append("and validToDate = :validToDate ");
    hqlQuery.append("and multipleRateBy = :multiply and divideRateBy = :divide ");

    Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(hqlQuery.toString());
    query.setParameter("currencyId", MappingUtils.getCurrencyByISOCode(assertData.getCurrency())
        .getId());
    query.setParameter("toCurrency", MappingUtils.getCurrencyByISOCode(assertData.getCurrencyTo())
        .getId());
    query.setParameter("validFromDate", assertData.getValidFromDate());
    query.setParameter("validToDate", assertData.getValidToDate());
    query.setParameter("multiply", assertData.getMultiplyBy());
    query.setParameter("divide", assertData.getDivideBy());
    query.setParameter("clientId", OBContext.getOBContext().getCurrentClient().getId());

    query.setMaxResults(1);
    if (query.uniqueResult() == null) {
      fail("There should be a ConversionRate with data " + assertData.toString());
    }
  }

  private class ConvertionRateData {
    private String currency;
    private String currencyTo;
    private Date validFromDate;
    private Date validToDate;
    private BigDecimal multiplyBy;
    private BigDecimal divideBy;

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public String getCurrency() {
      return currency;
    }

    public String getCurrencyTo() {
      return currencyTo;
    }

    public Date getValidFromDate() {
      return validFromDate;
    }

    public Date getValidToDate() {
      return validToDate;
    }

    public BigDecimal getMultiplyBy() {
      return multiplyBy;
    }

    public BigDecimal getDivideBy() {
      return divideBy;
    }

    public ConvertionRateData(String currency, String currencyTo, BigDecimal multiplyBy,
        BigDecimal divideBy) {
      super();
      this.currency = currency;
      this.currencyTo = currencyTo;
      try {
        this.validFromDate = sdf.parse(sdf.format(Calendar.getInstance().getTime()));
        this.validToDate = sdf.parse("9999-12-31");
      } catch (ParseException e) {
        e.printStackTrace();
      }
      this.multiplyBy = multiplyBy;
      this.divideBy = divideBy;
    }

    @Override
    public String toString() {
      return "ConvertionRateData [currency=" + currency + ", currencyTo=" + currencyTo
          + ", validFromDate=" + validFromDate + ", validToDate=" + validToDate + ", multiplyBy="
          + multiplyBy + ", divideBy=" + divideBy + "]";
    }
  }

}
