/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

/**
 * This test verifies exporting Goods Receipts
 *
 */
public class SAPStd350 extends InboundTest {

  private static final String EXPORTED_FINE_NAME = "WPUWBW101.xml";
  private static final String WPUWBW_ENTITY_MAPPING_ID = "EB7A4E4EFA954733A747B539D1EC8F6B";
  private static final String GOODS_RECEIPT_ID = "A13DE11824B042A09AEEE790C4946ED0";

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FINE_NAME;
  }

  @Override
  protected String getIdToExport() {
    return GOODS_RECEIPT_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WPUWBW_ENTITY_MAPPING_ID;
  }

}
