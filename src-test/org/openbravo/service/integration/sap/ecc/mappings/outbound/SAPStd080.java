/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.service.integration.sap.ecc.mappings.util.MappingUtils;

/**
 * This test case explains how to configure Openbravo to take into account the SAP field FILIALE of
 * "Outbound" Idocs. In this case organization's mapping is required.
 */
public class SAPStd080 extends OutBoundTest {

  private static final String PRODUCT_CATEGORY_ENTITY_MAPPING = "954BB14EC35D45F9BE1999D78FB08704";

  private final String[] fileNames = { "WPDWGR_2.xml", "WPDWGR_1.xml", "WPDWGR_0.xml" };
  private final String[] productCategorySearchKeys = { "PROD_CAT_01", "PROD_CAT_02", "PROD_CAT_03" };
  private final String[] parentProductCategorySearchKeys = { "PROD_CAT_02", "PROD_CAT_03", null };
  private final String[] productCategoryNames = { "PRODUCT CATEGORY 001", "PRODUCT CATEGORY 002",
      "PRODUCT CATEGORY 003" };
  private int iteration = 0;

  @Override
  protected File getIdocFileToImport() {

    File file = new File(OUTBOUND_BASE_PATH + fileNames[iteration]);
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return PRODUCT_CATEGORY_ENTITY_MAPPING;
  }

  private void removeProductCategory() {
    for (int i = 0; i < productCategorySearchKeys.length; i++) {
      ProductCategory productCategory = getProductCategory(productCategorySearchKeys[i]);
      if (productCategory != null) {
        OBDal.getInstance().remove(productCategory);
      }
    }
    OBDal.getInstance().flush();
  }

  private ProductCategory getProductCategory(String searchKey) {
    OBCriteria<ProductCategory> criteria = OBDal.getInstance()
        .createCriteria(ProductCategory.class);
    criteria.add(Restrictions.eq(ProductCategory.PROPERTY_SEARCHKEY, searchKey));
    ProductCategory productCategory = (ProductCategory) criteria.uniqueResult();
    return productCategory;
  }

  @Override
  public void importIdoc() throws Exception {
    removeProductCategory();
    while (iteration < productCategorySearchKeys.length) {
      super.importIdoc();
      iteration++;
    }
    performAsserts();
  }

  protected void performAsserts() {
    OBDal.getInstance().flush();
    assertProductCategoryName();
    assertProductCategorySapCode();
    assertProductCategoryTree();
  }

  private void assertProductCategorySapCode() {

    for (int i = 0; i < productCategorySearchKeys.length; i++) {
      ProductCategory productCategory = getProductCategory(productCategorySearchKeys[i]);
      assertTrue("Product Category SAP CODE should be \"" + productCategoryNames[i] + "\"",
          productCategory.getSapobmpSapCode().equals(productCategorySearchKeys[i]));
    }
  }

  private void assertProductCategoryTree() {
    for (int i = 0; i < productCategorySearchKeys.length; i++) {
      ProductCategory productCategory = getProductCategory(productCategorySearchKeys[i]);
      TreeNode productCategoryNode = MappingUtils.getTreeNodeByEntityId(productCategory);
      if (parentProductCategorySearchKeys[i] != null) {
        ProductCategory parentProductCategory = getProductCategory(parentProductCategorySearchKeys[i]);
        TreeNode parentProductCategoryNode = MappingUtils
            .getTreeNodeByEntityId(parentProductCategory);
        assertTrue("Search key of parent Product Category should be \""
            + parentProductCategorySearchKeys[i] + "\"",
            productCategoryNode.getReportSet().equals(parentProductCategoryNode.getNode()));
      } else {
        assertTrue("Top Product Category should not have a parent", productCategoryNode
            .getReportSet().equals("0"));
      }
    }
  }

  private void assertProductCategoryName() {
    for (int i = 0; i < productCategorySearchKeys.length; i++) {
      ProductCategory productCategory = getProductCategory(productCategorySearchKeys[i]);
      assertTrue("Product Category Name should be \"" + productCategoryNames[i] + "\"",
          productCategory.getName().equals(productCategoryNames[i]));
    }
  }

}
