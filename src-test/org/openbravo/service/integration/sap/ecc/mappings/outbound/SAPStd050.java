/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.service.integration.sap.ecc.mappings.outbound;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openbravo.service.integration.sap.ecc.mappings.util.TestConstants.OUTBOUND_BASE_PATH;

import java.io.File;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.ProductCategory;

/**
 * This test case explains how to import the first SAP Outbound IDoc to import, WPDWGR IDoc, as this
 * one contains mandatory information required to properly import products in Openbravo. This test
 * cases also explains that the import will fail because there is not any mapping between Openbravo
 * * organization and SAP Referece Store organization.
 */
public class SAPStd050 extends OutBoundTest {

  private static final String EXCEPTION_MESSAGE = "There is no Organization mapped with SAP Filiale S001";

  private static final String PRODUCT_CATEGORY_ENTITY_MAPPING = "954BB14EC35D45F9BE1999D78FB08704";

  private final String[] fileNames = { "WPDWGR_2.xml", "WPDWGR_1.xml", "WPDWGR_0.xml" };
  private final String[] productCategorySearchKeys = { "PROD_CAT_01", "PROD_CAT_02", "PROD_CAT_03" };
  private int iteration = 0;

  @Override
  protected File getIdocFileToImport() {

    File file = new File(OUTBOUND_BASE_PATH + fileNames[iteration]);
    return file;
  }

  @Override
  protected String getEntityMappingId() {
    return PRODUCT_CATEGORY_ENTITY_MAPPING;
  }

  private void removeProductCategory() {
    for (int i = 0; i < productCategorySearchKeys.length; i++) {
      ProductCategory productCategory = getProductCategory(productCategorySearchKeys[i]);
      if (productCategory != null) {
        OBDal.getInstance().remove(productCategory);
      }
    }
    OBDal.getInstance().flush();
  }

  private ProductCategory getProductCategory(String searchKey) {
    OBCriteria<ProductCategory> criteria = OBDal.getInstance()
        .createCriteria(ProductCategory.class);
    criteria.add(Restrictions.eq(ProductCategory.PROPERTY_SEARCHKEY, searchKey));
    ProductCategory productCategory = (ProductCategory) criteria.uniqueResult();
    return productCategory;
  }

  @Override
  public void importIdoc() throws Exception {
    unMapAllOrganization();
    removeProductCategory();
    try {
      while (iteration < productCategorySearchKeys.length) {
        super.importIdoc();
        iteration++;
      }
      fail("OBException should be thrown");
    } catch (OBException e) {
      assertTrue("There should bean Organization mapped with SAP Filiale S001", e.getMessage()
          .contains(EXCEPTION_MESSAGE));
    }
  }

}
