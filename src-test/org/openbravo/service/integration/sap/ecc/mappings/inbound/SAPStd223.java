/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


package org.openbravo.service.integration.sap.ecc.mappings.inbound;

import org.junit.Before;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;

/**
 * This test case explains how to generate a WP_PER Inbound file to SAP containing a business
 * partner modified in the OB Back Office, "Business Partner" window. Fiscal location is modified.
 *
 */
public class SAPStd223 extends InboundTest {

  private static final String EXPORTED_FILE_NAME = "WP_PER.xml";
  private static final String POSTAL_CODE = "99999";
  private static final String WP_PER_ENTITY_MAPPING_ID = "523A046E62A04BE4815A329DF2E6A962";
  private static final String CARL_STUDSON_BUSINESS_PARTNER_ID = "D5BF95FA079B4248B2CA2E2DA477D1B8";

  @Override
  protected String getIdToExport() {
    return CARL_STUDSON_BUSINESS_PARTNER_ID;
  }

  @Override
  protected String getEntityMappingId() {
    return WP_PER_ENTITY_MAPPING_ID;
  }

  @Before
  public void init() {
    super.init();
    OBContext.setAdminMode(false);
    try {
      BusinessPartner carlStudson = OBDal.getInstance().get(BusinessPartner.class,
          CARL_STUDSON_BUSINESS_PARTNER_ID);
      carlStudson.getBusinessPartnerLocationList().get(0).getLocationAddress()
          .setPostalCode(POSTAL_CODE);
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Override
  protected String getInboundFileName() {
    return EXPORTED_FILE_NAME;
  }

}
